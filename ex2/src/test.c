#include <stdlib.h>
#include <stdio.h>
#include "atoms.h"

// Parameters

#define MIX 0.1
#define THRESH 1e-10

const double alpha_H[4] = { 13.00773, 1.962079, 0.444529, 0.1219492 };
const double alpha_He[4] = { 14.899983, 2.726485, 0.757447, 0.251390 };
const double alpha_Be[8] = { 70.64859542, 12.92782254,  3.591490662,  1.191983464,
                             3.072833610, 0.6652025433, 0.2162825386, 0.08306680972 };
const double beta_Be[8] = { 0.05675242080,  0.2601413550,     0.5328461143, 0.2916254405,
                            -0.06220714565, 0.00002976804596, 0.5588549221, 0.4977673218 };

//
// Main for testing the functions of atoms.h
int main()
{
    size_t i, j, k;
    size_t num_iter;
    double energy;
    double coeff[16];

    // Hydrogen ------------------------------------

    compute_hydrogen(alpha_H, 4, 1, &energy, coeff);

    printf("HYDROGEN\n\nEnergy: %f\n\n", energy);
    printf("Coefficients:\n");

    for (i = 0; i < 4; i++) {
        printf("%f\n", coeff[i]);
    }

    // ---------------------------------------------

    // Helium --------------------------------------

    num_iter = compute_atom(alpha_He, 4, 2, MIX, THRESH, &energy, coeff);

    if (num_iter != 0) {
        printf("\n\nHELIUM\n\nEnergy: %f\n\n", energy);
        printf("Coefficients:\n");

        for (i = 0; i < 4; i++) {
            printf("%f\n", coeff[i]);
        }

        printf("\nNumber of iterations: %ld\n", num_iter);
    } else {
        printf("\n\nHELIUM: the algorithm does not converge\n");
    }

    // ---------------------------------------------

    // Beryllium -----------------------------------

    num_iter = compute_atom(alpha_Be, 8, 4, MIX, THRESH, &energy, coeff);

    if (num_iter != 0) {
        printf("\n\nBERYLLIUM\n\nEnergy: %f\n", energy);

        for (i = 0; i < 2; i++) {
            printf("\nCoefficients of orbital %ld:\n", i);
            for (j = 0; j < 8; j++) {
                printf("%f\n", coeff[i + j * 2]);
            }
        }

        printf("\nNumber of iterations: %ld\n", num_iter);
    } else {
        printf("\n\nBERYLLIUM: the algorithm does not converge\n");
    }

    // ---------------------------------------------

    // Beryllium with contracted orbitals ----------

    num_iter = compute_atom_contr(alpha_Be, beta_Be, 2, 4, 4, MIX, THRESH, &energy, coeff);

    if (num_iter != 0) {
        printf("\n\nBERYLLIUM with contracted orbitals\n\nEnergy: %f\n", energy);

        for (i = 0; i < 2; i++) {
            printf("\nCoefficients of orbital %ld:\n", i);
            for (j = 0; j < 2; j++) {
                printf("%f\n", coeff[i + j * 2]);
            }

            printf("\nAll coefficients of orbital %ld:\n", i);
            for (j = 0; j < 2; j++) {
                for (k = 0; k < 4; k++) {
                    printf("%.7f\n", coeff[i + j * 2] * beta_Be[k + j * 4]);
                }
            }
        }

        printf("\nNumber of iterations: %ld\n", num_iter);
    } else {
        printf("\n\nBERYLLIUM with contracted orbitals: the algorithm does not converge\n");
    }

    // ---------------------------------------------

    return 0;
}
