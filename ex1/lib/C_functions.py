import ctypes
from numpy.ctypeslib import ndpointer
import numpy as np

from pathlib import Path
import sys

THIS_DIRECTORY = Path(__file__).parent.absolute()
COMMON_DIRECTORY = Path(__file__).parents[2] / 'lib'
FILENAME = THIS_DIRECTORY / 'libnumerov.so'

sys.path.append(str(COMMON_DIRECTORY))

from C_functions_utils import Cfunction


class Bessel(Cfunction):
    def __init__(self):
        _argtypes = (ctypes.c_size_t,
                     ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),
                     ndpointer(dtype=np.double, ndim=1,
                               flags='C_CONTIGUOUS'), ctypes.c_size_t)
        _restype = ctypes.c_int
        super().__init__(FILENAME, _argtypes, _restype)
        self._bessel_j_func = self.CDLL.bessel_j_all
        self._bessel_n_func = self.CDLL.bessel_n_all

    def _bessel(self, x, l):
        n = len(x)
        x = x.astype(np.double, copy=False)
        y = np.empty(shape=n, dtype=np.double)
        if self.function(ctypes.c_size_t(n), y, x, ctypes.c_size_t(l)) == 0:
            return y
        else:
            return None

    def _bessel_j(self, x, l):
        self.init_function(self._bessel_j_func)
        return self._bessel(x, l)

    def _bessel_n(self, x, l):
        self.init_function(self._bessel_n_func)
        return self._bessel(x, l)


def bessel_j(*args, **kwargs):
    """ bessel_j(x, l)
        Compute the spherical Bessel function j_l at the points in x.

        Parameters:
            x: 1D array
                Input vector
            l: int > 0
                Index of the spherical Bessel function (orbital angular momentum quantum number)

        Returns:
            y: 1D array
                Output vector

        Binding:
            int bessel_j_all(const size_t n, double *y, const double *x, const size_t l)

        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libnumerov.so ./src/*.c ./src/*.h ../src/*.c ../src/*.h
    """
    return Bessel()._bessel_j(*args, **kwargs)


def bessel_n(*args, **kwargs):
    """ bessel_n(x, l)
        Compute the spherical Neumann function n_l at the points in x.

        Parameters:
            x: 1D array
                Input vector
            l: int > 0
                Index of the spherical Neumann function (orbital angular momentum quantum number)

        Returns:
            y: 1D array
                Output vector

        Binding:
            int bessel_n_all(const size_t n, double *y, const double *x, const size_t l)

        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libnumerov.so ./src/*.c ./src/*.h ../src/*.c ../src/*.h
    """
    return Bessel()._bessel_n(*args, **kwargs)


class HarmonicOscillator1D(Cfunction):
    def __init__(self):
        _argtypes = (
            ctypes.c_size_t,  # n_max
            ctypes.c_double,  # DE
            ctypes.c_double,  # thresh
            ctypes.c_double,  # r_low
            ctypes.c_double,  # h
            ctypes.c_size_t,  # M
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # x
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # y
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # E
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # dE
        )
        _restype = ctypes.c_int
        super().__init__(FILENAME, _argtypes, _restype)
        self.init_function(self.CDLL.main_harmonic_oscillator_1D)

    def _harmonic_oscillator_1D(self,
                                n_max: int,
                                DE: float,
                                thresh: float,
                                h: float,
                                M: int,
                                r_low=None):

        if r_low is None:
            r_low = 0.

        x = np.empty(shape=M, dtype=np.double)
        y = np.empty(shape=(n_max + 1) * M, dtype=np.double)
        E = np.empty(shape=n_max + 1, dtype=np.double)
        dE = np.empty(shape=n_max + 1, dtype=np.double)
        if self.function(ctypes.c_size_t(n_max), ctypes.c_double(DE),
                         ctypes.c_double(thresh), ctypes.c_double(r_low),
                         ctypes.c_double(h), ctypes.c_size_t(M), x, y, E,
                         dE) == 0:
            return x, y.reshape(n_max + 1, M), E, dE
        else:
            return None


def harmonic_oscillator_1D(*args, **kwargs):
    """ harmonic_oscillator_1D(n_max, DE, thresh, h, M, r_low=None)
        Find the energy levels and the corresponding wave functions for n <= n_max in the 1D harmonic oscillator case.
        
        Parameters:
            n_max: int > 0
                Maximum value of the principal quantum number
            DE: float > 0
                Energy increase for find_next_eigen
            thresh: float > 0
                Twice the error on each eigenenergy found
            h: float > 0
                Step for the Numerov algorithm
            M: int > 0
                Number of points at which each wave function is evaluated 
            r_low: float >= 0 (default = 0)
                Initial value of the spatial coordinate for the wave function calculation

        Returns:
            None if function does not converge
            x: 1D array (shape: M)
                Positions
            y: 2D array (shape: n_max + 1 x M)
                Wave functions
            E: 1D array (shape n_max + 1)
                Energy levels
            dE: 1D array (shape n_max + 1)
                Error on energy levels

        Binding:
            int main_harmonic_oscillator_1D(const size_t n_max, const double DE, const double thresh, const double r_low, const double h, const size_t M, double *x, double *y, double *E, double *dE)

        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libnumerov.so ./src/*.c ./src/*.h ../src/*.c ../src/*.h
    """
    return HarmonicOscillator1D()._harmonic_oscillator_1D(*args, **kwargs)


class HarmonicOscillator3D(Cfunction):
    def __init__(self):
        _argtypes = (
            ctypes.c_size_t,  # n_max
            ctypes.c_size_t,  # l
            ctypes.c_double,  # DE
            ctypes.c_double,  # thresh
            ctypes.c_double,  # r_low
            ctypes.c_double,  # h
            ctypes.c_size_t,  # M
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # x
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # y
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # E
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # dE
        )
        _restype = ctypes.c_int
        super().__init__(FILENAME, _argtypes, _restype)
        self.init_function(self.CDLL.main_harmonic_oscillator_3D)

    def _harmonic_oscillator_3D(self,
                                n_max: int,
                                l: int,
                                DE: float,
                                thresh: float,
                                h: float,
                                M: int,
                                r_low=None):
        if r_low is None:
            r_low = h

        x = np.empty(shape=M, dtype=np.double)
        y = np.empty(shape=(n_max + 1) * M, dtype=np.double)
        E = np.empty(shape=(n_max + 1), dtype=np.double)
        dE = np.empty(shape=(n_max + 1), dtype=np.double)

        if self.function(ctypes.c_size_t(n_max), ctypes.c_size_t(l),
                         ctypes.c_double(DE), ctypes.c_double(thresh),
                         ctypes.c_double(r_low), ctypes.c_double(h),
                         ctypes.c_size_t(M), x, y, E, dE) == 0:
            return x, y.reshape(n_max + 1, M), E, dE
        else:
            return None


def harmonic_oscillator_3D(*args, **kwargs):
    """ harmonic_oscillator_3D(n_max, l, DE, thresh, h, M, r_low=None)
        Find the energy levels and the corresponding wave functions for n <= n_max and l fixed in the 3D harmonic oscillator case.

        Parameters:
            n_max: int > 0
                Maximum value of the principal quantum number
            l: int >= 0
                Orbital angular momentum quantum number
            DE: float > 0
                Energy increase for find_next_eigen
            thresh: float > 0
                Twice the error on each eigenenergy found
            h: float > 0
                Step for the Numerov algorithm
            M: int > 0
                Number of points at which each wave function is evaluated 
            r_low: float >= 0 (default h)
                Initial value of the spatial coordinate for the wave function calculation

        Returns:
            None if function does not converge
            x: 1D array (shape: M)
                Positions
            y: 2D array (shape: n_max + 1 x M)
                Wave functions
            E: 1D array (shape n_max + 1)
                Energy levels
            dE: 1D array (shape n_max + 1)
                Error on energy levels

        Binding:
            int main_harmonic_oscillator_3D(const size_t n_max, const size_t l, const double DE, const double thresh, const double r_low, const double h, const size_t M, double *x, double *y, double *E, double *dE)

        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libnumerov.so ./src/*.c ./src/*.h ../src/*.c ../src/*.h
    """
    return HarmonicOscillator3D()._harmonic_oscillator_3D(*args, **kwargs)


class Scattering(Cfunction):
    def __init__(self):
        _argtypes = (
            ctypes.c_double,  # E
            ctypes.c_size_t,  # l_max
            ctypes.c_double,  # r_low
            ctypes.c_double,  # r_1
            ctypes.c_double,  # r_2
            ctypes.c_double,  # h
            ctypes.c_size_t,  # M
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # x
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # y
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # delta_l
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # sigma_tot
        )
        _restype = ctypes.c_int
        super().__init__(FILENAME, _argtypes, _restype)
        self.init_function(self.CDLL.main_scattering)

    def _scattering(self, E: float, l_max: int, r_low: float, r_1: float,
                    r_2: float, h: float, M: int):

        x = np.empty(shape=M, dtype=np.double)
        y = np.empty(shape=(l_max + 1) * M, dtype=np.double)
        delta_l = np.empty(shape=(l_max + 1), dtype=np.double)
        sigma = np.empty(shape=1, dtype=np.double)

        if self.function(ctypes.c_double(E), ctypes.c_size_t(l_max),
                         ctypes.c_double(r_low), ctypes.c_double(r_1),
                         ctypes.c_double(r_2), ctypes.c_double(h),
                         ctypes.c_size_t(M), x, y, delta_l, sigma) == 0:
            return x, y.reshape(l_max + 1, M), delta_l, sigma
        else:
            return None


def scattering(*args, **kwargs):
    """ scattering(E, l_max, r_low, r_1, r_2, h, M)
        Find the phase shifts delta_l and the total cross section sigma_tot for a fixed energy value.

        Parameters:
            E: float
                Energy
            l_max: int > 0
                Maximum value of the orbital angular momentum quantum number for which delta_l is calculated
            r_low: float > 0
                Initial value of the spatial coordinate for the wave function calculation
            r_1, r_2: float > 0
                Points at which the wave function is evaluated for the calculation of delta_l
            h: float > 0
                Step for the Numerov algorithm
            M: int > 0
                Number of points used in the Numerov algorithm

        Returns:
            None if function does not converge
            x: 1D array (shape: M)
                Positions
            y: 2D array (shape: l_max + 1 x M)
                Wave functions
            delta_l: 1D array (shape l_max + 1)
                Phase shift vector
            sigma: 0D array
                Total cross section

        Binding:
            int main_scattering(const double E, const size_t l_max, const double r_low, const double r_1, const double r_2, const double h, const size_t M, double *x, double *y, double *delta_l, double *sigma_tot)

        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libnumerov.so ./src/*.c ./src/*.h ../src/*.c ../src/*.h
    """
    return Scattering()._scattering(*args, **kwargs)
