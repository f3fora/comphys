#ifndef ATOMS_H
#define ATOMS_H

#include <stdlib.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include "variational.h"
#include "hartree_fock.h"
#include "hartree_fock_contracted.h"

int compute_hydrogen(const double *, const size_t, const size_t, double *, double *);
size_t compute_atom(const double *, const size_t, const size_t, const double, const double,
                    double *, double *);
size_t compute_atom_contr(const double *, const double *, const size_t, const size_t, const size_t,
                          const double, const double, double *, double *);

#endif
