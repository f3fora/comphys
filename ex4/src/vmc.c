#include "vmc.h"

/*
 * Compute the squared modulus of the (non-normalized) trial wave function (with boundary
 * correction).
 * particles: pointer to a Particle N-array containing the positions of the particles
 * N: number of particles in the simulation box
 * L: side of the simulation box
 * b_5: variational parameter b^5
 * a: amplitude of the 3rd-degree polynomial a * (r - L/2)^3 which corrects the Jastrow correlation
 *    pseudopotential (b / r)^5 close to r = L/2
 * c: constant shift applied to (b / r)^5
 * r_0_squared: squared modulus of r_0, point of connection between (b / r)^5 and a * (r - L/2)^3
 * return: squared modulus of the (non-normalized) trial wave function
 */
double _compute_pdf(const Particle *particles, const size_t N, const double L, const double b_5,
                    const double a, const double c, const double r_0_squared)
{
    size_t i, j;
    double dr_squared, L_half = 0.5 * L, L_half_squared = L_half * L_half;
    double sum1 = 0., sum2 = 0.;

    for (i = 1; i < N; i++) {
        for (j = 0; j < i; j++) {
            dr_squared = compute_squared_distance(&particles[i], &particles[j], L);

            if (dr_squared < L_half_squared) {
                if (dr_squared <= r_0_squared) {
                    sum1 -= b_5 * pow(dr_squared, -2.5) + c;
                } else {
                    sum2 -= pow(sqrt(dr_squared) - L_half, 3);
                }
            }
        }
    }

    return exp(sum1 + a * sum2);
}

/*
 * Calculate the Lennard-Jones potential.
 * r_squared: squared distance between two particles
 * return: Lennard-Jones potential evaluated in sqrt(r_squared)
 */
double _lennard_jones(const double r_squared)
{
    double v1 = pow(r_squared, -3);
    double v2 = v1 * v1;

    return 4. * (v2 - v1);
}

/*
 * Compute the local energy and the Jackson-Feenberg local energy, and update the circular arrays
 * containing their samples.
 * E_L: pointer to the circular array containing the samples of the local energy [output]
 * E_L_JF: pointer to the circular array containing the samples of the JF local energy [output]
 * particles: pointer to a Particle N-array containing the positions of the particles
 * N: number of particles in the simulation box
 * L: side of the simulation box
 * b_5: variational parameter b^5
 * a: amplitude of the 3rd-degree polynomial a * (r - L/2)^3 which corrects the Jastrow correlation
 *    pseudopotential (b / r)^5 close to r = L/2
 * r_0_squared: squared modulus of r_0, point of connection between (b / r)^5 and a * (r - L/2)^3
 */
void _update_local_energies(Circ_array *E_L, Circ_array *E_L_JF, const Particle *particles,
                            const size_t N, const double L, const double b_5, const double a,
                            const double r_0_squared)
{
    size_t i, j, k, l;
    double L_half = 0.5 * L, L_half_squared = L_half * L_half;
    double dr1[3], dr2[3], dr1_squared, dr2_squared, dr1_mod, dr2_mod;
    double dr_6, dr_7, d, d_squared, scalar_prod;
    double T1 = 0., T2 = 0., T3 = 0., T4 = 0., T5 = 0., V = 0., T_L_JF;

    for (i = 0; i < N; i++) {
        for (j = 0; j < N; j++) {
            if (j != i) {
                dr1_squared = 0.;
                for (l = 0; l < 3; l++) {
                    dr1[l] = pbc(particles[i].r[l] - particles[j].r[l], L);
                    dr1_squared += dr1[l] * dr1[l];
                }

                if (dr1_squared < L_half_squared) {
                    if (j < i) {
                        if (dr1_squared <= r_0_squared) {
                            dr_6 = pow(dr1_squared, 3);
                            dr_7 = dr_6 * sqrt(dr1_squared);
                            T1 += 1. / dr_7;
                            T2 += 1. / (dr_6 * dr_6);
                        } else {
                            dr1_mod = sqrt(dr1_squared);
                            d = dr1_mod - L_half;
                            d_squared = d * d;
                            T3 += d * (2. - L_half / dr1_mod);
                            T4 += d_squared * d_squared;
                        }

                        V += _lennard_jones(dr1_squared);
                    }

                    for (k = 0; k < j; k++) {
                        if (k != i) {
                            dr2_squared = 0.;
                            for (l = 0; l < 3; l++) {
                                dr2[l] = pbc(particles[i].r[l] - particles[k].r[l], L);
                                dr2_squared += dr2[l] * dr2[l];
                            }

                            if (dr2_squared < L_half_squared) {
                                scalar_prod = 0.;
                                for (l = 0; l < 3; l++) {
                                    scalar_prod += dr1[l] * dr2[l];
                                }

                                if (dr1_squared <= r_0_squared) {
                                    if (j > i) {
                                        dr_7 = pow(dr1_squared, 3.5);
                                    }

                                    if (dr2_squared <= r_0_squared) {
                                        T2 += 1. / (dr_7 * pow(dr2_squared, 3.5)) * scalar_prod;
                                    } else {
                                        dr2_mod = sqrt(dr2_squared);
                                        T5 += pow(dr2_mod - L_half, 2) / (dr_7 * dr2_mod) *
                                              scalar_prod;
                                    }
                                } else {
                                    if (j > i) {
                                        dr1_mod = sqrt(dr1_squared);
                                        d_squared = pow(dr1_mod - L_half, 2);
                                    }

                                    if (dr2_squared <= r_0_squared) {
                                        T5 += d_squared / (dr1_mod * pow(dr2_squared, 3.5)) *
                                              scalar_prod;
                                    } else {
                                        dr2_mod = sqrt(dr2_squared);
                                        T4 += pow(dr2_mod - L_half, 2) * d_squared /
                                              (dr1_mod * dr2_mod) * scalar_prod;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    T_L_JF = B * (10. * b_5 * T1 + 3. * a * T3);

    // clang-format off
    Circ_array_add(E_L_JF, T_L_JF + V);
    Circ_array_add(E_L, 2. * T_L_JF - 12.5 * B * b_5 * b_5 * T2 - 4.5 * B * a * a * T4 +
                        7.5 * B * a * b_5 * T5 + V);
    // clang-format on
}

/*
 * Execute a step of the Metropolis algorithm.
 * pdf: pointer to a double variable containing the probability of the current configuration; the
 *      probability of the new configuration will be stored in this variable [output]
 * particles: pointer to a pointer which points to a Particle N-array containing the current
 *            positions of the particles; at the end, *particles will point to the new positions
 *            [output]
 * N: number of particles in the simulation box
 * L: side of the simulation box
 * b_5: variational parameter b^5
 * a: amplitude of the 3rd-degree polynomial a * (r - L/2)^3 which corrects the Jastrow correlation
 *    pseudopotential (b / r)^5 close to r = L/2
 * c: constant shift applied to (b / r)^5
 * r_0_squared: squared modulus of r_0, point of connection between (b / r)^5 and a * (r - L/2)^3
 * delta: parameter for the generation of the new positions
 * return: if the generated configuration is accepted: true
 *         else: false
 */
bool _metropolis_step(double *pdf, Particle **particles, const size_t N, const double L,
                      const double b_5, const double a, const double c, const double r_0_squared,
                      const double delta)
{
    size_t i, j;
    double pdf_new;

    Particle *particles_new = malloc(N * sizeof(Particle));

    for (i = 0; i < N; i++) {
        for (j = 0; j < 3; j++) {
            particles_new[i].r[j] =
                    pbc((*particles)[i].r[j] + delta * ((double)rand() / RAND_MAX - 0.5), L);
        }
    }

    pdf_new = _compute_pdf(particles_new, N, L, b_5, a, c, r_0_squared);

    if ((double)rand() / RAND_MAX < pdf_new / *pdf) {
        free(*particles);
        *particles = particles_new;
        *pdf = pdf_new;

        return true;
    }

    free(particles_new);

    return false;
}

/*
 * Send the content of a variable or an array from the master process (rank 0) to the other
 * processes. This function must be called by all processes.
 * variable: pointer to the variable or array to copy
 * size: size of the object pointed by variable; if the object is a variable, then it must be
 *       size = 1
 * datatype: MPI data type of the variable or of each item of the array to copy
 * rank: rank of the current process
 * nProcs: total number of processes
 */
void _copy_from_rank_zero(void *variable, const int size, const MPI_Datatype datatype,
                          const int rank, const int nProcs)
{
    size_t i;

    MPI_Bcast(variable, size, datatype, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        for (i = 1; i < nProcs; i++) {
            MPI_Send(variable, size, datatype, i, 0, MPI_COMM_WORLD);
        }
    } else {
        MPI_Recv(variable, size, datatype, 0, 0, MPI_COMM_WORLD, MPI_STATUSES_IGNORE);
    }
}

/*
 * Compute, through the Metropolis algorithm, the mean values of local energy and Jackson-Feenberg
 * local energy with statistical errors, the autocorrelation coefficients of the two quantities and
 * the corresponding autocorrelation lengths.
 * INPUTS:
 * seed: seed for the random number generation
 * b: variational parameter
 * n_cells: number of elementary cubic cells which constitute the simulation box; the number of
 *          particles in the simulation box will be N = 4 * n_cells
 * rho: particle density
 * size_CE: size of the array pointed by CE; it is equal to the number of points in which the
 *          autocorrelation coefficient of the local energy is evaluated
 * size_CE_JF: size of the array pointed by CE_JF; it is equal to the number of points in which the
 *             autocorrelation coefficient of the JF local energy is evaluated
 * M_eq: number of Metropolis steps constituting the equilibration phase
 * M_eq_procs: number of Metropolis steps that each processor performs independently between the
 *             equilibration phase and the statistical one
 * M_stat: number of Metropolis steps constituting the statistical phase
 * acc_rate_min: lower bound of the range within which the acceptance rate is approximately set
 * acc_rate_max: upper bound of the range within which the acceptance rate is approximately set
 * delta_ini_over_L: initial value of the parameter Delta divided by the side of the simulation box
 * dDelta_over_L: initial value of the parameter dDelta divided by the side of the simulation box;
 *                dDelta is repeatedly subtracted or added to Delta for its adjustment during the
 *                equilibration phase
 * steps_control: number of Metropolis steps between two consecutive adjustments of Delta during the
 *                equilibration phase
 * OUTPUTS:
 * E_mean: pointer to a double variable, where the mean value of the local energy (per particle)
 *         will be stored
 * E_std: pointer to a double variable, where the statistical error on the mean local energy (per
 *        particle) will be stored
 * E_JF_mean: pointer to a double variable, where the mean value of the JF local energy (per
 *            particle) will be stored
 * E_JF_std: pointer to a double variable, where the statistical error on the mean JF local energy
 *           (per particle) will be stored
 * CE: pointer to a double (size_CE)-array, where the autocorrelation coefficient of the local
 *     energy will be stored
 * tau: pointer to a double variable, where the autocorrelation length of the local energy will be
 *      stored
 * tau_std: pointer to a double variable, where the error on the autocorrelation length of the local
 *          energy will be stored
 * CE_JF: pointer to a double (size_CE_JF)-array, where the autocorrelation coefficient of the JF
 *        local energy will be stored
 * tau_JF: pointer to a double variable, where the autocorrelation length of the JF local energy
 *         will be stored
 * tau_JF_std: pointer to a double variable, where the error on the autocorrelation length of the JF
 *             local energy will be stored
 * delta_over_L: pointer to a double variable, where the final value of the parameter Delta divided
 *               by the side of the simulation box will be stored
 * acc_rate: pointer to a double variable, where the average acceptance rate measured during the
 *           statistical phase will be stored
 * NOTE: the outputs are only for the master process identified by rank 0.
 */
int vmc(const size_t seed, const double b, const size_t n_cells, const double rho,
        const size_t size_CE, const size_t size_CE_JF, const size_t M_eq, const size_t M_eq_procs,
        const size_t M_stat, const double acc_rate_min, const double acc_rate_max,
        const double delta_ini_over_L, const double dDelta_over_L, const size_t steps_control,
        double *E_mean, double *E_std, double *E_JF_mean, double *E_JF_std, double *CE, double *tau,
        double *tau_std, double *CE_JF, double *tau_JF, double *tau_JF_std, double *delta_over_L,
        double *acc_rate)
{
    size_t i, j;
    size_t N = n_cells * 4;
    size_t acc = 0, acc_tot = 0;
    size_t steps_print_eq = STEPS_PRINT_EQ * steps_control;
    size_t M_stat_procs, M_stat_tot;
    double b_5 = pow(b, 5);
    double L = pow((double)N / rho, 1. / 3.);
    double l = L / pow(n_cells, 1. / 3.);
    double a = -40. * pow(8. / 3., 7) * b_5 / pow(L, 8);
    double r_0 = 3. / 8. * L;
    double r_0_squared = r_0 * r_0;
    double c = a * pow(r_0 - 0.5 * L, 3) - b_5 / pow(r_0, 5);
    double E_sum = 0., E_JF_sum = 0., E_squared_sum = 0., E_JF_squared_sum = 0.;
    double E_sum_tot, E_JF_sum_tot, E_squared_sum_tot, E_JF_squared_sum_tot;
    double pdf, delta, dDelta, acc_rate_proc, acc_rate_sum, L_half_m3, V_tail;
    double CE_proc[size_CE], CE_JF_proc[size_CE_JF];
    int rank, nProcs;
    bool test[3] = { false, false, false };

    Circ_array E_L, E_L_JF;

    Particle *particles = malloc(N * sizeof(Particle));

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &nProcs);

    M_stat_procs = ceil((double)M_stat / (double)nProcs);

    Circ_array_alloc(&E_L, size_CE);
    Circ_array_alloc(&E_L_JF, size_CE_JF);

    initialize_autocorr(CE_proc, size_CE);
    initialize_autocorr(CE_JF_proc, size_CE_JF);

    if (rank == 0) {
        M_stat_tot = M_stat_procs * nProcs;

        delta = delta_ini_over_L * L;
        dDelta = dDelta_over_L * L;

        place_particles(particles, l, n_cells);

        pdf = _compute_pdf(particles, N, L, b_5, a, c, r_0_squared);
        _update_local_energies(&E_L, &E_L_JF, particles, N, L, b_5, a, r_0_squared);

        printf("Variational parameter: b = %f\n", b);
        printf("Number of particles: N = %ld\n", N);
        printf("Density: rho = %f\n", rho);
        printf("Side of simulation box: L = %f\n\n", L);
        printf("Initial local energy per particle: E_L / N = %f\n",
               Circ_array_last_item(&E_L) / (double)N);
        printf("Initial Jackson-Feenberg local energy per particle: E_L_JF / N = %f\n\n",
               Circ_array_last_item(&E_L_JF) / (double)N);
        printf("Equilibration phase\n\n");
        printf("Step\t\tE_L / N\t\tE_L_JF / N\tDelta / L\tPartial Acceptance Rate\n");
        fflush(stdout);

        srand(seed);

        for (i = 1, j = 0;
             !test[2] || j <= M_eq || acc_rate_proc > acc_rate_max || acc_rate_proc < acc_rate_min;
             i++) {
            if (_metropolis_step(&pdf, &particles, N, L, b_5, a, c, r_0_squared, delta)) {
                if (test[2]) {
                    acc_tot++;
                }
                acc++;
            }

            if (!(i % steps_control)) {
                acc_rate_proc = (double)acc / (double)steps_control;

                if (acc_rate_proc > acc_rate_max) {
                    delta += dDelta;
                    test[1] = true;

                    if (test[0] == test[1]) {
                        dDelta *= 1.1;
                    }
                } else if (acc_rate_proc < acc_rate_min) {
                    delta -= dDelta;
                    test[1] = false;

                    if (test[0] == test[1]) {
                        dDelta *= 1.1;
                    }
                } else if (!test[2]) {
                    test[2] = true;
                }

                if (delta <= 0) {
                    delta += dDelta;
                    dDelta *= 0.5;
                } else if (i > steps_control && test[0] != test[1]) {
                    dDelta *= 0.5;
                }

                test[0] = test[1];
                acc = 0;
            }

            if (!(i % steps_print_eq)) {
                _update_local_energies(&E_L, &E_L_JF, particles, N, L, b_5, a, r_0_squared);

                printf("%ld\t\t%f\t%f\t%f\t%f\n", i, Circ_array_last_item(&E_L) / (double)N,
                       Circ_array_last_item(&E_L_JF) / (double)N, delta / L, acc_rate_proc);
                fflush(stdout);
            }

            if (test[2]) {
                j++;
            }
        }

        printf("\nEnd of the equilibration phase\n\n");
        printf("Number of iterations: %ld\n", i - 1);
        printf("Acceptance rate: %f\n", (double)acc_tot / (double)(j - 1));
        printf("Delta / L: %f\n\n", delta / L);
        printf("\nStatistical phase\n\n");
        printf("Process\t\tStep\t\tE_L / N\t\tE_L_JF / N\tAcceptance Rate\n");
        fflush(stdout);

        Circ_array_realloc(&E_L, size_CE);
        Circ_array_realloc(&E_L_JF, size_CE_JF);

        acc_tot = 0;
    }

    _copy_from_rank_zero(&pdf, 1, MPI_DOUBLE, rank, nProcs);
    _copy_from_rank_zero(&delta, 1, MPI_DOUBLE, rank, nProcs);
    _copy_from_rank_zero(particles, N, create_Particle_mpiDatatype(), rank, nProcs);

    srand(seed + rank);

    for (i = 1; i <= M_eq_procs; i++) {
        _metropolis_step(&pdf, &particles, N, L, b_5, a, c, r_0_squared, delta);
    }

    for (i = 1; i <= M_stat_procs; i++) {
        if (_metropolis_step(&pdf, &particles, N, L, b_5, a, c, r_0_squared, delta)) {
            acc_tot++;
            _update_local_energies(&E_L, &E_L_JF, particles, N, L, b_5, a, r_0_squared);
        } else {
            Circ_array_add(&E_L, Circ_array_last_item(&E_L));
            Circ_array_add(&E_L_JF, Circ_array_last_item(&E_L_JF));
        }

        if (i >= size_CE) {
            update_autocorr(CE_proc, &E_L);
        }
        if (i >= size_CE_JF) {
            update_autocorr(CE_JF_proc, &E_L_JF);
        }

        E_sum += Circ_array_last_item(&E_L);
        E_squared_sum += Circ_array_last_item(&E_L) * Circ_array_last_item(&E_L);
        E_JF_sum += Circ_array_last_item(&E_L_JF);
        E_JF_squared_sum += Circ_array_last_item(&E_L_JF) * Circ_array_last_item(&E_L_JF);

        if (!(i % STEPS_PRINT_STAT)) {
            printf("%d\t\t%ld\t\t%f\t%f\t%f\n", rank, i, Circ_array_last_item(&E_L) / (double)N,
                   Circ_array_last_item(&E_L_JF) / (double)N, (double)acc_tot / (double)i);
            fflush(stdout);
        }
    }

    MPI_Barrier(MPI_COMM_WORLD);

    if (rank == 0) {
        printf("\nEnd of the statistical phase\n\n");
        printf("Number of iterations per process: %ld\n", M_stat_procs);
        printf("Total number of iterations: %ld\n\n", M_stat_tot);
        fflush(stdout);
    }

    MPI_Barrier(MPI_COMM_WORLD);

    acc_rate_proc = (double)acc_tot / (double)M_stat_procs;

    printf("Acceptance rate of process %d: %f\n", rank, acc_rate_proc);
    fflush(stdout);

    MPI_Reduce(&E_sum, &E_sum_tot, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&E_squared_sum, &E_squared_sum_tot, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&E_JF_sum, &E_JF_sum_tot, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&E_JF_squared_sum, &E_JF_squared_sum_tot, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(CE_proc, CE, size_CE, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(CE_JF_proc, CE_JF, size_CE_JF, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
    MPI_Reduce(&acc_rate_proc, &acc_rate_sum, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);

    if (rank == 0) {
        *delta_over_L = delta / L;
        *acc_rate = acc_rate_sum / (double)nProcs;

        printf("Average acceptance rate: %f\n", *acc_rate);

        L_half_m3 = pow(0.5 * L, -3);
        V_tail = 8. / 3. * M_PI * rho * (1. / 3. * pow(L_half_m3, 3) - L_half_m3);

        *E_mean = E_sum_tot / (double)M_stat_tot;
        *E_JF_mean = E_JF_sum_tot / (double)M_stat_tot;

        normalize_autocorr(CE, size_CE, *E_mean, (M_stat_procs - size_CE + 1) * nProcs);
        normalize_autocorr(CE_JF, size_CE_JF, *E_JF_mean, (M_stat_procs - size_CE_JF + 1) * nProcs);

        compute_tau(tau, tau_std, CE, size_CE);
        compute_tau(tau_JF, tau_JF_std, CE_JF, size_CE_JF);

        *E_std = sqrt((E_squared_sum_tot / (double)M_stat_tot - *E_mean * *E_mean) /
                      ((double)M_stat_tot - 1.) * *tau);
        *E_JF_std = sqrt((E_JF_squared_sum_tot / (double)M_stat_tot - *E_JF_mean * *E_JF_mean) /
                         ((double)M_stat_tot - 1.) * *tau_JF);

        *E_mean = (*E_mean + V_tail) / (double)N;
        *E_JF_mean = (*E_JF_mean + V_tail) / (double)N;
        *E_std /= (double)N;
        *E_JF_std /= (double)N;

        printf("\nResults:\n");
        printf("Autocorrelation length of local energy: %f +/- %f\n", *tau, *tau_std);
        printf("Autocorrelation length of Jackson-Feenberg local energy: %f +/- %f\n", *tau_JF,
               *tau_JF_std);
        printf("Mean local energy per particle: %f +/- %f\n", *E_mean, *E_std);
        printf("Mean Jackson-Feenberg local energy per particle: %f +/- %f\n", *E_JF_mean,
               *E_JF_std);
        printf("Difference between mean local energies per particle: %f +/- %f\n\n",
               fabs(*E_mean - *E_JF_mean), *E_std + *E_JF_std);
    }

    free(particles);

    Circ_array_free(&E_L);
    Circ_array_free(&E_L_JF);

    return 0;
}
