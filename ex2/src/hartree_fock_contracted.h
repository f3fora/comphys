#ifndef HARTREE_FOCK_CONTRACTED_H
#define HARTREE_FOCK_CONTRACTED_H

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include "variational.h"
#include "hartree_fock.h"

size_t self_consistent_method_contr(double *, gsl_matrix *, const double *, const double *,
                                    const size_t, const size_t, const size_t, const gsl_matrix *,
                                    const double, const double);

#endif
