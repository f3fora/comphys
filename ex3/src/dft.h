#ifndef DFT_H
#define DFT_H

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <float.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include "../../src/gsl_utility.h"
#include "../../src/numerov.h"
#include "dft_potentials.h"
#include "dft_structs.h"

// Maximum number of iterations for the self-consistent field method
#define NUM_ITER_SCF_MAX 1000

int compute_closed_shell_systems(const size_t, const double, const NumerovParams, const size_t,
                                 const size_t, size_t *, double *, double *, double *, double *,
                                 Levels *, size_t *);

int self_consistent_method(const size_t, const double, const NumerovParams, const size_t,
                           const size_t, const double, const double, double *, double *, double *,
                           double *, Levels *, size_t *);

#endif
