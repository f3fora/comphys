\section{Discussion of the Results} \label{sec:results}

In this section, we present the results obtained from the VMC calculations. For the computations, performed as the density $\rho$ and the variational parameter $b$ vary, we set $N = 32$, $M_{eq} = 3 \times 10^4$, $M_{eq}^{procs} = 10^3$, $M_{stat} = 5 \times 10^5$.

In order to qualitatively establish the value of acceptance rate $\eta$ for which the autocorrelation lengths of the desired quantities are minimal, some preliminary tests are performed with $\rho = \rho_{exp}$ and $b = 1.2\,\sigma$ fixed. The resulting \autoref{fig:eta} shows that the optimal value of $\eta$ belongs approximately to the interval $[0.32,\, 0.36]$. Consequently, for the subsequent calculations, we impose a value of $\eta$ belonging to this interval, assuming that the behavior of the autocorrelation lengths as functions of $\eta$ does not change too much as $\rho$ and $b$ vary.

\inputfigure[.782]{eta}%
{Autocorrelation Lengths vs Acceptance Rate}%
{The figure shows the autocorrelation lengths $\bar{\tau}$, related to the local energy $E_L$ and the JF local energy $E_L^{JF}$, as functions of the acceptance rate $\eta$, with $\rho = \rho_{exp}$ and $b = 1.2\,\sigma$ fixed. The values of $\bar{\tau}$ are obtained by fitting the function \eqref{eq:fit_tau} to the estimations of the corresponding autocorrelation coefficients $C_{E_L}(\tau)$ and $C_{E_L^{JF}}(\tau)$ (equation \eqref{eq:autocorrelation}). The minimum correlation lengths are obtained when $\eta \in [0.32,\,0.36]$.}

\inputfigure[.782]{tau}%
{Autocorrelation Coefficients}%
{The figure shows the autocorrelation coefficients $C_E(\tau)$ (equation \eqref{eq:autocorrelation}), related to the local energy $E_L$ and the JF local energy $E_L^{JF}$, as functions of the Metropolis steps $\tau$, with $\rho = \rho_{exp}$ and $b = 1.2\,\sigma$ fixed. The solid lines represent the results of the fitting of the function \eqref{eq:fit_tau} to the points.}

As an example, \autoref{fig:tau} shows the autocorrelation coefficients $C_{E_L}(\tau)$ and $C_{E_L^{JF}}(\tau)$ for $\rho = \rho_{exp}$ and $b = 1.2\,\sigma$. The fitted curves \eqref{eq:fit_tau} are not perfectly superimposable to the points given by the MC calculation, but still sufficient to provide an estimation of the autocorrelation lengths necessary to correct the errors on the two estimates of the average energy.

\inputfigure[.976]{b1.0}%
{Energy per Particle vs Variational Parameter}%
{The figure shows, as a function of the variational parameter $b$, the mean energy per particle $\left\langle E \right\rangle / N$ for $\rho = \rho_{exp}$, computed as the mean value of the local energy $E_L$ and of the JF local energy $E_L^{JF}$, divided by the number $N$ of particles. The value $b_0$ of the variational parameter that minimizes the average energy is obtained by fitting the parabola \eqref{eq:parabola_b} to the points close to the minimum. For a fixed value of $b$, the two estimates of the mean energy are approximately equal, as predicted by \eqref{eq:JF_identity}.}

For each considered value of $\rho$, the mean values of the local energy $E_L$ and of the JF local energy $E_L^{JF}$ are computed as $b$ varies. From all the calculations performed, it results that, for any fixed value of $b$, $\left\langle E_L \right\rangle$ and $\left\langle E_L^{JF} \right\rangle$ converge to approximately the same value, as predicted by the JF identity \eqref{eq:JF_identity}. This reassures about the correctness of the implemented algorithm. As an example, \autoref{fig:b1.0} shows the behaviour of $\left\langle E_L \right\rangle / N$ and $\left\langle E_L^{JF} \right\rangle / N$ as functions of $b$, with $\rho = \rho_{exp}$ fixed. The value of $b$ which minimizes the two quantities is obtained by fitting, to the points close to the minimum, the parabola
\begin{equation}\label{eq:parabola_b}
    \frac{\left\langle E_L \right\rangle}{N} = \alpha b^2 + \beta b + \gamma\,,
\end{equation}
where $\alpha$, $\beta$ and $\gamma$ are parameters to be optimized. Similar calculations are also performed for the other considered values of $\rho\,$\footnote{For some plots, see \autoref{app:figures}.}. In any case, we observe that, as expected, the uncertainty on $\left\langle E_L \right\rangle / N$ is always smaller than that on $\left\langle E_L^{JF} \right\rangle / N$, and both uncertainties decrease approaching the minimum point.

In order to obtain a theoretical estimation of the saturation density, we consider, as $\rho$ varies, the best estimate of the energy per particle $E / N$ (reported in \autoref{tab:b}), obtained by minimizing $\left\langle E_L \right\rangle / N$ with respect to $b$. The saturation density $\rho_0$ is then obtained as the minimum of the parabola
\begin{equation}\label{eq:parabola_rho}
    \frac{E}{N} = \alpha \rho^2 + \beta \rho + \gamma\,,
\end{equation}
with $\alpha$, $\beta$ and $\gamma$ parameters to be optimized, which best approximates the behaviour of the computed points. The results are shown in \autoref{fig:rho} and reported in \autoref{tab:comparisons}, where they are compared with what is reported in the paper \cite{schiff} by \citeauthor{schiff}.

\begin{table}[H]
    \centering
    \renewcommand\arraystretch{1.3}
    \begin{tabular}{c|c|c}
        $\rho/\rho_{exp}$ & $b_0$ $[\sigma]$ & $E / N$ $[\varepsilon]$    \\
        \hline
        0.65 	&   1.153(6) 	&   -0.392(2)   \\
        0.7 	&   1.155(6) 	&   -0.397(2)   \\  
        0.75 	&   1.158(6) 	&   -0.399(2)   \\
        0.8 	&   1.161(6) 	&   -0.392(2)   \\
        0.85 	&   1.163(6) 	&   -0.380(2)   \\
        0.9 	&   1.166(7) 	&   -0.359(2)   \\
        0.95 	&   1.169(7) 	&   -0.331(2)   \\
        0.975 	&   1.170(7) 	&   -0.313(2)   \\
        1.0 	&   1.172(7) 	&   -0.294(2)   \\
        1.025 	&   1.173(7) 	&   -0.271(2)   \\
        1.05 	&   1.174(7) 	&   -0.248(2)
    \end{tabular}
    \vspace{1em}
    \caption{For each considered value of density $\rho$ (divided by the experimental saturation density $\rho_{exp}$), the table shows our estimation of the energy per particle $E/N$ and the corresponding value $b_0$ of the variational parameter, obtained from the VMC calculations performed for $N = 32$ particles in the simulation box.}
    \label{tab:b}
\end{table}

\inputfigure[.959]{rho}%
{Energy per Particle vs Density}%
{The figure shows, as a function of the density $\rho$ divided by the experimental saturation density $\rho_{exp}$, the best estimation of the energy per particle $E / N$, obtained by minimizing the mean value of the local energy $E_L$ with respect to the variational parameter $b$. The theoretical saturation density $\rho_0$ is obtained by fitting to the points the parabola \eqref{eq:parabola_rho}. The result is incompatible with what is reported in the paper \cite{schiff} by \citeauthor{schiff}.}

The comparison shows the incompatibility of our results with the experimental measurements and the theoretical calculations reported in the paper. This discrepancy is attributable to the choice to consider a simulation box containing only $N = 32$ particles, which are insufficient to effectively reproduce the thermodynamic limit. In support of this, we try to increase the number of particles to $N = 108$. In order to save computational time, we assume that the values of $b$ which minimize the average energy remain approximately the same for both $N = 32$ and $N = 108$. With this assumption, for each value of $\rho$ considered in the previous calculations, we compute the energy per particle $E / N$ (this time setting $M_{eq} = 3 \times 10^3$, $M_{eq}^{procs} = 10^3$, $M_{stat} = 5 \times 10^4$) and obtain a new estimation of $\rho_0$ by fitting to the points the usual parabola \eqref{eq:parabola_rho}.

\inputfigure[1]{rho108}%
{Comparison between $\boldsymbol{N = 32}$ and $\boldsymbol{N = 108}$: Energy per Particle vs Density}%
{The figure shows the energy per particle $E / N$ as a function of the density $\rho$ divided by the experimental saturation density $\rho_{exp}$. The solid lines represent the parabolas \eqref{eq:parabola_rho} which best approximate the VMC results for simulation boxes with $N = 32$ and $N = 108$ particles. For $N = 108$, the VMC points and the theoretical saturation density $\rho_0$ are also reported. The results obtained for $N = 108$ are significantly closer to both the experimental measurements and the theoretical calculations reported in the paper \cite{schiff} by \citeauthor{schiff} (see \autoref{tab:comparisons}).}

\begin{table}[H]
    \centering
    \renewcommand\arraystretch{1.3}
    \begin{tabular}{c|c|c|c}
        & $N$ & $\rho_0 / \rho_{exp}$ & $E / N$ $[\varepsilon]$\\
        \hline
        &   32      &   0.73(2) &   -0.400(9)   \\
        &   108 	&   0.83(3) &   -0.51(2)    \\
        \citeauthor{schiff} &&  0.9(1)  &   -0.58(2)    \\
        Experiment  &&  1       &   -0.699
    \end{tabular}
    \vspace{1em}
    \caption{The table shows the theoretical estimation $\rho_0$ of the saturation density (divided by the experimental value $\rho_{exp}$) and the corresponding estimate of the energy per particle $E / N$, obtained from our VMC calculations performed for $N = 32$ and $N = 108$ particles in the simulation box. For the same quantities, the experimental measurements and theoretical estimations reported in the paper \cite{schiff} by \citeauthor{schiff} are also shown. For $N = 108$, our estimate of the saturation density is about $83\,\%$ of the experimental value, but it is compatible with the theoretical result of \citeauthor{schiff}.}
    \label{tab:comparisons}
\end{table}

\autoref{fig:rho108} compares the results obtained with the two different numbers of particles. We observe that, as $N$ increases, $E / N$ decreases, while $\rho_0$ grows, both approaching the results reported in the paper. In particular, with reference to \autoref{tab:comparisons}, the value of $\rho_0$ obtained by \citeauthor{schiff} is compatible, within its uncertainty, with our result for $N = 108$, while their value of $E / N$ is about $14\,\%$ smaller than our estimate for $N = 108$. We expect that, by increasing $N$ further, our results can still improve. Further improvements can be obtained by considering a more complicated trial wave function, which takes into account the correlations between more than two particles, introducing new variational parameters.