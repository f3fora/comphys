\subsection{Boundary Corrections} \label{sec:corrections}

In order to preserve the isotropy of the system, we consider a cutoff distance equal to $L / 2$ for all calculations, i.e. all interparticle distances greater than $L / 2$ are neglected. However, this procedure introduces errors, which must be taken into account with appropriate corrections.

First, the total potential must be corrected by adding a tail correction, which accounts for the interactions between particles that have a distance greater than $L / 2$. This correction is estimated to be equal to 
\begin{equation}\label{eq:general_tail_corr}
    V_{tail} = 2 \pi \rho \int_{L/2}^{+\infty} V(r) \,r^2 \,\dd r\,,
\end{equation}
which, for the LJ potential \eqref{eq:lj_potential}, becomes
\begin{equation}\label{eq:tail_corr}
    V_{tail} = \frac{8}{3} \pi \rho \varepsilon \sigma^3 \left[\frac{1}{3} \left(\frac{\sigma}{L / 2}\right)^9 - \left(\frac{\sigma}{L / 2}\right)^3\right].
\end{equation}

Second, for the JF identity \eqref{eq:JF_identity} to remain valid, the Jastrow correlation pseudopotential \eqref{eq:Jastrow_pseudopotential} must vanish for $r = L / 2$, with first and second derivatives equal to zero at this point. For this purpose, the function $(b/r)^5$ must be smoothly connected in a certain point $r_0 < L / 2$ with a function that satisfies the boundary conditions just mentioned. A function with these features is for example the third-degree polynomial $a(r - L/2)^3$. The two functions, $(b/r)^5$ and $a(r - L/2)^3$, are smoothly connected if
\begin{equation}
    \begin{dcases} 
        -5 \frac{b^5}{r_0^6} = 3a \left(r_0 - \frac{L}{2}\right)^2 \\
        30 \frac{b^5}{r_0^7} = 6a \left(r_0 - \frac{L}{2}\right) \\
        \left(\frac{b}{r_0}\right)^5 + c = a \left(r_0 - \frac{L}{2}\right)^3 
    \end{dcases}
\end{equation}
These conditions are satisfied if
\begin{equation}\label{eq:parameters_corr}
    r_0 = \frac{3}{8} L\,, \quad a = -5\frac{8^8 b^5}{3^7 L^8}\,, \quad c = a \left(r_0 - \frac{L}{2}\right)^3 - \left(\frac{b}{r_0}\right)^5.
\end{equation}
With this correction, the Jastrow correlation pseudopotential becomes
\begin{equation}\label{eq:Jastrow_pseudopotential_corr}
    U^{(2)}(r) = 
    \begin{dcases}
        \left(\frac{b}{r}\right)^5 + c \; &\text{if } r \le r_0 \\ a\left(r - \frac{L}{2}\right)^3 \; &\text{if } r_0 < r < \frac{L}{2} \\
        0 \; &\text{otherwise}
    \end{dcases}
\end{equation}
The new local energy and JF local energy can be easily calculated by applying the first line of the equations \eqref{eq:local_energy_Jastrow} and \eqref{eq:JF_local_energy_Jastrow}.