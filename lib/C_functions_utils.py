import ctypes


class Cfunction():
    """ tool class for C bindings

        Instances:
            # Actually all instance are private. Please use the standalone functions
            path
            CDLL
            argtypes
            restypes
            function

        Functions:
            init_function
    """
    def __init__(self, filename: str, argtypes: tuple, restype):
        """ Initialize C binding

            Parameters:
                filename: str
                    path of a library file such as 'lib*.so', located in ex?/lib directory
                argtypes: tuple
                    a tuple of the types which the function has in input
                restype
                    the type which the function returned
        """
        self.path = filename
        self.CDLL = ctypes.CDLL(str(self.path))
        self.argtypes = argtypes
        self.restype = restype

    def init_function(self, function):
        """ Initialize function

            Parameters:
                function: CDLL.'name of the C function'
                    THE C function
        """
        self.function = function
        self.function.restype = self.restype
        self.function.argtypes = self.argtypes
