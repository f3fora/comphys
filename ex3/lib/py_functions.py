import numpy as np
from scipy.integrate import simps


def calculate_polarizability(N, r_s, rho, r):
    """ calculate_polarizability
    
        Parameters:
            N: int
            r_s: float
            rho: np.array
            r: np.array

        Returns:
            Rc: float
            spill_out: float
            polarizability: float
            
    """
    Rc = np.power(N, 1 / 3) * r_s
    idx_Rc = np.abs(r - Rc).argmin()
    spill_out = simps(rho[idx_Rc::] * np.square(r[idx_Rc::]) * 4 * np.pi,
                      r[idx_Rc::])
    polarizability = np.power(Rc, 3) * (1 + spill_out / N)
    return Rc, spill_out, polarizability
