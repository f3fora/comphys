\section{Scattering} \label{sec: scattering}

Tested the correctness of the Numerov method on two problems for which analytical solutions are well known, we try now to apply the algorithm to a more difficult task. In particular, we study the elastic scattering of \ce{H} atoms on a \ce{Kr} atom, with the aim of modeling and reproducing the total cross section, as measured in the paper \cite{Toennies}.

With this purpose, we model the \ce{H}-\ce{Kr} interaction with a Lennard-Jones potential:
\begin{equation}\label{eq: lennard-jones}
    V(r) = 4\varepsilon \left[\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^{6}\right],
\end{equation}
where $\varepsilon = \SI{5.9}{\milli\electronvolt}$, $\sigma = \SI{3.18}{\angstrom}$ and $r$ is the interatomic distance. Notice that in general the parameters $\varepsilon$ and $\sigma$ should be fitted against the experiment. Considering the potential \eqref{eq: lennard-jones}, the radial Schrödinger equation for the \ce{H}-\ce{Kr} relative motion becomes
\begin{equation}\label{eq: dim-schr-scattering}
    \left(-\frac{\hbar^2}{2\mu}\dv[2]{r} + 4\varepsilon \left[\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^{6}\right] + \frac{\hbar^2}{2\mu}\frac{\ell(\ell + 1)}{r^2}\right)\!R_{\ell}(r) = E R_{\ell}(r)\,,
\end{equation}
where $\mu \approx \SI{0.996}{amu}$ is the \ce{H}-\ce{Kr} reduced mass and the energy $E$ is now a parameter of the problem.

It can be proved that the total cross section describing the scattering problem is given by the following formula\footnote{For more details, see for example \cite{Pederiva}.}:
\begin{equation}\label{eq: total-cross-section}
    \sigma_{tot} = \frac{4 \pi}{k^2} \sum_{\ell = 0}^{+\infty} (2 \ell + 1) \sin^2\!\left(\delta_{\ell}\right),
\end{equation}
where $k \equiv \sqrt{2 \mu E} / \hbar$ is the relative wave vector and the phase shifts $\delta_{\ell}$ are estimable by the expression
\begin{equation}\label{eq: delta_l}
    \delta_{\ell} = \arctan\!\left(\frac{\kappa j_{\ell}(k r_2) - j_{\ell}(k r_1)}{\kappa n_{\ell}(k r_2) - n_{\ell}(k r_1)}\right),
\end{equation}
where 
\begin{equation}
    \kappa \equiv \frac{R_{\ell}(r_1)r_2}{R_{\ell}(r_2)r_1}\,,
\end{equation}
$j_{\ell}$ and $n_{\ell}$ are the spherical Bessel and Neumann functions respectively, and $r_1 \neq r_2$ are two coordinates situated at sufficient distance from the origin, in such a way that $R_{\ell}(r)$ tends to a sinusoidal function with period $\lambda = 2 \pi / k$.

% Subsections
\vspace{1mm}
\input{sections/sec_bessel.tex}
\vspace{1mm}
\input{sections/sec_natural_units.tex}
\vspace{1mm}
\input{sections/sec_initial_conditions.tex}
\vspace{1mm}
\input{sections/sec_cross_section.tex}