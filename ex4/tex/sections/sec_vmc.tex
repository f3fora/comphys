\section{Variational Monte Carlo} \label{sec:vmc}

The mean-field approach, extremely useful for studying many-body quantum systems, is not effective in describing certain physical situations, since it does not allow one to properly account for the correlations among particles. To go beyond the intrinsic limits of the mean-field strategy, it is necessary to introduce corrective factors in the wave function, whose optimization passes through the use of Monte Carlo (MC) methods. In particular, in this exercise, we implement a Variational Monte Carlo (VMC) simulation, so called as it arises from the union of the stochastic integration typical of Monte Carlo methods with the variational method of Quantum Mechanics.

The objective is to study the physical properties of the zero-temperature superfluid \ce{^4He}, approximated by a collection of $N$ point-like particles occupying the ground state of the following Hamiltonian:
\begin{equation}\label{eq:hamiltonian}
    \mathcal{H} = -\frac{\hbar^2}{2m} \sum_{i=1}^{N} \nabla_i^2 + \sum_{i<j} V(r_{ij})\,,
\end{equation}
where $m \approx \SI{4.003}{amu}$ is the atomic mass, $\mathbf{r}_i$ is the position of the $i$-th particle, $r_{ij} \equiv \left|\mathbf{r}_i - \mathbf{r}_j\right|$. The interatomic interactions are modeled by an effective Lennard-Jones (LJ) potential:
\begin{equation}\label{eq:lj_potential}
    V(r) = 4\varepsilon \left[\left(\frac{\sigma}{r}\right)^{12} - \left(\frac{\sigma}{r}\right)^{6}\right],
\end{equation}
where $\varepsilon / k_B = \SI{10.22}{\kelvin}$ and $\sigma = \SI{0.2556}{\nano\meter}$.

Superfluid \ce{^4He} shows a saturation property, i.e. it exhibits a finite density when no external pressure is applied. This saturation density is experimentally measured around $\rho_{exp} = \SI{21.86}{\nano\meter^{-3}}$. Our main aim is to obtain a theoretical estimation of this quantity through our VMC calculation.

As mentioned, the VMC method exploits the variational principle, according to which, given a trial wave function $\psi_{T}\!\left(\{\alpha_k\};\, \mathbf{r}_1,\, \mathbf{r}_2,\, \dots,\, \mathbf{r}_N\right) \equiv \psi_T\!\left(\{\alpha_k\};\, R\right)$, dependent on the set of variational parameters $\{\alpha_k\}$ and on the spatial coordinates $R \equiv \left(\mathbf{r}_1,\, \mathbf{r}_2,\, \dots,\, \mathbf{r}_N\right)$, the following inequality holds:
\begin{equation}\label{eq:var_principle}
    \left\langle \mathcal{H} \right\rangle_{\psi_{T}} = \frac{\int \psi_{T}^*\!\left(\{\alpha_k\};\, R\right) \mathcal{H} \psi_{T}\!\left(\{\alpha_k\};\, R\right) \dd R}{\int \left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2 \dd R}
    = \frac{\int \left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2 E_L(R) \,\dd R}{\int \left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2 \dd R} \ge E_0\,,
\end{equation}
where $E_0$ is the ground state energy, while $E_L(R)$, called local energy, is defined as
\begin{equation}\label{eq:local_energy}
    E_L(R) \equiv \frac{\mathcal{H} \psi_{T}\!\left(\{\alpha_k\};\, R\right)}{\psi_{T}\!\left(\{\alpha_k\};\, R\right)}
    = -\frac{\hbar^2}{2m} \frac{\sum_{i=1}^N\nabla_i^2\psi_{T}\!\left(\{\alpha_k\};\, R\right)}{\psi_{T}\!\left(\{\alpha_k\};\, R\right)} + \sum_{i<j} V(r_{ij})\,.
\end{equation}
For the inequality \eqref{eq:var_principle}, we can estimate the wave function of the system by searching for the values of the variational parameters that minimize the mean value of $E_L$ sampled with the probability density function (pdf) 
\begin{equation}\label{eq:pdf}
    P_{\psi_{T}}(R) = \frac{\left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2}{\int \left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2 \dd R}\,.
\end{equation}
Consequently, for the central limit theorem, if we are able to sample this pdf, we can obtain an estimation of the wave function and of the corresponding ground state energy $E_0$. For this purpose, we exploit the Metropolis algorithm, which will be described in the following section.

In order to check the correctness of the computations, it is useful to consider, together with the local energy \eqref{eq:local_energy}, the so-called Jackson-Feenberg (JF) local energy: 
\begin{equation}\label{eq:JF_local_energy}
    E_L^{JF}(R) \equiv -\frac{\hbar^2}{4m} \sum_{i=1}^N \left[\frac{\nabla_i^2 \psi_{T}\!\left(\{\alpha_k\};\, R\right)}{\psi_{T}\!\left(\{\alpha_k\};\, R\right)} - \left|\frac{\boldsymbol{\nabla}_i \psi_{T}\!\left(\{\alpha_k\};\, R\right)}{\psi_{T}\!\left(\{\alpha_k\};\, R\right)}\right|^2\right] + \sum_{i<j} V(r_{ij})\,.
\end{equation}
If $\psi_{T}^*\!\left(\{\alpha_k\};\, R\right)\sum_{i=1}^N\boldsymbol{\nabla}_i\psi_{T}\!\left(\{\alpha_k\};\, R\right)$ vanishes at the border of the integration volume, the so-called JF identity holds \cite{pederiva}:
\begin{equation}\label{eq:JF_identity}
    \frac{\int \left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2 E_L(R) \,\dd R}{\int \left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2 \dd R} = \frac{\int \left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2 E_L^{JF}(R) \,\dd R}{\int \left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2 \dd R}\,.
\end{equation}