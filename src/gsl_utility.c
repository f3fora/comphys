#include "gsl_utility.h"

// Return the last item of the gsl_vector vec.
double last_value(const gsl_vector *vec)
{
    return gsl_vector_get(vec, vec->size - 1);
}

/*
 * Sum two gsl_vectors.
 * res: sum of the two vectors [output]
 * vec_1: 1st addend
 * vec_2: 2nd addend
 */
int sum_vectors(gsl_vector *res, const gsl_vector *vec_1, const gsl_vector *vec_2)
{
    size_t i;

    for (i = 0; i < res->size; i++) {
        gsl_vector_set(res, i, gsl_vector_get(vec_1, i) + gsl_vector_get(vec_2, i));
    }

    return 0;
}

/*
 * Multiply two gsl_vectors.
 * res: product of the two vectors [output]
 * vec_1: 1st factor
 * vec_2: 2nd factor
 */
int multiply_vectors(gsl_vector *res, const gsl_vector *vec_1, const gsl_vector *vec_2)
{
    size_t i;

    for (i = 0; i < res->size; i++) {
        gsl_vector_set(res, i, gsl_vector_get(vec_1, i) * gsl_vector_get(vec_2, i));
    }

    return 0;
}

/*
 * Raise each element of a gsl_vector to a power.
 * res: power [output]
 * vec: base
 * exponent: exponent
 */
int pow_vector(gsl_vector *res, const gsl_vector *vec, const double exponent)
{
    size_t i;

    for (i = 0; i < res->size; i++) {
        gsl_vector_set(res, i, pow(gsl_vector_get(vec, i), exponent));
    }

    return 0;
}

/*
 * Set the elements of a gsl_vector between two indices (included) equal to a certain value.
 * vec: gsl_vector to modify [output]
 * first_item: 1st index
 * last_item: 2nd index
 * value: value to set
 * return: if first_item and last_item are less than the size of vec: 0
 *         else: 1
 */
int set_value(gsl_vector *vec, const size_t first_item, const size_t last_item, const double value)
{
    if (first_item < vec->size && last_item < vec->size) {
        gsl_vector_view vec_view =
                gsl_vector_subvector(vec, first_item, last_item - first_item + 1);
        gsl_vector_set_all(&vec_view.vector, value);

        return 0;
    }

    return 1;
}

/*
 * Integrate a function through the composite Simpson’s 1/3 rule.
 * func: gsl_vector of odd size containing the function to integrate
 * h: integration step
 * return: integral
 */
double integral(const gsl_vector *func, const double h)
{
    if (func->size <= 1) {
        return 0.;
    } else if (func->size <= 2) {
        return (gsl_vector_get(func, 0) + last_value(func)) * h * 0.5;
    }

    size_t i;
    double integ = gsl_vector_get(func, 0) + last_value(func);

    for (i = 1; i < func->size - 1; i += 2) {
        integ += 4. * gsl_vector_get(func, i);
    }

    for (i = 2; i < func->size - 1; i += 2) {
        integ += 2. * gsl_vector_get(func, i);
    }

    integ *= h / 3.;

    return integ;
}

/*
 * Compute the 2nd derivative of a function through seven-point formulas.
 * der: 2nd derivative [output]
 * func: function to derive
 * h: discretization step
 */
int second_derivative(gsl_vector *der, const gsl_vector *func, const double h)
{
    size_t i;
    double den = 180. * h * h;

    // clang-format off
    gsl_vector_set(der, 0,
                   (812. * gsl_vector_get(func, 0) - 3132. * gsl_vector_get(func, 1) +
                    5265. * gsl_vector_get(func, 2) - 5080. * gsl_vector_get(func, 3) +
                    2970. * gsl_vector_get(func, 4) - 972. * gsl_vector_get(func, 5) +
                    137. * gsl_vector_get(func, 6)) / den);

    gsl_vector_set(der, 1,
                   (137. * gsl_vector_get(func, 0) - 147. * gsl_vector_get(func, 1) -
                    255. * gsl_vector_get(func, 2) + 470. * gsl_vector_get(func, 3) -
                    285. * gsl_vector_get(func, 4) + 93. * gsl_vector_get(func, 5) -
                    13. * gsl_vector_get(func, 6)) / den);

    gsl_vector_set(der, 2,
                   (-13. * gsl_vector_get(func, 0) + 228. * gsl_vector_get(func, 1) -
                    420. * gsl_vector_get(func, 2) + 200. * gsl_vector_get(func, 3) +
                    15. * gsl_vector_get(func, 4) - 12. * gsl_vector_get(func, 5) +
                    2. * gsl_vector_get(func, 6)) / den);

    for (i = 3; i < func->size - 3; i++) {
        gsl_vector_set(der, i,
                       (2. * gsl_vector_get(func, i - 3) - 27. * gsl_vector_get(func, i - 2) +
                        270. * gsl_vector_get(func, i - 1) - 490. * gsl_vector_get(func, i) +
                        270. * gsl_vector_get(func, i + 1) - 27. * gsl_vector_get(func, i + 2) +
                        2. * gsl_vector_get(func, i + 3)) / den);
    }

    gsl_vector_set(der, func->size - 3,
                   (2. * gsl_vector_get(func, func->size - 7) -
                    12. * gsl_vector_get(func, func->size - 6) +
                    15. * gsl_vector_get(func, func->size - 5) +
                    200. * gsl_vector_get(func, func->size - 4) -
                    420. * gsl_vector_get(func, func->size - 3) +
                    228. * gsl_vector_get(func, func->size - 2) -
                    13. * gsl_vector_get(func, func->size - 1)) / den);

    gsl_vector_set(der, func->size - 2,
                   (-13. * gsl_vector_get(func, func->size - 7) +
                    93. * gsl_vector_get(func, func->size - 6) -
                    285. * gsl_vector_get(func, func->size - 5) +
                    470. * gsl_vector_get(func, func->size - 4) -
                    255. * gsl_vector_get(func, func->size - 3) -
                    147. * gsl_vector_get(func, func->size - 2) +
                    137. * gsl_vector_get(func, func->size - 1)) / den);

    gsl_vector_set(der, func->size - 1,
                   (137. * gsl_vector_get(func, func->size - 7) -
                    972. * gsl_vector_get(func, func->size - 6) +
                    2970. * gsl_vector_get(func, func->size - 5) -
                    5080. * gsl_vector_get(func, func->size - 4) +
                    5265. * gsl_vector_get(func, func->size - 3) -
                    3132. * gsl_vector_get(func, func->size - 2) +
                    812. * gsl_vector_get(func, func->size - 1)) / den);
    // clang-format on

    return 0;
}

/*
 * Check if a gsl_vector contains a nan (not a number).
 * vec: gsl_vector to check
 * return: if vec contains at least one nan: true
 *         else: false
 */
bool is_there_a_nan(gsl_vector *vec)
{
    size_t i;

    for (i = 0; i < vec->size; i++) {
        if (isnan(gsl_vector_get(vec, i))) {
            return true;
        }
    }

    return false;
}
