\subsection{Beryllium} \label{sec:beryllium}

A more significant example of application of the Hartree-Fock method is the computation of the ground state properties of the beryllium atom (N = Z = 4), having electronic configuration $1s^2 2s^2$, i.e. two fully occupied orbitals.

As a basis on which to expand the orbitals, this time we consider an STO-8G basis, formed by $8$ Gaussians defined as in \eqref{eq:gaussians}, with the $\alpha$-coefficients reported in \autoref{tab:alpha_beta_Be}. As in the He case, the HFR equations are given by \eqref{eq:HFR}, with the overlap matrix $S_{qp}$, the one-body matrix $h_{qp}$ and the integrals \eqref{eq:ERI} given respectively by the formulas \eqref{eq:overlap_matrix_H}, \eqref{eq:one_body_matrix_H} (with $Z = 4$) and \eqref{eq:ERI_gaussians}.  

An alternative approach involves the use of a basis of $2$ contracted orbitals, given by $2$ fixed linear combinations of Gaussians:
\begin{equation}\label{eq:contracted_basis}
    \zeta_{p}(\mathbf{r}) = \sum_{i=4p+1}^{4(p+1)}\beta_i e^{-\alpha_i r^2}\,,\quad \text{with }p = 0,\,1,
\end{equation}
where the coefficients $\alpha_i$ and $\beta_i$ ($i = 1,\,2,\,\dots,\,8$) are reported in \autoref{tab:alpha_beta_Be}.

\begin{table}[H]
\centering
\begin{tabular}{cc}
\toprule
$\alpha$        &$\beta$ \\
\midrule
70.64859542     &0.05675242080 \\
12.92782254     &0.2601413550 \\
3.591490662     &0.5328461143 \\
1.191983464     &0.2916254405 \\
\midrule
3.072833610     &-0.06220714565 \\
0.6652025433    &0.00002976804596 \\
0.2162825386    &0.5588549221 \\
0.08306680972   &0.4977673218 \\
\bottomrule
\end{tabular}
\vspace{1em}
\caption{The first column shows the $\alpha$-coefficients defining the Gaussian basis functions on which the Be orbitals are expanded. The second one shows the corresponding $\beta$-coefficients defining the $2$ contractions of Gaussians considered in the computation with contracted orbitals.}
\label{tab:alpha_beta_Be}
\end{table}

In the case of contracted orbitals, the overlap matrix becomes
\begin{equation}
    S_{qp} = \int \zeta_q(\mathbf{r}) \zeta_p(\mathbf{r})\,\text{d}\mathbf{r} = \sum_{i=4q+1}^{4(q+1)}\sum_{j=4p+1}^{4(p+1)} \beta_i \beta_j \left(\frac{\pi}{\alpha_i + \alpha_j}\right)^{\sfrac{3}{2}},
\end{equation}
the one-body matrix reads
\begin{equation}
    \begin{aligned}
        h_{qp} &= \int \zeta_q(\mathbf{r}) \left(-\frac{1}{2} \nabla^2 - \frac{4}{r}\right) \zeta_p(\mathbf{r}) \,\text{d}\mathbf{r} \\
        &= \sum_{i=4q+1}^{4(q+1)}\sum_{j=4p+1}^{4(p+1)} \beta_i\beta_j\left(3\frac{\alpha_i\alpha_j\pi^{3/2}}{(\alpha_i + \alpha_j)^{5/2}} - \frac{8\pi}{\alpha_i + \alpha_j}\right),
    \end{aligned}
\end{equation}
while the integrals \eqref{eq:ERI} are equal to
\begin{align}
    \left(qn\left|\frac{1}{|\mathbf{r}-\mathbf{r}'|}\right|pm\right) &= \iint\zeta_q(\mathbf{r})\zeta_n(\mathbf{r}')\frac{1}{|\mathbf{r}-\mathbf{r}'|}\zeta_p(\mathbf{r})\zeta_m(\mathbf{r}')\,\text{d}\mathbf{r}\text{d}\mathbf{r}' \\ 
    &= \sum_{i=4q+1}^{4(q+1)}\sum_{j=4p+1}^{4(p+1)}\sum_{k=4n+1}^{4(n+1)}\sum_{\ell=4m+1}^{4(m+1)}\frac{2\pi^{5/2}\beta_i\beta_j\beta_k\beta_{\ell}}{(\alpha_i + \alpha_j)(\alpha_k + \alpha_{\ell})\sqrt{\alpha_i + \alpha_j + \alpha_k + \alpha_{\ell}}}\,. \nonumber
\end{align}