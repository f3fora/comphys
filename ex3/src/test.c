#include <stdlib.h>
#include <stdio.h>
#include "dft_structs.h"
#include "dft.h"

// Parameters

#define r_s_Na 3.93
#define r_s_K 4.86

#define NUM_CS_SYSTEMS 6

#define M 28001
#define M_CUT 28001
#define THRESH_NUMEROV 1e-6
#define DE 0.01
#define R_LOW 1e-4
#define H 1e-3

#define N_SC 40
#define MIX 0.2
#define THRESH_SCF 1e-3

//
// Main for testing the functions of dft.h
int main()
{
    size_t i, j, first_idx = 0, succ_first_idx;
    int out;

    size_t N[NUM_CS_SYSTEMS];
    double E_tot[NUM_CS_SYSTEMS];
    double r[M];
    double V_ext[NUM_CS_SYSTEMS * M];
    double rho[NUM_CS_SYSTEMS * M_CUT];
    size_t levels_sizes[NUM_CS_SYSTEMS];
    Levels levels;
    NumerovParams np = { THRESH_NUMEROV, DE, R_LOW, H };

    size_t num_iter;
    double E_tot_sc;

    double V_edxc[M];
    double rho_sc[M_CUT];

    out = compute_closed_shell_systems(NUM_CS_SYSTEMS, r_s_Na, np, M, M_CUT, N, E_tot, r, V_ext,
                                       rho, &levels, levels_sizes);

    printf("CLOSED-SHELL SYSTEMS\nReturn: %d\n\n", out);

    for (i = 0; i < NUM_CS_SYSTEMS; i++) {
        printf("Closed-shell system %ld\n", i);
        printf("N = %ld\n", N[i]);
        printf("E_tot = %f\n", E_tot[i]);
        printf("Levels:\nn\tl\tE\n");

        succ_first_idx = first_idx + levels_sizes[i];
        for (j = first_idx; j < succ_first_idx; j++) {
            printf("%ld\t%ld\t%f\n", levels.n[j], levels.l[j], levels.E[j]);
        }
        first_idx = succ_first_idx;

        printf("\n");
    }

    printf("\nSELF-CONSISTENT METHOD WITH N = %d\n", N_SC);

    out = self_consistent_method(N_SC, r_s_Na, np, M, M_CUT, MIX, THRESH_SCF, &E_tot_sc, r, V_edxc,
                                 rho_sc, &levels, &num_iter);

    printf("\nReturn: %d\nNumber of iterations: %ld\n\n", out, num_iter);

    printf("E_tot = %f\n\n", E_tot_sc);
    printf("Levels:\nn\tl\tE\n");

    for (i = 0; i < levels.size; i++) {
        printf("%ld\t%ld\t%f\n", levels.n[i], levels.l[i], levels.E[i]);
    }

    printf("\n");

    return 0;
}
