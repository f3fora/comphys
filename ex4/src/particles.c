#include "particles.h"

/*
 * Place N = 4 * n_cells particles in a fcc lattice.
 * particles: pointer to a Particle N-array, where the positions of the particles will be stored
 *            [output]
 * l: side of an elementary cubic box
 * n_cells: number of elementary cubic boxes
 */
int place_particles(Particle *particles, const double l, const size_t n_cells)
{
    size_t i = 0;
    double l_half = 0.5 * l;
    double L_half = l_half * pow(n_cells, 1. / 3.);
    double r_max = L_half - l_half;
    double x, y, z;

    for (x = -L_half; x < r_max; x += l) {
        for (y = -L_half; y < r_max; y += l) {
            for (z = -L_half; z < r_max; z += l) {
                particles[i].r[0] = x;
                particles[i].r[1] = y;
                particles[i].r[2] = z;
                i++;

                particles[i].r[0] = x + l_half;
                particles[i].r[1] = y + l_half;
                particles[i].r[2] = z;
                i++;

                particles[i].r[0] = x + l_half;
                particles[i].r[1] = y;
                particles[i].r[2] = z + l_half;
                i++;

                particles[i].r[0] = x;
                particles[i].r[1] = y + l_half;
                particles[i].r[2] = z + l_half;
                i++;
            }
        }
    }

    return 0;
}

/*
 * Apply the Born-von Karman periodic boundary conditions.
 * x: coordinate to transform
 * L: side of the simulation box
 * return: the trasformed coordinate
 */
double pbc(const double x, const double L)
{
    return x - L * rint(x / L);
}

/*
 * Compute the squared distance between two particles.
 * particle_1: pointer to the Particle variable containing the position of the 1st particle
 * particle_2: pointer to the Particle variable containing the position of the 2nd particle
 * L: side of the simulation box
 * return: squared distance
 */
double compute_squared_distance(const Particle *particle_1, const Particle *particle_2,
                                const double L)
{
    size_t i;
    double dr_squared = 0.;

    for (i = 0; i < 3; i++) {
        dr_squared += pow(pbc(particle_1->r[i] - particle_2->r[i], L), 2);
    }

    return dr_squared;
}

/*
 * Create an MPI data type for the communication of variables and arrays of type Particle among
 * different processes of a parallel computation.
 * return: Particle MPI data type
 */
MPI_Datatype create_Particle_mpiDatatype()
{
    MPI_Datatype mpi_Particle;
    int block_lengths[1] = { 3 };
    MPI_Aint displacements[1] = { offsetof(Particle, r) };
    MPI_Datatype types[1] = { MPI_DOUBLE };

    MPI_Type_create_struct(1, block_lengths, displacements, types, &mpi_Particle);
    MPI_Type_commit(&mpi_Particle);

    return mpi_Particle;
}
