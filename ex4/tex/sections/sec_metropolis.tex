\subsection{Metropolis Algorithm} \label{sec:metropolis}

The Metropolis algorithm, also known as Metropolis-Hastings algorithm, allows one to efficiently sample sets of positions $R$ distributed with pdf $P_{\psi_{T}}(R)$.

The algorithm is iterative\footnote{For more details on the Metropolis algorithm, see for example \cite{pederiva}.}. It starts from an initial configuration $R_0$. At each iteration, given the configuration $R_i \equiv R$, a configuration $R'$ is generated with uniform pdf
\begin{equation}
    W(R,\,R') =
    \begin{dcases}
        \frac{1}{\Delta^{3N}} \; &\text{if } \left|r'_{j,k} - r_{j,k}\right| < \frac{\Delta}{2} \;\;\forall\,j = 1,\,2,\,\dots,\,N,\;\forall\,k = x,\,y,\,z\\
        0 \; &\text{otherwise}
    \end{dcases}
\end{equation}
where $\Delta > 0$ is a fixed parameter. The generation of the configuration $R'$ is implemented through the extraction of random numbers $\xi_{j,k}$ uniformly distributed in $[0,\,1)$:
\begin{equation}
    r'_{j,k} = r_{j,k} + \Delta \left(\xi_{j,k} - \frac{1}{2}\right) \quad \forall\,j = 1,\,2,\,\dots,\,N,\;\forall\,k = x,\,y,\,z.
\end{equation}
The proposed configuration $R'$ is accepted with probability
\begin{equation}
    A(R,\,R') = \min\!\left(\frac{P_{\psi_{T}}(R')}{P_{\psi_{T}}(R)},\,1\right) = \min\!\left(\frac{\left|\psi_{T}\!\left(\{\alpha_k\};\, R'\right)\right|^2}{\left|\psi_{T}\!\left(\{\alpha_k\};\, R\right)\right|^2},\,1\right).
\end{equation}
Therefore, in order to establish the new configuration $R_{i+1}$, a new random number $\zeta$, uniformly distributed in $[0,\,1)$, is extracted. If $\zeta < A(R,\,R')$, then $R_{i+1} = R'$, else $R_{i+1} = R_i$.

The configurations generated in this way are distributed with pdf $P_{\psi_{T}}(R)$ only in the $i \to +\infty$ limit. Consequently, we consider only the configurations sampled during the so-called statistical phase, after an initial equilibration phase of $M_{eq}$ steps, necessary for the system to lose memory of the initial conditions and for the sampled probability distribution to converge to the desired one.

In order to estimate the mean value, and the corresponding statistical error, of the local energy $E_L(R)$, at each step of the algorithm we evaluate $E_L$ on the configuration $R_i$. After $M_{stat}$ iterations, with $M_{stat}$ large enough to reduce statistical errors as desired, the mean value of $E_L$ is estimated by the quantity
\begin{equation}\label{eq:mean_local_energy}
    \left\langle E_L \right\rangle \approx \frac{1}{M_{stat}} \sum_{i=1}^{M_{stat}} E_L(R_i)\,,
\end{equation}
affected by a statistical error equal to
\begin{equation}\label{eq:std_local_energy}
    \begin{aligned}
        \sigma_{E_L} &\approx \sqrt{\frac{\bar{\tau}}{M_{stat} - 1}\left(\left\langle E_L^2 \right\rangle - \left\langle E_L \right\rangle^2\right)} \\
        &= \sqrt{\frac{\bar{\tau}}{M_{stat} - 1}\left(\frac{1}{M_{stat}} \sum_{i=1}^{M_{stat}} E_L^2(R_i) - \left\langle E_L \right\rangle^2\right)}\,,
    \end{aligned}
\end{equation}
where $\bar{\tau}$ is the typical autocorrelation length of the generated chain of configurations, characterized by an autocorrelation coefficient defined as
\begin{equation}\label{eq:autocorrelation}
    C_{E_L}(\tau) \equiv \frac{\left\langle E_L(R_i) E_L(R_{i+\tau}) \right\rangle - \left\langle E_L \right\rangle^2}{\left\langle E_L^2 \right\rangle - \left\langle E_L \right\rangle^2} \sim e^{-\tau / \bar{\tau}}\,,
\end{equation}
where the averages are calculated on the steps of the algorithm.

In order to obtain an estimation of the autocorrelation length $\bar{\tau}$, we fit the function
\begin{equation}\label{eq:fit_tau}
    \log\!\left(C_{E_L}(\tau)\right) = \alpha \tau\,,
\end{equation}
where $\alpha$ is the only parameter to be optimized. The desired quantity is then obtained as $\bar{\tau} = -\alpha^{-1}$.

The same procedure is applied to the estimation of the mean value of the JF local energy $E_L^{JF}$.

The parameter $\Delta$ is set in the equilibration phase, during which it is dynamically modified in order to reach an optimal value for which the acceptance rate $\eta$ (fraction of proposals accepted during the execution of the Metropolis algorithm) belongs to a fixed interval. This interval is chosen in order to minimize the autocorrelation lengths (and therefore the uncertainties) of the computed quantities.

Finally, in order to save computational time, the calculation is parallelized, distributing the tasks among $n_{procs}$ processes\footnote{The benefits, in terms of execution time, of the code parallelization are briefly discussed in \autoref{app:exec_time}.}. The master process performs the equilibration phase, making the system evolve from the initial configuration to its thermalization, and setting the optimal value of the parameter $\Delta$. Subsequently, it communicates to the other processes the last sampled configuration, its probability and the value of $\Delta$. The various processes then set a different seed for the random number generation, and make the system evolve through the Metropolis algorithm for $M_{eq}^{procs}$ steps, in order to make the configurations sampled by different processes independent. The subsequent statistical phase is then performed in parallel by the different processes, which singularly execute $\sim M_{stat} / n_{procs}$ iterations. The results obtained by each of them are finally averaged to obtain the final results.