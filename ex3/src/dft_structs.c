#include "dft_structs.h"

/*
 * Initialize a variable of type Levels.
 * levels: pointer to the Levels variable to initialize [output]
 * M: value to assign to levels->M
 */
int Levels_initialize(Levels *levels, const size_t M)
{
    levels->size = 0;
    levels->M = M;

    levels->orbitals = NULL;
    levels->E = NULL;
    levels->n = NULL;
    levels->l = NULL;

    return 0;
}

/*
 * Allocate a variable of type Levels.
 * levels: pointer to the Levels variable to allocate [output]
 * size: number of energy levels that the variable pointed by levels will contain
 */
int Levels_alloc(Levels *levels, const size_t size)
{
    levels->size = size;

    levels->orbitals = malloc(levels->size * levels->M * sizeof(double));
    levels->E = malloc(levels->size * sizeof(double));
    levels->n = malloc(levels->size * sizeof(size_t));
    levels->l = malloc(levels->size * sizeof(size_t));

    return 0;
}

/*
 * Reallocate a variable of type Levels.
 * levels: pointer to the Levels variable to reallocate [output]
 * size_increase: increase in the number of energy levels that the variable pointed to by levels
 *                contains
 */
int Levels_realloc(Levels *levels, const size_t size_increase)
{
    levels->size += size_increase;

    levels->orbitals = realloc(levels->orbitals, levels->size * levels->M * sizeof(double));
    levels->E = realloc(levels->E, levels->size * sizeof(double));
    levels->n = realloc(levels->n, levels->size * sizeof(size_t));
    levels->l = realloc(levels->l, levels->size * sizeof(size_t));

    return 0;
}

/*
 * Free a variable of type Levels.
 * levels: pointer to the Levels variable to free
 */
int Levels_free(Levels *levels)
{
    free(levels->orbitals);
    free(levels->E);
    free(levels->n);
    free(levels->l);

    levels->orbitals = NULL;
    levels->E = NULL;
    levels->n = NULL;
    levels->l = NULL;

    levels->size = 0;

    return 0;
}

/*
 * Put a new energy level in a specific position of the arrays that constitute a Levels variable.
 * levels: pointer to the Levels variable to update [output]
 * idx: index that identifies the position in the arrays where to put the new energy level
 * orbital: (levels->M)-gsl_vector containing the orbital
 * E: eigenenergy
 * n: principal quantum number
 * l: orbital angular momentum quantum number
 */
int Levels_set(Levels *levels, const size_t idx, const gsl_vector *orbital, const double E,
               const size_t n, const size_t l)
{
    size_t i, j;

    for (i = idx * levels->M, j = 0; j < levels->M; i++, j++) {
        levels->orbitals[i] = gsl_vector_get(orbital, j);
    }

    levels->E[idx] = E;
    levels->n[idx] = n;
    levels->l[idx] = l;

    return 0;
}

/*
 * Reallocate a Levels variable adding a new energy level.
 * levels: pointer to the Levels variable to reallocate [output]
 * orbital: (levels->M)-gsl_vector containing the orbital
 * E: eigenenergy
 * n: principal quantum number
 * l: orbital angular momentum quantum number
 */
int Levels_add(Levels *levels, const gsl_vector *orbital, const double E, const size_t n,
               const size_t l)
{
    size_t idx = levels->size;

    Levels_realloc(levels, 1);
    Levels_set(levels, idx, orbital, E, n, l);

    return 0;
}

/*
 * Append to a Levels variable another Levels variable.
 * self_levels: pointer to the Levels variable to reallocate [output]
 * other_levels: pointer to the Levels variable to append
 */
int Levels_append(Levels *self_levels, const Levels *other_levels)
{
    size_t last_size = self_levels->size;

    Levels_realloc(self_levels, other_levels->size);

    memcpy(self_levels->orbitals + last_size * self_levels->M, other_levels->orbitals,
           other_levels->size * other_levels->M * sizeof(double));
    memcpy(self_levels->E + last_size, other_levels->E, other_levels->size * sizeof(double));
    memcpy(self_levels->n + last_size, other_levels->n, other_levels->size * sizeof(size_t));
    memcpy(self_levels->l + last_size, other_levels->l, other_levels->size * sizeof(size_t));

    return 0;
}

/*
 * Return a gsl_vector_view of an orbital stored in a Levels variable.
 * levels: pointer to a Levels variable
 * idx: index that identifies the orbital to return
 * return: gsl_vector_view of the desired orbital
 */
gsl_vector_view Levels_get_orbital(const Levels *levels, const size_t idx)
{
    return gsl_vector_view_array(levels->orbitals + idx * levels->M, levels->M);
}
