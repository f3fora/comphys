#ifndef VMC_H
#define VMC_H

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <math.h>
#include <mpi.h>
#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_const_num.h>
#include "particles.h"
#include "circ_array.h"
#include "autocorrelation.h"

// Conversion factors
#define hbar GSL_CONST_MKSA_PLANCKS_CONSTANT_HBAR
#define k_B GSL_CONST_MKSA_BOLTZMANN
#define nm GSL_CONST_NUM_NANO
#define amu GSL_CONST_MKSA_UNIFIED_ATOMIC_MASS

// Parameters
#define He4_mass (4.002603254 * amu) // kg
#define sigma (0.2556 * nm) // m
#define epsilon (10.22 * k_B) // J

// hbar^2 / (2 * mass) in dimensionless units
#define B (0.5 * hbar * hbar / (He4_mass * sigma * sigma * epsilon))

/* Proportional to the number of Metropolis steps between two prints of some info during the
 * equilibration phase */
#define STEPS_PRINT_EQ 10

// Number of Metropolis steps between two prints of some info during the statistical phase
#define STEPS_PRINT_STAT 50000

int vmc(const size_t, const double, const size_t, const double, const size_t, const size_t,
        const size_t, const size_t, const size_t, const double, const double, const double,
        const double, const size_t, double *, double *, double *, double *, double *, double *,
        double *, double *, double *, double *, double *, double *);

#endif
