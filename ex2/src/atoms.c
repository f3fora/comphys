#include "atoms.h"

/*
 * Compute the ground state energy and the corresponding wave function of a hydrogen-like atom,
 * applying the variational method to a trial wave function which is a linear combination of M
 * Gaussians.
 * INPUTS:
 * alpha: pointer to a double M-array containing the coefficients which define the M Gaussians whose
 *        linear combination is the trial wave function
 * M: number of Gaussians (size of the array pointed to by alpha)
 * Z: nuclear charge
 * OUTPUTS:
 * energy: pointer to a double variable, where the estimated ground state energy will be stored
 * coeff: pointer to a double M-array, where the coefficients of the linear combination of Gaussians
 *        that best approximates the ground state wave function will be stored
 */
int compute_hydrogen(const double *alpha, const size_t M, const size_t Z, double *energy,
                     double *coeff)
{
    gsl_matrix *H = gsl_matrix_alloc(M, M);
    gsl_matrix *S = gsl_matrix_alloc(M, M);
    gsl_vector_view C = gsl_vector_view_array(coeff, M);

    hamilton(H, alpha, Z);
    overlap_matrix(S, alpha);

    *energy = find_ground_state(&C.vector, H, S);
    normalize(&C.vector, alpha);

    gsl_matrix_free(H);
    gsl_matrix_free(S);

    return 0;
}

/*
 * Compute the ground state energy and the coefficients defining the N/2 occupied orbitals of a
 * neutral atom with an even number N > 1 of electrons, solving the Hartree-Fock-Roothaan equations,
 * written on a basis of M Gaussians, through a self-consistent field calculation.
 * This function uses the function self_consistent_method defined in hartree_fock.h.
 * INPUTS:
 * alpha: pointer to a double M-array containing the coefficients which define the Gaussians that
 *        form the considered basis
 * M: number of basis functions (size of the array pointed to by alpha)
 * N: number of electrons (it must be N > 1 even)
 * mix: mixing parameter
 * thresh: threshold for evaluating the convergence of the self-consistent field calculation
 * OUTPUTS:
 * energy: pointer to a double variable, where the estimated ground state energy will be stored
 * coeff: pointer to a double (M * N/2)-array, where the coefficients defining the orbitals will be
 *        stored; the array will contain the m-th coefficient of the n-th orbital in the position
 *        n + m * N/2 (with n = 0, ..., N/2-1, m = 0, 1, ..., M-1)
 * return: if the algorithm converges within NUM_ITER_MAX (defined in hartree_fock.h) iterations:
 *             number of iterations required for the convergence
 *         else:
 *             0
 */
size_t compute_atom(const double *alpha, const size_t M, const size_t N, const double mix,
                    const double thresh, double *energy, double *coeff)
{
    size_t num_iter;

    gsl_matrix_view C = gsl_matrix_view_array(coeff, M, N / 2);
    gsl_matrix *density_0 = gsl_matrix_alloc(M, M);

    gsl_matrix_set_zero(density_0);

    num_iter = self_consistent_method(energy, &C.matrix, alpha, M, N, density_0, mix, thresh);

    gsl_matrix_free(density_0);

    return num_iter;
}

/*
 * Compute the ground state energy and the coefficients defining the N/2 occupied orbitals of a
 * neutral atom with an even number N > 1 of electrons, solving the Hartree-Fock-Roothaan equations,
 * written on a basis of M1 contractions of M2 Gaussians, through a self-consistent field
 * calculation.
 * This function uses the function self_consistent_method_contr defined in hartree_fock_contracted.h.
 * INPUTS:
 * alpha: pointer to a double M-array containing the coefficients which define the M = M1 * M2
 *        Gaussians whose contractions form the considered basis
 * beta: pointer to a double M-array containing the coefficients which define the M1 contractions of
 *       Gaussians that form the considered basis; each contraction is constructed with M2 = M / M1
 *       Gaussians
 * M1: number of contractions (i.e. number of basis functions)
 * M2: number of Gaussians with which each contraction is constructed
 * N: number of electrons (it must be N > 1 even)
 * mix: mixing parameter
 * thresh: threshold for evaluating the convergence of the self-consistent field calculation
 * OUTPUS:
 * energy: pointer to a double variable, where the estimated ground state energy will be stored
 * coeff: pointer to a double (M1 * N/2)-array, where the coefficients defining the orbitals will be
 *        stored; the array will contain the m-th coefficient of the n-th orbital in the position
 *        n + m * N/2 (with n = 0, ..., N/2-1, m = 0, 1, ..., M1-1)
 * return: if the algorithm converges within NUM_ITER_MAX (defined in hartree_fock.h) iterations:
 *             number of iterations required for the convergence
 *         else:
 *             0
 */
size_t compute_atom_contr(const double *alpha, const double *beta, const size_t M1, const size_t M2,
                          const size_t N, const double mix, const double thresh, double *energy,
                          double *coeff)
{
    size_t num_iter;

    gsl_matrix_view C = gsl_matrix_view_array(coeff, M1, N / 2);
    gsl_matrix *density_0 = gsl_matrix_alloc(M1, M1);

    gsl_matrix_set_zero(density_0);

    num_iter = self_consistent_method_contr(energy, &C.matrix, alpha, beta, M1, M2, N, density_0,
                                            mix, thresh);

    gsl_matrix_free(density_0);

    return num_iter;
}
