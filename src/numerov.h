#ifndef NUMEROV_H
#define NUMEROV_H

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include "gsl_utility.h"

// Maximum number of iterations for the secant algorithm, implemented by _secant_method
#define MAX_SEC 1000

int set_coordinate(gsl_vector *, const double, const double);
int numerov(gsl_vector *, const gsl_vector *, const double, const double, const double);
int find_next_eigen(double *, gsl_vector *, const gsl_vector *, const double, const double,
                    const double, const double, const double);

#endif
