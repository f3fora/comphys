#ifndef AUTOCORRELATION_H
#define AUTOCORRELATION_H

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <gsl/gsl_fit.h>
#include "circ_array.h"

int initialize_autocorr(double *, const size_t);
int update_autocorr(double *, const Circ_array *);
int normalize_autocorr(double *, const size_t, const double, const size_t);
int compute_tau(double *, double *, const double *, const size_t);
int save_autocorr(const double *, const size_t, const char *);

#endif
