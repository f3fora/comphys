#ifndef DFT_STRUCTS_H
#define DFT_STRUCTS_H

#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_vector.h>

/*
 * Parameters for the Numerov algorithm and the search for the energy levels.
 * thresh: convergence threshold -> twice the error on the eigenenergies
 * dE: energy increase for the search for the eigenenergies
 * r_low: 1st point of the mesh
 * h: discretization step
 * For more details, see the library numerov.h.
 */
typedef struct NumerovParams {
    double thresh;
    double dE;
    double r_low;
    double h;
} NumerovParams;

/*
 * Energy levels.
 * orbitals: pointer to a double (size * M)-array containing the orbitals
 * E: pointer to a double (size)-array containing the eigenenergies
 * n: pointer to a size_t (size)-array containing the values of the principal quantum number
 * l: pointer to a szie_t (size)-array containing the values of the orbital angular momentum
 *    quantum number
 * size: number of energy levels
 * M: number of points of the mesh on which the orbitals are discretized
 */
typedef struct Levels {
    double *orbitals;
    double *E;
    size_t *n;
    size_t *l;
    size_t size;
    size_t M;
} Levels;

int Levels_initialize(Levels *, const size_t);
int Levels_alloc(Levels *, const size_t);
int Levels_realloc(Levels *, const size_t);
int Levels_free(Levels *);
int Levels_set(Levels *, const size_t, const gsl_vector *, const double, const size_t,
               const size_t);
int Levels_add(Levels *, const gsl_vector *, const double, const size_t, const size_t);
int Levels_append(Levels *, const Levels *);
gsl_vector_view Levels_get_orbital(const Levels *, const size_t);

#endif
