\subsection{Interacting Electrons} \label{sec:interacting_electrons}

In this section, we show the results obtained from the DFT applied to closed-shell Na and K clusters with $N = 8,\,20,\,40$ valence electrons, as explained in \autoref{sec:dft}.

\autoref{fig:potentialsNa} and \autoref{fig:potentialsK} show the mean-field potentials \eqref{eq:V_KS} obtained at the end of the SCF procedure. As expected, the interactions among electrons make the potential wells shallower, in addition to modifying their shape, especially for low values of $r$ (compare with external potential \eqref{eq:V_ext}).

\inputfigure[0.853]{potentialsNa}%
{Interacting Electrons: Mean-Field Potential for Closed-Shell Na Clusters}%
{The figure shows the mean-field potential $V_{KS}(r)$ (equation \eqref{eq:V_KS}) obtained for closed-shell Na clusters with $N = 8,\,20,\,40$ valence electrons at the end of the SCF procedure illustrated in \autoref{fig:flowchart}, by setting $r_{low} = 10^{-4}\,a_0$, $h = 10^{-3}\,a_0$, $M = 28001$, $\epsilon_N = \SI{e-6}{Ha}$, $\xi = 0.2$, $\epsilon_{scf} = \SI{e-5}{Ha}$. The dashed lines represent the mean-field potential for $N = 40$ as reported in the paper \cite{Giai1996} by \citeauthor{Giai1996}, computed from two different models, JM (\textit{jellium} model) and PHJM (pseudo-hamiltonian \textit{jellium} model). Our potential for $N = 40$ is in quite good agreement with the JM potential of the paper.}

\inputfigure[0.853]{potentialsK}%
{Interacting Electrons: Mean-Field Potential for Closed-Shell K Clusters}%
{Same as \autoref{fig:potentialsNa}, for K clusters. Here the algorithm parameters are $r_{low} = 10^{-4}\,a_0$, $h = 10^{-3}\,a_0$, $M = 30001$, $\epsilon_N = \SI{e-6}{Ha}$, $\xi = 0.2$, $\epsilon_{scf} = \SI{5e-6}{Ha}$.}

The comparison with the \citeauthor{Giai1996}'s results \cite{Giai1996} shows a quite good agreement between our mean-field potentials and the JM potentials reported in the paper. The discrepancies for large values of $r$ are attributable to numerical errors due to the eigenvalue search strategy adopted (see Exercise 1 \cite{OurWork}). More interesting are the discrepancies found for low values of $r$, which can be due to the different parametrization of the correlation energy density $\varepsilon_c\!\left[\rho(r)\right]$ adopted in the paper.

\inputfigure[0.87]{densityNa}%
{Interacting Electrons: Electronic Density of Closed-Shell Na Clusters}%
{The figure shows the (normalized) ground state electronic density $\rho(r)$ and the corresponding ground state energy $E_0$ (equation \eqref{eq:E_0_eig}) of closed-shell Na clusters with $N = 8,\,20,\,40$ valence electrons, computed by solving the radial Schrödinger equation \eqref{eq:radial_equation} through the SCF procedure described in \autoref{fig:flowchart}, by setting $r_{low} = 10^{-4}\,a_0$, $h = 10^{-3}\,a_0$, $M = 28001$, $\epsilon_N = \SI{e-6}{Ha}$, $\xi = 0.2$, $\epsilon_{scf} = \SI{e-5}{Ha}$. The dashed lines represent the electronic density for $N = 40$ as reported in the paper \cite{Giai1996} by \citeauthor{Giai1996}, computed from two different models, JM (\textit{jellium} model) and PHJM (pseudo-hamiltonian \textit{jellium} model). Our density for $N = 40$ is in very good agreement with the JM density of the paper.}

\inputfigure[0.87]{densityK}%
{Interacting Electrons: Electronic Density of Closed-Shell K Clusters}%
{Same as \autoref{fig:densityNa}, for K clusters. Here the algorithm parameters are $r_{low} = 10^{-4}\,a_0$, $h = 10^{-3}\,a_0$, $M = 30001$, $\epsilon_N = \SI{e-6}{Ha}$, $\xi = 0.2$, $\epsilon_{scf} = \SI{5e-6}{Ha}$.}

\autoref{fig:densityNa} and \autoref{fig:densityK} show the ground state electronic density $\rho(r)$ and the corresponding ground state energy found for the considered Na and K clusters. As expected, the interactions between electrons increase the ground state energies and make the electronic densities significantly different from zero for larger values of $r$ (compare with \autoref{fig:indepRhoNa} and \autoref{fig:indepRhoK}). The comparison with the \citeauthor{Giai1996}'s results \cite{Giai1996} shows a very good agreement between our densities and the JM densities reported in the paper, despite the differences found when comparing the mean-field potentials.

As a final result, we estimate the polarizability $\alpha(N)$ of the considered metal clusters:
\begin{equation}\label{eq:polarizability}
    \alpha(N) = R_c^3 \left(1 + \frac{\delta N}{N}\right),
\end{equation}
where $\delta N$ is the electron spillout, given by
\begin{equation}\label{eq:spillout}
    \delta N = 4\pi \int_{R_c}^{+\infty} \rho(r)\,r^2\dd r\,.
\end{equation}
The results are presented in \autoref{tab:polarizability}, where they are compared with what is reported in the \citeauthor{Giai1996}'s paper \cite{Giai1996}. As expected from the comparison between the densities, our electron spillouts for $N = 20$ clusters are compatible with the JM electron spillouts reported in the paper.

\begin{table}[H]
    \centering
    \renewcommand\arraystretch{1.3}
    \begin{tabular}{c c|c|c|c|c|c}
        && \multicolumn{3}{c|}{\mbox{}} & \multicolumn{2}{c}{\citeauthor{Giai1996}} \\
        &   $N$    &  $R_c$ $\left[ a_0 \right]$    &   $\delta N $    &   $\alpha$ $\left[ \text{a.u.} \right]$  & $\delta N_{\text{JM}}$ & $\delta N_{\text{PHJM}}$ \\
        \hline
        \multirow{3}{*}{Na} & 8     &   7.860    &   1.517    &   577.67      &   --  &   --  \\
                            & 20    &   10.668   &   2.975    &   1394.57     &   2.9 &   4.3 \\
                            & 40    &   13.440   &   4.981    &   2730.29     &   --  &   --  \\
        \hline
        \multirow{3}{*}{K}  & 8     &   9.720    &   1.283    &   1065.63     &   --  &   --  \\
                            & 20    &   13.192   &   2.532    &   2586.52     &   2.5 &   4.3 \\
                            & 40    &   16.621   &   4.244    &   5078.85     &   --  &   --
    \end{tabular}
    \vspace{1em}
    \caption{For each metal cluster, the table shows the number $N$ of valence electrons, the \textit{jellium} radius $R_c$, the electron spillout $\delta N$ (equation \eqref{eq:spillout}) and the polarizability $\alpha$ (equation \eqref{eq:polarizability}), computed from the electronic densities reported in \autoref{fig:densityNa} and \autoref{fig:densityK}. The last two columns show the electron spillout for $N = 20$ as reported in the paper \cite{Giai1996} by \citeauthor{Giai1996}, computed from two different models, JM (\textit{jellium} model) and PHJM (pseudo-hamiltonian \textit{jellium} model). Our electron spillouts for $N = 20$ are compatible with the JM electron spillouts reported in the paper.}
    \label{tab:polarizability}
\end{table}