#ifndef DFT_POTENTIALS_H
#define DFT_POTENTIALS_H

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include "../../src/gsl_utility.h"

#define FOUR_PI (4. * M_PI)

// Parameters of the Perdew-Wang parametrization of the correlation energy density
#define P 1.
#define A 0.031091
#define ALPHA_1 0.21370
#define BETA_1 7.5957
#define BETA_2 3.5876
#define BETA_3 1.6382
#define BETA_4 0.49294

int set_external_potential(gsl_vector *, const gsl_vector *, const double, const size_t);
int set_centrifugal_potential(gsl_vector *, const gsl_vector *, const size_t);
int set_direct_potential(gsl_vector *, const gsl_vector *, const gsl_vector *, const double);
int compute_exchange_energy_density(gsl_vector *, const gsl_vector *);
int compute_correlation_energy_density(gsl_vector *, const gsl_vector *);
int set_exchange_correlation_potential(gsl_vector *, const gsl_vector *);
int set_ks_potential(gsl_vector *, const gsl_vector *, const gsl_vector *, const gsl_vector *,
                     const double);
int set_effective_potential(gsl_vector *, const gsl_vector *, const gsl_vector *, const size_t);
double find_potential_minimum(const gsl_vector *);

#endif
