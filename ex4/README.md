# Variational Monte Carlo for Superfluid <sup>4</sup>He

[Assignment](ex4/README.pdf)

[Report](https://gitlab.com/f3fora/comphys/-/jobs/artifacts/master/file/main.pdf?job=TeXex4)

[C Code](ex4/src/)
