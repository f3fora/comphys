\subsection{Computation of Total Cross Section} \label{sec: cross_section}

As mentioned, the computation of the phase shifts $\delta_{\ell}$ through the approximated formula \eqref{eq: delta_l} requires the solution of the equation \eqref{eq: schr-scattering} using the Numerov algorithm. In this case, with reference to the \autoref{sec: numerov}, we have
\begin{equation}
    K^2(\tilde{r}_i) = \frac{2\mu\varepsilon\sigma^2}{\hbar^2}\left[E - 4\left(\frac{1}{\tilde{r}_i^{12}} - \frac{1}{\tilde{r}_i^6}\right)\right] - \frac{\ell(\ell + 1)}{\tilde{r}_i^2}\,, 
\end{equation}
while as initial conditions we can set
\begin{equation}
    R_{\ell}(\tilde{r}_{low}) = e^{-(b/\tilde{r}_{low})^5}\,, \quad
    R_{\ell}(\tilde{r}_{low}+h) = e^{-[b/(\tilde{r}_{low}+h)]^5}\,,
\end{equation}
where the normalization is arbitrary, $b$ is fixed by \eqref{eq: b_value} and $0 < \tilde{r}_{low} < 1$ (since the effective potential diverges for $\tilde{r} \to 0$).

As an example, \autoref{fig:sc_wf} shows the wave functions found by setting $\tilde{E} = 0.3$, for three different values of $\ell$. For $\tilde{r} \gg 1$, the three curves exhibit the expected sinusoidal behaviour.

\inputfigure[0.95]{sc_wf}{Scattering: Wave Functions}{The figure shows the solutions of the equation \eqref{eq: schr-scattering} for $\ell = 0,\,3,\,6$. The wave functions $R_{\ell}(r)$ are computed numerically using the Numerov algorithm \eqref{eq: numerov}, setting $E = 0.3\,\varepsilon$, $r_{low} = 0.8\,\sigma$, $h = 10^{-4}$. The three curves tend asymptotically to sinusoidal functions.}

Found the wave functions, each phase shift is computed by applying the formula \eqref{eq: delta_l}, evaluating the correspondent solution of \eqref{eq: schr-scattering} in two points $\tilde{r}_1 < \tilde{r}_2$, sufficiently distant from $\tilde{r}_{low}$ and from each other.
The phase shifts $\delta_{\ell}$, with $0 \le \ell \le 6$, obtained for $\tilde{E} = 0.3\,$\footnote{Notice that $\tilde{E} = 0.3$ is an intermediate value in the range of energies that we will consider in the total cross section calculation.} are represented in \autoref{fig:deltal_rlow}, \autoref{fig:deltal_r1} and \autoref{fig:deltal_deltar}. The three plots show the dependence of the estimation of $\delta_{\ell}$ on $\tilde{r}_{low}$, $\tilde{r}_{1}$ and $\delta\tilde{r} \equiv \tilde{r}_2 - \tilde{r}_1$, with, in any case, all other parameters fixed.

\autoref{fig:deltal_rlow} shows a decrease of $\delta_{\ell}$ with the growth of $\tilde{r}_{low}$, for each $\ell$ considered. However, a significant variation of $\delta_{\ell}$ occurs only for $\tilde{r}_{low} \gtrsim 0.9$: for lower values of $\tilde{r}_{low}$, the phase shifts are approximately constant.

\inputfigure[0.93]{deltal_rlow}{Scattering: $\boldsymbol{\delta_{\ell}}$ vs $\boldsymbol{{r}_{low}}$}{The figure shows the dependence of the phase shifts $\delta_{\ell}$, for $0 \le \ell \le 6$, on the starting point $r_{low}$ of the Numerov algorithm. The other relevant parameters are fixed: $E = 0.3\,\varepsilon$, $h = 10^{-4}$, $r_1 = 12\,\sigma$, $r_2 = 12.5\,\sigma$.}

\autoref{fig:deltal_r1} shows an increase of $\delta_{\ell}$ with the growth of $\tilde{r}_1$, for each $\ell$ examined. But the dependence of $\delta_{\ell}$ on $\tilde{r}_1$ is significant only for $\tilde{r}_1 \lesssim 8$: for larger values of the parameter, the phase shifts tend to a constant. This stabilization of the phase shifts indicates the achievement of the asymptotic sinusoidal trend by the solutions of \eqref{eq: schr-scattering}.

We observe that, in the considered ranges of values, $\delta_{\ell}$ is more sensible to a change of $\tilde{r}_{low}$ rather than a change of $\tilde{r}_1$. Indeed, excluding the $\ell = 6$ case, the excursion registered between the lowest and the highest value assumed by $\delta_{\ell}$ as $\tilde{r}_{low}$ varies is of the order of $10^{-1}$ for each $\ell$; the same quantity is instead of the order of $10^{-3}$ if we consider the variation with respect to $\tilde{r}_1$.

\inputfigure[0.93]{deltal_r1}{Scattering: $\boldsymbol{\delta_{\ell}}$ vs $\boldsymbol{{r}_{1}}$}{The figure shows the dependence of the phase shifts $\delta_{\ell}$, for $0 \le \ell \le 6$, on the point $r_1$. The other relevant parameters are fixed: $E = 0.3\,\varepsilon$, $r_{low} = 0.8\,\sigma$, $h = 10^{-4}$, $r_2 = r_1 + 0.5\,\sigma$.}

\autoref{fig:deltal_deltar} shows a periodic behaviour of $\delta_{\ell}$ as $\delta\tilde{r}$ varies. For each $\ell$, the phase shifts are approximately constant if $\delta\tilde{r}$ is not around an integer multiple of
\begin{equation}\label{eq: period}
    \frac{\tilde{\lambda}}{2} = \frac{\pi}{\tilde{k}} = \frac{\pi\hbar}{\sigma\sqrt{2 \mu E}} \approx 1.08\,,
\end{equation}
where $\tilde{k}$ and $\tilde{\lambda} = 2 \pi / \tilde{k}$ are respectively the wave vector $k$ and the wavelength $\lambda$ in dimensionless units. Around integer multiples of \eqref{eq: period}, the phase shifts are instead particularly unstable, as we expected from the periodic oscillations exhibited by the spherical Bessel and Neumann functions which appear in the formula \eqref{eq: delta_l}.

\inputfigure[0.9]{deltal_deltar}{Scattering: $\boldsymbol{\delta_{\ell}}$ vs $\boldsymbol{\delta r}$}{The figure shows the dependence of the phase shifts $\delta_{\ell}$, for $0 \le \ell \le 6$, on $\delta r \equiv r_2 - r_1$, with $r_1 = 12\,\sigma$ fixed. The other relevant parameters are fixed: $E = 0.3\,\varepsilon$, $r_{low} = 0.8\,\sigma$, $h = 10^{-4}$. In each graph, the red lines are plotted in the midpoint between the minimum and maximum points of $\delta_{\ell}$ around the regions presenting pathological oscillations. In the legends, the distances measured between two consecutive red lines are shown. The values found are compatible with $\lambda / 2 = \pi / k \approx 1.08\,\sigma$, as expected from formula \eqref{eq: delta_l}.}

In order to obtain the best estimation of the total cross section $\sigma_{tot}$, we choose the parameters for the computation of the phase shifts exploiting what emerges from the previous figures. In particular, we set $\tilde{r}_{low} = 0.8$ and $\tilde{r}_1 = 12$, in order to remain in the region where the phase shifts do not significantly depend on these parameters. Furthermore, we set $\delta \tilde{r} = \tilde{\lambda} / 4 = \pi / \big(2\tilde{k}\big)$, in order to avoid the pathological values shown in \autoref{fig:deltal_deltar}.

The total cross section is then calculated applying the formula $\eqref{eq: total-cross-section}$, truncating the series at a certain value $\ell_{max}$ of the orbital angular momentum quantum number. This is legitimated by \autoref{fig:sigmatot_lmax}, which shows how the estimation of $\sigma_{tot}$ tends to a limit value for $\ell_{max} \gtrsim 6$, by setting $\tilde{E} = 0.3$. In general, the maximum value of $\ell$ for which the correspondent $\delta_{\ell}$ is significant in the total cross section calculation grows with the increase of $\tilde{E}$. Physically, fixed the energy, for sufficiently large values of $\ell$, the repulsive centrifugal potential, proportional to $\ell(\ell+1)/\tilde{r}^2$, makes the penetration of the incident wave negligible. Consequently, the correspondent phase shifts do not contribute significantly to the observable total cross section and can be neglected in the summation.

\inputfigure[0.86]{sigmatot_lmax}{Scattering: Total Cross Section as a Function of $\boldsymbol{\ell_{max}}$}{The figure shows the dependence of the total cross section $\sigma_{tot}$ on the maximum value $\ell_{max}$ of the orbital angular momentum quantum number considered in the summation \eqref{eq: total-cross-section}. The plot is obtained by setting $E = 0.3\,\varepsilon$, $r_{low} = 0.8\,\sigma$, $h = 10^{-4}$, $r_1 = 12\,\sigma$, $\delta r = \pi / (2 k) \approx 0.54\,\sigma$. $\sigma_{tot}$ tends to a limit value for $\ell_{max} \gtrsim 6$.}

\inputfigure[0.95]{sc_best}{Scattering: Total Cross Section as a Function of Energy}{The figure shows the total cross section $\sigma_{tot}$ as a function of the energy $E$. The theoretical curves are numerically computed by setting $h = 10^{-4}$, $r_1 = 12\,\sigma$, $\delta r = \pi / (2 k)$. The experimental data, with uncertainty bars, are taken from the paper \cite{Toennies} (figure 20, page 633). The best agreement between theoretical model and data is obtained for $\ell_{max} = 7$ and $r_{low} \approx 0.997\,\sigma$. In this case, the reduced chi-square is equal to $\chi^2_r \approx 6.78$.}

As a final result, in \autoref{fig:sc_best}, we plot the total cross section $\sigma_{tot}$ as a function of energy in the interval $0 < \tilde{E} < 0.6$. The figure compares the numerical results with the experimental data reported in the paper \cite{Toennies}.

In order to obtain the best agreement with the experimental data, we analyzed the dependence of the reduced chi-square $\chi_r^2$, as a quantifier of the goodness of our model, on the choice of $\ell_{max}$ and $\tilde{r}_{low}$.

\autoref{fig:chi_lmax} shows that $\chi_r^2$ decreases as $\ell_{max}$ increases, as we expected from the analysis of \autoref{fig:sigmatot_lmax}. In particular, for $\ell_{max} \ge 7$, $\chi_r^2$ tends to its minimum value. As shown in \autoref{fig:sc_best}, the addend of \eqref{eq: total-cross-section} with $\ell = 7$ must be considered if we want to describe the experimental results for $\tilde{E} \gtrsim 0.5$, regardless of the choice of $\tilde{r}_{low}$. This fact is consistent with our previous analyzes.

\inputfigure[.92]{chi_lmax}{Scattering: $\boldsymbol{\chi^2_r}$ vs $\boldsymbol{{\ell}_{max}}$}{The figure shows the dependence of the reduced chi-square $\chi_r^2$, as a quantifier of the discrepancy between the numerically computed total cross section and the experimental one, on the maximum value $\ell_{max}$ of the orbital angular momentum quantum number considered in the summation \eqref{eq: total-cross-section}. The plot is obtained by setting $r_{low} = 0.8\,\sigma$, $h = 10^{-4}$, $r_1 = 12\,\sigma$, $\delta r = \pi / (2k)$. $\chi^2_r$ tends to a limit value for $\ell_{max} \ge 7$.}

\autoref{fig:chi_rlow} shows the dependence of $\chi^2_r$ on the value of $\tilde{r}_{low}$. The curve is approximately constant for $\tilde{r}_{low} \lesssim 0.9$ and has a minimum in $\tilde{r}_{low} \approx 0.997$.

Hence, the best agreement between theoretical model and experimental data is obtained for $\ell_{max} = 7$ and $\tilde{r}_{low} \approx 0.997$. The other relevant parameters of our algorithm are set as follows: $h = 10^{-4}$, $\tilde{r}_1 = 12\,$\footnote{Remember that, by comparing the graphs in \autoref{fig:deltal_rlow} and \autoref{fig:deltal_r1}, the choice of $\tilde{r}_1$ is less important than that of $\tilde{r}_{low}$. Further verifications confirmed that moderate changes in $\tilde{r}_1$ do not significantly modify the graphs shown in \autoref{fig:sc_best}.}, $\delta \tilde{r} = \pi / \big(2 \tilde{k}\big)$. With these settings, we obtain $\chi^2_r \approx 6.78$. In \autoref{tab:chi2}, we report the values of $\chi^2_r$ obtained also for the other parameter choices shown in \autoref{fig:sc_best}.

Finally, we note that the curves in \autoref{fig:sc_best} obtained with $\tilde{r}_{low} = 0.8$, although they are globally characterized by a greater $\chi^2_r$, seem to better describe the experimental data for energies $\tilde{E} \ll 1$. 

\inputfigure[.92]{chi_rlow}{Scattering: $\boldsymbol{\chi^2_r}$ vs $\boldsymbol{{r}_{low}}$}{The figure shows the dependence of the reduced chi-square $\chi_r^2$, as a quantifier of the discrepancy between the numerically computed total cross section and the experimental one, on the starting point $r_{low}$ of the Numerov algorithm. The plot is obtained by setting $h = 10^{-4}$, $r_1 = 12\,\sigma$, $\delta r = \pi / (2k)$, $\ell_{max} = 7$. The curve has a minimum in $r_{low} \approx 0.997\,\sigma$.}

\begin{table}[H]
\centering
\renewcommand\arraystretch{1.3}
\begin{tabular}{ c | c | c }
$\ell_{max}$            &$r_{low}$ $[\sigma]$       &$\chi_r^2$ \\
\hline
\multirow{2}{*}{$6$}    &$0.8$                      &$16.98$    \\
                        &$0.997$                    &$8.23$     \\
\hline
\multirow{2}{*}{$7$}    &$0.8$                      &$14.14$    \\
                        &$0.997$                    &$6.78$     \\
\end{tabular}
\vspace{1em}
\caption{Reduced chi-squares $\chi_r^2$ that quantify the discrepancy between theoretical model and experimental data in \autoref{fig:sc_best}. The table shows the values obtained for different choices of the starting point $r_{low}$ in the Numerov algorithm and of the maximum value $\ell_{max}$ of the orbital angular momentum quantum number considered in the summation \eqref{eq: total-cross-section}.}
\label{tab:chi2}
\end{table}