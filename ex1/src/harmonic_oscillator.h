#ifndef HARMONIC_OSCILLATOR_H
#define HARMONIC_OSCILLATOR_H

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include "../../src/numerov.h"

// hbar^2 / m = 1
#define B_HO 1

int main_harmonic_oscillator_1D(const size_t, const double, const double, const double,
                                const double, const size_t, double *, double *, double *, double *);

int main_harmonic_oscillator_3D(const size_t, const size_t, const double, const double,
                                const double, const double, const size_t, double *, double *,
                                double *, double *);

#endif
