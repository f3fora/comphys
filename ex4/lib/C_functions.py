import numpy as np

from pathlib import Path
import subprocess

THIS_DIRECTORY = Path(__file__).parent.absolute()
COMMON_DIRECTORY = Path(__file__).parents[2] / 'lib'


def vmc(b: float,
        n_cells: int,
        rho: float,
        size_CE: int,
        size_CE_JF: int,
        M_eq: int,
        M_eq_procs: int,
        M_stat: int,
        acc_rate_min: float,
        acc_rate_max: float,
        delta_ini_over_L: float,
        dDelta_over_L: float,
        steps_control: float,
        seed: int = 42,
        path: str = '/tmp/vmc/',
        args: tuple = ('-n', '8', '--use-hwthread-cpus'),
        verbose: bool = False):
    """ vmc(b, n_cells, rho, size_CE, size_CE_JF, M_eq, M_eq_procs, M_stat, acc_rate_min, acc_rate_max, delta_ini_over_L, dDelta_over_L, steps_control, seed = 42, path = '/tmp/vmc/', args = ('-n', '8', '--use-hwthread-cpus'), verbose = False)

        Compute, through the Metropolis algorithm, the mean values of local energy and Jackson-Feenberg local energy with statistical errors, the autocorrelation coefficients of the two quantities and the corresponding autocorrelation lengths.

        Parameters:
            b: float
                variational parameter
            n_cells: int
                number of elementary cubic cells which constitute the simulation box; the number of particles in the simulation box will be N = 4 * n_cells
            rho: float
                particle density
            size_CE: int
                size of the array pointed by CE; it is equal to the number of points in which the autocorrelation coefficient of the local energy is evaluated
            size_CE_JF: int
                size of the array pointed by CE_JF; it is equal to the number of points in which the autocorrelation coefficient of the JF local energy is evaluated
            M_eq: int
                number of Metropolis steps constituting the equilibration phase
            M_eq_procs: int
                number of Metropolis steps that each processor performs independently between the equilibration phase and the statistical one
            M_stat: int
                number of Metropolis steps constituting the statistical phase
            acc_rate_min: float
                lower bound of the range within which the acceptance rate is approximately set
            acc_rate_max: float
                upper bound of the range within which the acceptance rate is approximately set
            delta_ini_over_L: float
                initial value of the parameter Delta divided by the side of the simulation box
            dDelta_over_L: float
                initial value of the parameter dDelta divided by the side of the simulation box
            steps_control: float
                number of Metropolis steps between two consecutive adjustments of Delta during the equilibration phase
            seed: int = 42
                seed for the random number generation
            path: str = '/tmp/vmc/'
                path '/path/to/dir/' where auxiliary files are temporary saved
            args: tuple = ('-n', '8', '--use-hwthread-cpus')
                arguments for mpiexec
            verbose: bool = False
                if true: print log of C code

        Returns:
            E_mean: float
                mean value of the local energy (per particle)
            E_std: float
                statistical error on the mean local energy (per particle)
            E_JF_mean: float
                mean value of the JF local energy (per particle)
            E_JF_std: float
                statistical error on the mean JF local energy (per particle)
            tau: float
                autocorrelation length of the local energy
            tau_std: float
                error on the autocorrelation length of the local energy
            tau_JF: float
                autocorrelation length of the JF local energy
            tau_JF_std: float
                error on the autocorrelation length of the JF local energy
            delta_over_L: float
                final value of the parameter Delta divided by the side of the simulation box
            acc_rate: float
                average acceptance rate measured during the statistical phase
            CE: 1D array(shape size_CE)
                autocorrelation coefficient of the local energy 
            CE_JF: 1D array(shape size_CE_JF)
                autocorrelation coefficient of the JF local energy

        Compiling:
            !mpicc -Wall -lm -lgsl -lgslcblas ./src/*.h ./src/*.c -o ./lib/mainvmc.out

        [This function spawns a process running mainvmc.out with mpiexec]
    """

    cmd = ['mpiexec']
    cmd += list(args)
    cmd += [str(THIS_DIRECTORY / 'mainvmc.out')]
    cmd += [path]
    cmd += [str(seed)]
    cmd += [
        str(x) for x in [
            b, n_cells, rho, size_CE, size_CE_JF, M_eq, M_eq_procs, M_stat,
            acc_rate_min, acc_rate_max, delta_ini_over_L, dDelta_over_L,
            steps_control
        ]
    ]

    lpath = Path(path)
    lpath.mkdir(parents=True, exist_ok=True)
    ce_file = lpath / 'ce'
    ce_jf_file = lpath / 'ce_jf'
    res_file = lpath / 'res'

    try:
        output = None if verbose else subprocess.DEVNULL
        subprocess.run(cmd, check=True, stdout=output, stderr=output)
    except subprocess.CalledProcessError:
        return None

    CE = np.loadtxt(ce_file)
    CE_JF = np.loadtxt(ce_jf_file)
    results = np.loadtxt(res_file)

    ce_file.unlink()
    ce_jf_file.unlink()
    res_file.unlink()

    E_mean, E_std, E_JF_mean, E_JF_std, tau, tau_std, tau_JF, tau_JF_std, delta_over_L, acc_rate = results

    return E_mean, E_std, E_JF_mean, E_JF_std, tau, tau_std, tau_JF, tau_JF_std, delta_over_L, acc_rate, CE, CE_JF
