# Scattering of H Atoms on a Kr Atom

[Assignment](ex1/README.pdf)

[Report](https://gitlab.com/f3fora/comphys/-/jobs/artifacts/master/file/main.pdf?job=TeXex1)

[C Code](ex1/src/)
