from matplotlib import pyplot as plt
from matplotlib import font_manager as font_manager, get_data_path


def set_size(width=400, fraction=1, subplots=(1, 1), portrait=False):
    """
        https://jwalton.info/Embed-Publication-Matplotlib-Latex/
    """
    width_pt = width

    # Width of figure (in pts)
    fig_width_pt = width_pt * fraction
    # Convert from pt to inches
    inches_per_pt = 1 / 72.27

    # Golden ratio to set aesthetic figure height
    # https://disq.us/p/2940ij3
    golden_ratio = (5**.5 - 1) / 2
    if portrait:
        golden_ratio = (5**.5 + 1) / 2

    ratio = (subplots[0] / subplots[1]) * golden_ratio

    # Figure width in inches
    fig_width_in = fig_width_pt * inches_per_pt
    # Figure height in inches
    fig_height_in = fig_width_in * ratio

    return (fig_width_in, fig_height_in)


def savefig(f, fname, path='img/', extension='.pdf'):
    """
    """
    # save a matplotlib figure
    f.savefig(path + fname + extension,
              dpi=f.dpi,
              format='pdf',
              transparent=True,
              bbox_inches='tight')


# set style
plt.style.use('fivethirtyeight')

_SMALL_SIZE = 10
_MEDIUM_SIZE = 12
_BIG_SIZE = 14
_cmfont = font_manager.FontProperties(fname=get_data_path() +
                                      '/fonts/ttf/cmr10.ttf')

plt.rcParams.update({
    'text.usetex': True,
    'font.family': 'serif',
    'font.serif': _cmfont.get_name(),
    'mathtext.fontset': 'cm',
    'axes.unicode_minus': False,
    'figure.figsize': set_size(),
    'font.size': _SMALL_SIZE,
    'axes.titlesize': _SMALL_SIZE,
    'axes.labelsize': _MEDIUM_SIZE,
    'xtick.labelsize': _SMALL_SIZE,
    'ytick.labelsize': _SMALL_SIZE,
    'legend.fontsize': _SMALL_SIZE,
    'figure.titlesize': _BIG_SIZE,
    'lines.linewidth': 2,
    'lines.markersize': 3,
    'axes.xmargin': .05,
    'axes.ymargin': .11,
})
