import ctypes
from numpy.ctypeslib import ndpointer
import numpy as np

from pathlib import Path
import sys

THIS_DIRECTORY = Path(__file__).parent.absolute()
COMMON_DIRECTORY = Path(__file__).parents[2] / 'lib'

sys.path.append(str(COMMON_DIRECTORY))

from C_functions_utils import Cfunction


class Hydrogen(Cfunction):
    def __init__(self):
        _argtypes = (
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # alpha
            ctypes.c_size_t,  # M
            ctypes.c_size_t,  # Z
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # energy
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # coeff
        )
        _restype = ctypes.c_int
        _filename = THIS_DIRECTORY / 'libatoms.so'
        super().__init__(_filename, _argtypes, _restype)
        self.init_function(self.CDLL.compute_hydrogen)

    def _hydrogen(self, alpha: np.ndarray, Z: int = 1):
        alpha = alpha.astype(np.double)
        M = len(alpha)

        E = np.empty(shape=1, dtype=np.double)
        coeff = np.empty(shape=M, dtype=np.double)

        if self.function(alpha, ctypes.c_size_t(M), ctypes.c_size_t(Z), E,
                         coeff) == 0:
            return E, coeff
        else:
            return None


def hydrogen(*args, **kwargs):
    """ hydrogen(alpha, Z = 1):
        Compute the ground state energy and the corresponding wave function of a hydrogen-like atom, applying the variational method to a trial wave function which is a linear combination of M Gaussians.
        
        Parameters:
            alpha: 1D array (shape M)
                Alpha parameter of Gaussian functions
            Z: int > 0 (default 1)
                Nuclear charge of a Hydrogen-like atom
        Returns:
            None if function does not converge
            E: 0D array
                Ground state energy
            coeff: 1D array(shape M)
                Linear coefficient of the Gaussian functions

        Binding:
            int compute_hydrogen(const double *alpha, const size_t M, const size_t Z, double *energy, double *coeff)

        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libatoms.so ./src/*.c ./src/*.h
    """
    return Hydrogen()._hydrogen(*args, **kwargs)


class Atom(Cfunction):
    def __init__(self):
        _argtypes = (
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # alpha
            ctypes.c_size_t,  # M
            ctypes.c_size_t,  # N
            ctypes.c_double,  # mix
            ctypes.c_double,  # thresh
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # energy
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # coeff
        )
        _restype = ctypes.c_size_t
        _filename = THIS_DIRECTORY / 'libatoms.so'
        super().__init__(_filename, _argtypes, _restype)
        self.init_function(self.CDLL.compute_atom)

    def _atom(self, alpha: np.ndarray, N: int, mix: float, thresh: float):
        alpha = alpha.astype(np.double)
        M = len(alpha)

        E = np.empty(shape=1, dtype=np.double)
        coeff = np.empty(shape=M * int(N / 2), dtype=np.double)

        n = self.function(alpha, ctypes.c_size_t(M), ctypes.c_size_t(N),
                          ctypes.c_double(mix), ctypes.c_double(thresh), E,
                          coeff)
        if n > 0:
            return n, E, coeff.reshape(-1, int(N / 2)).T.squeeze()
        else:
            return None


def atom(*args, **kwargs):
    """ atom(alpha, N, thresh):
        Compute the ground state energy and the coefficients defining the N/2 occupied orbitals of a neutral atom with an even number N > 1 of electrons, solving the Hartree-Fock-Roothaan equations, written on a basis of M Gaussians, through a self-consistent field calculation.
        
        Parameters:
            alpha: 1D array (shape M)
                Alpha parameter of Gaussian functions
            N: int > 0 (even)
                Number of electrons
            mix: float > 0
                Mixing parameter
            thresh: float > 0
                Threshold value
        Returns:
            None if function does not converge
            n: int > 0
                Number of iteration
            E: 0D array
                Ground state energy
            coeff: np.array(?D, shape N/2 x M)
                Linear coefficient of the Gaussian functions

        Binding:
            size_t compute_atom(const double *alpha, const size_t M, const size_t N, const double mix, const double thresh, double *energy, double *coeff)
        
        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libatoms.so ./src/*.c ./src/*.h
    """
    return Atom()._atom(*args, **kwargs)


class Atom_contr(Cfunction):
    def __init__(self):
        _argtypes = (
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # alpha
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # beta
            ctypes.c_size_t,  # M1
            ctypes.c_size_t,  # M2
            ctypes.c_size_t,  # N
            ctypes.c_double,  # mix
            ctypes.c_double,  # thresh
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # energy
            ndpointer(dtype=np.double, ndim=1, flags='C_CONTIGUOUS'),  # coeff
        )
        _restype = ctypes.c_size_t
        _filename = THIS_DIRECTORY / 'libatoms.so'
        super().__init__(_filename, _argtypes, _restype)
        self.init_function(self.CDLL.compute_atom_contr)

    def _atom_contr(self, alpha: np.ndarray, beta: np.ndarray, N: int,
                    mix: float, thresh: float):
        alpha = alpha.astype(np.double)

        M1 = alpha.shape[0]
        M2 = alpha.shape[1]
        E = np.empty(shape=1, dtype=np.double)
        coeff = np.empty(shape=M1 * int(N / 2), dtype=np.double)

        n = self.function(alpha.reshape(-1), beta.reshape(-1),
                          ctypes.c_size_t(M1), ctypes.c_size_t(M2),
                          ctypes.c_size_t(N), ctypes.c_double(mix),
                          ctypes.c_double(thresh), E, coeff)
        if n > 0:
            return n, E, coeff.reshape(-1, int(N / 2)).T.squeeze()
        else:
            return None


def atom_contr(*args, **kwargs):
    """ atom_contr(alpha, beta, N, mix, thresh)
        Compute the ground state energy and the coefficients defining the N/2 occupied orbitals of a neutral atom with an even number N > 1 of electrons, solving the Hartree-Fock-Roothaan equations, written on a basis of M1 contractions of M2 Gaussians, through a self-consistent field calculation.

        Parameters:
            alpha: np.array (2D, shape M1xM2)
                Alpha parameter of Gaussian functions
            beta: np.array (2D, shape M1xM2)
                Linear coefficients
            N: int > 0 (even)
                Number of electrons
            mix: float > 0
                Mixing parameter
            thresh: float > 0
                Threshold value
        Returns:
            None if function does not converge
            n: int > 0
                Number of iteration
            E: 0D array
                Ground state energy
            coeff: 2D array(shape N/2 x M1)
                Linear coefficient of the Gaussian functions

        Binding:
            size_t compute_atom_contr(const double *alpha, const double *beta, const size_t M1, const size_t M2, const size_t N, const double mix, const double thresh, double *energy, double *coeff)

        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libatoms.so ./src/*.c ./src/*.h
    """
    return Atom_contr()._atom_contr(*args, **kwargs)
