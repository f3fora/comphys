#ifndef VARIATIONAL_H
#define VARIATIONAL_H

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_eigen.h>

int hamilton(gsl_matrix *, const double *, const size_t);
int overlap_matrix(gsl_matrix *, const double *);
int find_eigenstates(gsl_vector *, gsl_matrix *, const gsl_matrix *, const gsl_matrix *);
double find_ground_state(gsl_vector *, const gsl_matrix *, const gsl_matrix *);
double normalize(gsl_vector *, const double *);

#endif
