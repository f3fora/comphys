#include "numerov.h"

/*
 * Fill x with successive values separated by the step h, starting from r_low.
 * x: gsl_vector to fill [output]
 * r_low: minimum value to store
 * h: step between two consecutive values
 */
int set_coordinate(gsl_vector *x, const double r_low, const double h)
{
    size_t i;

    for (i = 0; i < x->size; i++) {
        gsl_vector_set(x, i, r_low + i * h);
    }

    return 0;
}

/*
 * Execute the Numerov algorithm.
 * y: resulting wave function [output];
 *    the 1st and 2nd items must contain the initial conditions
 * V: potential
 * E: energy
 * B: hbar^2 / m in the chosen units
 * h: step between two consecutive values of wave function and potential
 */
int numerov(gsl_vector *y, const gsl_vector *V, const double E, const double B, const double h)
{
    size_t i;

    for (i = 1; i < y->size - 1; i++) {
        // clang-format off
        gsl_vector_set(y, i + 1, (gsl_vector_get(y, i) * (2. - 5. * h * h / 3. *
                       (E - gsl_vector_get(V, i)) / B) - gsl_vector_get(y, i - 1) *
                       (1. + h * h / 6. * (E - gsl_vector_get(V, i - 1)) / B)) /
                       (1. + h * h / 6. * (E - gsl_vector_get(V, i + 1)) / B));
        // clang-format on
    }

    return 0;
}

/*
 * Find the 1st sign change of the function psi(r_max, E), with r_max fixed, for E > initial_E.
 * results: pointer to a double 4-array, which will contain, in order, an energy value E_1 less than
 *          the value by which psi changes sign, a value E_2 greater than the value by which psi
 *          changes sign, psi(r_max, E_1), psi(r_max, E_2) [output]
 * y: the 1st and 2nd items must contain the initial conditions for the Numerov algorithm
 * V: potential
 * initial_E: energy value from which the search for the sign change begins
 * dE: increase of the energy at each iteration
 * B: hbar^2 / m in the chosen units
 * h: step for the Numerov algorithm
 */
void _find_next_zero(double *results, gsl_vector *y, const gsl_vector *V, const double initial_E,
                     const double dE, const double B, const double h)
{
    int sign;

    results[1] = initial_E;
    numerov(y, V, results[1], B, h);

    if (last_value(y) > 0) {
        sign = 1;
    } else {
        sign = -1;
    }

    do {
        results[2] = last_value(y);
        results[1] += dE;
        numerov(y, V, results[1], B, h);
    } while (last_value(y) * sign > 0);

    results[0] = results[1] - dE;
    results[3] = last_value(y);
}

/*
 * Execute the secant algorithm to search for a zero of psi(r_max, E), with r_max fixed, starting
 * from the results of _find_next_zero.
 * E: pointer to a double variable, where the energy satisfying psi(r_max, E) = 0 will be stored
 *    [output]
 * y: wave function corresponding to the energy E [output];
 *    the 1st and 2nd items must contain the initial conditions for the Numerov algorithm
 * V: potential
 * initial_values: results of _find_next_zero
 * thresh: twice the error on the energy found
 * B: hbar^2 / m in the chosen units
 * h: step for the Numerov algorithm
 * return: if the algorithm has terminated successfully: 0
 *         if the maximum number of iterations MAX_SEC has been reached: 1
 */
int _secant_method(double *E, gsl_vector *y, const gsl_vector *V, const double *initial_values,
                   const double thresh, const double B, const double h)
{
    double _E[3], last_y[2], temp;
    size_t i;

    _E[0] = initial_values[0];
    _E[1] = initial_values[1];
    last_y[0] = initial_values[2];
    last_y[1] = initial_values[3];

    for (i = 0; fabs(_E[1] - _E[0]) > thresh && i < MAX_SEC; i++) {
        _E[2] = (_E[0] * last_y[1] - _E[1] * last_y[0]) / (last_y[1] - last_y[0]);
        numerov(y, V, _E[2], B, h);
        temp = last_value(y);
        if (temp * last_y[1] < 0) {
            last_y[0] = last_y[1];
            _E[0] = _E[1];
        }
        last_y[1] = temp;
        _E[1] = _E[2];
    }

    *E = (_E[1] + _E[0]) * 0.5;
    numerov(y, V, *E, B, h);

    if (fabs(_E[1] - _E[0]) > thresh) {
        return 1;
    }
    return 0;
}

/*
 * Execute, in succession, _find_next_zero and _secant_method, in order to find the smallest
 * eigenenergy E >= initial_E and the corresponding wave function for the potential V.
 * E: pointer to a double variable, where the eigenenergy will be stored [output]
 * y: resulting wave function [output];
 *    the 1st and 2nd items must contain the initial conditions for the Numerov algorithm
 * V: potential
 * initial_E: energy value from which the search for the eigenenergy begins
 * dE: energy increase for _find_next_zero
 * thresh: twice the error on the eigenenergy found
 * B: hbar^2 / m in the chosen units
 * h: step for the Numerov algorithm
 * return: if the secant algorithm has terminated successfully: 0
 *         if the maximum number of iterations MAX_SEC has been reached: 1
 */
int find_next_eigen(double *E, gsl_vector *y, const gsl_vector *V, const double initial_E,
                    const double dE, const double thresh, const double B, const double h)
{
    double results[4];
    _find_next_zero(results, y, V, initial_E, dE, B, h);
    return _secant_method(E, y, V, results, thresh, B, h);
}
