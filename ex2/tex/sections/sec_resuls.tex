\section{Discussion of the Results} \label{sec:results}

In this section, we show the results obtained from the HF method applied to the H, He and Be atoms, as explained in the previous sections. All the results are obtained by setting the mixing parameter $\mu = 0.1$ and the threshold $\epsilon = \SI{e-10}{Ha}$. For each HF calculation, we plot the normalized orbitals $\phi_k(r)$, the electronic density
\begin{equation}
    \rho(r) = 2\sum_{k = 1}^{N/2} \left|\phi_k(r)\right|^2
\end{equation}
and, given the spherical symmetry of the considered problems, the radial electronic density
\begin{equation}
    \rho_{rad}(r) = 4 \pi r^2 \rho(r)\,.
\end{equation}

\autoref{fig:H_wf} compares the H atom's ground state wave function obtained by solving the HFR equation \eqref{eq:HFR_H} with the one that solves the Schrödinger equation
\begin{equation}\label{eq:Schroedinger_H}
    \left(-\frac{1}{2} \nabla^2 - \frac{1}{r}\right)\Psi(\mathbf{r}) = E \Psi(\mathbf{r})\,,
\end{equation}
namely
\begin{equation}\label{eq:Scr_wf_H}
    \Psi(r) = \frac{1}{\sqrt{\pi}} e^{-r}\,.
\end{equation}

\inputfigure[.825]{H_wf}{Hydrogen: Ground State Wave Function}{The figure compares the H atom's (normalized) ground state wave function $\Psi(r)$ which solves the Schrödinger equation \eqref{eq:Schroedinger_H} with the one computed through the HF method, by solving the HFR equation \eqref{eq:HFR_H} written on the STO-4G basis defined by the coefficients \eqref{eq:alpha_H}. The plot below shows the difference $\Delta \Psi(r)$ between the Schrödinger solution and the HFR one. The chosen STO-4G basis is not able to provide an effective description of the H atom's ground state in the $r \to 0$ limit.}

\noindent The HF result is in good agreement with the wave function \eqref{eq:Scr_wf_H}, except for $r \to 0$, where the discrepancy is particularly significant. This is due to the choice of a basis of Gaussians, which are not able to reproduce an exponential function in the $r \to 0$ limit. Consequently, the same problem affects also the electronic density $\rho(r)$ shown in \autoref{fig:H_allrho}. The radial electronic density $\rho_{rad}(r)$ is instead affected by an error of an order of magnitude smaller, since the presence of the factor $4\pi r^2$ corrects its behaviour in the $r \to 0$ limit.

\inputfigure[.825]{H_allrho}{Hydrogen: Ground State Electronic Density}{The figure shows the H atom's ground state electronic density $\rho(r) = |\Psi(r)|^2$ and the corresponding radial electronic density $\rho_{rad}(r) = 4 \pi r^2 \rho(r)$. The electronic density given by the Schrödinger equation \eqref{eq:Schroedinger_H} is compared to the one obtained through the HF method, by solving the HFR equation \eqref{eq:HFR_H} written on the STO-4G basis defined by the coefficients \eqref{eq:alpha_H}. The plot below shows the differences $\Delta \rho(r)$ and $\Delta\rho_{rad}(r)$ between the quantities $\rho$ and $\rho_{rad}$ given by the Schrödinger equation and the same obtained through the HF method. For $\rho(r)$, the discrepancy is particularly significant only in the $r \to 0$ limit.}

\inputfigure[.825]{He_wf}{Helium: $\boldsymbol{1s}$ Orbital}{The figure shows the He atom's (normalized) $1s$ orbital $\phi_{1s}(r)$ computed through the HF method, by solving the HFR equations \eqref{eq:HFR} written on the STO-4G basis defined by the coefficients \eqref{eq:alpha_He}. Compared to the H atom's $1s$ orbital, the He atom's one exhibits a faster decay.}

The He atom's $1s$ orbital reported in \autoref{fig:He_wf} shows a behavior similar to the H atom's wave function of \autoref{fig:H_wf}. But, compared to the H atom, the He orbital exhibits a faster decay, as expected from the larger charge of the He nucleus, which binds the electrons with a stronger potential. A faster decay, compared to the H atom, is also exhibited by the He atom's ground state electronic density $\rho(r)$ shown in \autoref{fig:He_allrho}. The corresponding radial electronic density $\rho_{rad}(r)$ has instead a peak with a larger FWHM than that shown by the H atom. This is explained by the repulsion between the two He electrons.

\inputfigure[.825]{He_allrho}{Helium: Ground State Electronic Density}{The figure shows the He atom's ground state electronic density $\rho(r) = 2|\phi_{1s}(r)|^2$ and the corresponding radial electronic density $\rho_{rad}(r) = 4 \pi r^2 \rho(r)$. The electronic density is calculated using the $1s$ orbital computed through the HF method, by solving the HFR equations \eqref{eq:HFR} written on the STO-4G basis defined by the coefficients \eqref{eq:alpha_He}. Compared to the H atom results, $\rho(r)$ exhibits a faster decay, while $\rho_{rad}(r)$ shows a peak with a larger FWHM.}

\inputfigure[.825]{allBe_wf}{Beryllium: Occupied Orbitals in the Ground State}{The figure shows the Be atom's (normalized) $1s$ and $2s$ orbitals, denoted by $\phi_{1s}(r)$ and $\phi_{2s}(r)$ respectively. The orbitals are computed through the HF method, by solving the HFR equations \eqref{eq:HFR} written on two different bases: the non-contracted STO-8G basis defined by the $\alpha$-coefficients in \autoref{tab:alpha_beta_Be} and the contracted basis \eqref{eq:contracted_basis}. As expected, the $2s$ orbital has a node.}

\inputfigure[.801]{allBe_rho}{Beryllium: Ground State Electronic Density}{The figure shows the Be atom's ground state electronic density $\rho(r) = 2\big(\left|\phi_{1s}(r)\right|^2 + \left|\phi_{2s}(r)\right|^2\big)$, calculated using the $1s$ and $2s$ orbitals obtained by solving the HFR equations \eqref{eq:HFR} written on two different bases: the non-contracted STO-8G basis defined by the $\alpha$-coefficients in \autoref{tab:alpha_beta_Be} and the contracted basis \eqref{eq:contracted_basis}. The plot below shows the difference $\Delta \rho(r)$ between the densities computed in the two different ways. The discrepancy is particularly large for $r \lesssim 0.4\,a_0$.}

\inputfigure[.801]{allBe_rhorad}{Beryllium: Ground State Radial Electronic Density}{The figure shows the Be atom's ground state radial electronic density $\rho_{rad}(r) = 4 \pi r^2 \rho(r)$, where $\rho(r)$ is the electronic density, calculated using the $1s$ and $2s$ orbitals obtained by solving the HFR equations \eqref{eq:HFR} written on two different bases: the non-contracted STO-8G basis defined by the $\alpha$-coefficients in \autoref{tab:alpha_beta_Be} and the contracted basis \eqref{eq:contracted_basis}. The plot below shows the difference $\Delta \rho_{rad}(r)$ between the radial densities computed in the two different ways. The discrepancy is particularly significant for $r \lesssim 1.2\,a_0$.}

\autoref{fig:allBe_wf} compares the Be atom's orbitals computed with and without contractions. The orbitals obtained with the two bases are particularly different near $r = 0$, while for $r \to \infty$ they show the same behaviour. Notice that the $2s$ orbital, regardless of the basis used, has a node. This is reasonable, since, as known, the H atom's $2s$ orbital also has a node. The large discrepancies found, for small values of $r$, between the orbitals computed with and without contractions are also reflected in the electronic density $\rho(r)$ and radial electronic density $\rho_{rad}(r)$, shown in \autoref{fig:allBe_rho} and \autoref{fig:allBe_rhorad} respectively. In particular, we observe that $\rho_{rad}(r)$ has two distinct peaks, corresponding to the two occupied orbitals.

\autoref{fig:mixing} shows that, for the Be atom\footnote{\autoref{fig:mixing} is for the Be atom, but similar results are obtained for the other HF calculations as well.}, the SCF algorithm converges for all values of the mixing parameter $\mu$, after a number of iterations that grows as $\mu$ decreases. Furthermore, as $\mu$ varies, the estimated ground state energy shows variations lower than the fixed threshold $\epsilon$, guaranteeing the independence of the result from the choice of $\mu$.

\inputfigure[.809]{mixing}{Beryllium: Energy and Number of Iterations vs Mixing Parameter}{The two plots show the computed ground state energy $E_0$ and the number $n$ of iterations of the SCF loop (illustrated in \autoref{fig:flowchart}) as the mixing parameter $\mu$ (in equation \eqref{eq:mixing}) varies. The plots are produced in the case of the Be atom treated with non-contracted orbitals, setting the threshold $\epsilon = \SI{e-10}{Ha}$.}

For each considered atom, the ground state energy computed through the HF method is reported in \autoref{tab:energies}, where it is compared with the experimental measurement. All calculated energies approximate the experimental data by excess, as predicted by the variational principle on which the HF method is based. The smallest relative error is obtained for the H atom, as might be expected, since for this atom, having only one electron, the mean-field approximation was not applied. Furthermore, we observe that, for the Be atom, the result obtained with non-contracted orbitals is affected by a relative error of an order of magnitude smaller than that found with contracted orbitals. This was expected, given the larger number of free parameters to be optimized in the non-contracted case.

Finally, \autoref{tab:coefficients} shows, for each considered atom, the coefficients $c_{kp}$ that solve the HFR equations \eqref{eq:HFR}.

\begin{table}[H]
\centering
\renewcommand\arraystretch{1.3}
%\small
\begin{tabular}{ c | c | c | c }
Atom                    &Hartree-Fock [Ha]    &Experimental value [Ha]          &Relative error \\
\hline
H                       &$-0.499278$          &$-0.4997332541891(11)$           &$0.091\,\%$    \\
\hline
He                      &$-2.847595$          &$-2.9033858765(15)$              &$1.922\,\%$    \\
\hline
Be (contracted)         &$-13.039163$         &\multirow{2}{*}{$-14.668442(2)$} &$11.107\,\%$   \\
\cline{1-2}\cline{4-4}
Be (non-contracted)     &$-14.518321$         &                                 &$1.023\,\%$    \\
\end{tabular}
\vspace{1em}
\caption{The table shows, for each considered atom, the ground state energy computed through the HF method (applying the equations \eqref{eq:energy_exp} and \eqref{eq:energy_eig}) and its experimental measurement, as reported in \cite{NIST}. The last column shows the relative errors between the Hartree-Fock energies $E_0$ and the corresponding experimental values $E_0^{exp}$, calculated as $\left|E_0 - E_0^{exp}\right| / E_0^{exp}$.}
\label{tab:energies}
\end{table}

\begin{table}[H]
\centering
\renewcommand\arraystretch{1.3}
\small
\begin{tabular}{ c | c | r }
Atom                                                &Orbital                    &Coefficients \\
\hline
\multirow{4}{*}{H}                                  &\multirow{4}{*}{$1s$}      &$0.096102$   \\
                                                    &                           &$0.163017$   \\
                                                    &                           &$0.185587$   \\
                                                    &                           &$0.073701$   \\                                                                          
\hline
\multirow{4}{*}{He}                                 &\multirow{4}{*}{$1s$}      &$0.364674$   \\
                                                    &                           &$0.408537$   \\
                                                    &                           &$0.258516$   \\
                                                    &                           &$0.095164$   \\
\hline
\multirow{4}{*}{\samecell{Be\\(contracted)}}        &\multirow{2}{*}{$1s$}      &$1.568563$   \\
                                                    &                           &$-0.006817$  \\
\cline{2-3}
                                                    &\multirow{2}{*}{$2s$}      &$-0.519954$  \\
                                                    &                           &$0.158824$   \\
\hline
\multirow{16}{*}{\samecell{Be\\(non-contracted)}}   &\multirow{8}{*}{$1s$}      &$1.062646$   \\
                                                    &                           &$1.279233$   \\
                                                    &                           &$0.930715$   \\
                                                    &                           &$0.263355$   \\
                                                    &                           &$-0.005876$  \\
                                                    &                           &$-0.001105$  \\
                                                    &                           &$0.003369$   \\
                                                    &                           &$-0.000451$  \\
\cline{2-3}
                                                    &\multirow{8}{*}{$2s$}      &$0.199775$   \\
                                                    &                           &$0.251675$   \\
                                                    &                           &$0.289726$   \\
                                                    &                           &$0.165882$   \\
                                                    &                           &$-0.071933$  \\
                                                    &                           &$-0.038991$  \\
                                                    &                           &$-0.056335$  \\
                                                    &                           &$-0.089526$  \\
\end{tabular}
\vspace{1em}
\caption{The table shows, for each considered atom, the coefficients $c_{kp}$ resulting from the resolution of the HFR equations \eqref{eq:HFR}. The coefficients are divided by orbital and have the same order of the $\alpha$-coefficients reported in \eqref{eq:alpha_H}, \eqref{eq:alpha_He} and \autoref{tab:alpha_beta_Be}.}
\label{tab:coefficients}
\end{table}