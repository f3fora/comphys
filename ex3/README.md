# Alkali Metal Clusters in Density Functional Theory

[Assignment](ex3/README.pdf)

[Report](https://gitlab.com/f3fora/comphys/-/jobs/artifacts/master/file/main.pdf?job=TeXex3)

[C Code](ex3/src/)
