#ifndef HARTREE_FOCK_H
#define HARTREE_FOCK_H

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include "variational.h"

// Maximum number of iterations for the self-consistent field method
#define NUM_ITER_MAX 10000000

#define IDX(i, j, n, m, M) (i + M * (j + M * (n + M * m)))

int compute_density(gsl_matrix *, const gsl_matrix *);
int compute_eris(double *, const double *, const size_t);
int fock(gsl_matrix *, const double *, const gsl_matrix *);
double compute_fock_energy(const double *, const gsl_matrix *);
double compute_energy_eig(const double *, const double, const size_t);
double compute_energy_exp(const gsl_matrix *, const double, const gsl_matrix *);
int mixing(gsl_matrix *, gsl_matrix *, const double);
int sum_sym_matrices(gsl_matrix *, const gsl_matrix *, const gsl_matrix *);
int find_lower_states(double *, gsl_matrix *, const gsl_vector *, const gsl_matrix *, const size_t);
size_t self_consistent_method(double *, gsl_matrix *, const double *, const size_t, const size_t,
                              const gsl_matrix *, const double, const double);

#endif
