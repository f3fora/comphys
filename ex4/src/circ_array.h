#ifndef CIRC_ARRAY_H
#define CIRC_ARRAY_H

#include <stdlib.h>

/*
 * Implementation of a circular array.
 * array: pointer to the array in which the circular array items are stored
 * size: size of the circular array
 * last_index: index that identifies the last item inserted
 */
typedef struct Circ_array {
    double *array;
    size_t size;
    size_t last_index;
} Circ_array;

int Circ_array_alloc(Circ_array *, const size_t);
int Circ_array_realloc(Circ_array *, const size_t);
int Circ_array_add(Circ_array *, const double);
double Circ_array_get(const Circ_array *, const size_t);
double Circ_array_last_item(const Circ_array *);
int Circ_array_free(Circ_array *);

#endif
