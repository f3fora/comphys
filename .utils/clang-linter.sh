#!/usr/bin/env bash

DIR=$1
cd $DIR
linter_errors=$(git clang-format -q --diff --style file --extension c,h | grep -v --color=never "no modified files to format" || true)
if [[ ! -z "$linter_errors" ]]; then
	echo "$linter_errors"
	echo "Detected formatting issues; please fix"
	exit 1
else
	echo "Formatting is correct"
fi
