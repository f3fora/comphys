#!/usr/bin/env bash

DIR=$1
cd $DIR
linter_errors=$(yapf --style pep8 -d -r $DIR)
 if [[ ! -z "$linter_errors" ]]; then
    echo "$linter_errors"
	echo "Detected formatting issues; please fix"
	exit 1
else
	echo "Formatting is correct"
fi
