#include "circ_array.h"

/*
 * Allocate a circular array and initialize its items to zero.
 * a: pointer to the circular array to allocate [output]
 * size: size of the circular array
 */
int Circ_array_alloc(Circ_array *a, const size_t size)
{
    size_t i;

    a->size = size;
    a->last_index = -1;
    a->array = malloc(a->size * sizeof(double));

    for (i = 0; i < a->size; i++) {
        a->array[i] = 0.;
    }

    return 0;
}

/*
 * Reallocate a circular array and initialize its items to zero.
 * a: pointer to the circular array to reallocate [output]
 * size: new size of the circular array
 */
int Circ_array_realloc(Circ_array *a, const size_t size)
{
    size_t i;

    a->size = size;
    a->last_index = -1;
    a->array = realloc(a->array, a->size * sizeof(double));

    for (i = 0; i < a->size; i++) {
        a->array[i] = 0.;
    }

    return 0;
}

/*
 * Add an item to a circular array. If the circular array is full, the new item replaces the oldest
 * item inserted.
 * a: pointer to the circular array [output]
 * value: value to add
 */
int Circ_array_add(Circ_array *a, const double value)
{
    a->last_index = (a->last_index + 1) % a->size;
    a->array[a->last_index] = value;

    return 0;
}

/*
 * Return an item of a circular array.
 * a: pointer to the circular array
 * index: relative position of the desired item with respect to the oldest item inserted
 * return: the desired item
 */
double Circ_array_get(const Circ_array *a, const size_t index)
{
    return a->array[(a->last_index + 1 + index) % a->size];
}

/*
 * Return the last item inserted in a circular array.
 * a: pointer to the circular array
 * return: the last item inserted
 */
double Circ_array_last_item(const Circ_array *a)
{
    return a->array[a->last_index];
}

/*
 * Free a circular array.
 * a: pointer to the circular array to free
 */
int Circ_array_free(Circ_array *a)
{
    free(a->array);

    a->array = NULL;
    a->last_index = -1;
    a->size = 0;

    return 0;
}
