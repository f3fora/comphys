#include "scattering.h"

/*
 * Calculate Lennard-Jones potential and centrifugal potential at point x.
 * x: point where to evaluate the potential
 * l: orbital angular momentum quantum number
 * return: Lennard-Jones potential + centrifugal potential at point x
 */
double _potential_lj(const double x, const size_t l)
{
    return 4. * (pow(x, -12) - pow(x, -6)) + 0.5 * B_SC * (double)l * ((double)l + 1.) / (x * x);
}

/*
 * Fill V with Lennard-Jones potential + centrifugal potential evaluated at the points in x.
 * V: gsl_vector where to store the potential [output]
 * x: points where to evaluate the potential
 * l: orbital angular momentum quantum number
 */
void _set_potential_lj(gsl_vector *V, const gsl_vector *x, const size_t l)
{
    size_t i;

    for (i = 0; i < V->size; i++) {
        gsl_vector_set(V, i, _potential_lj(gsl_vector_get(x, i), l));
    }
}

/*
 * Put, in the first two items of y, the initial conditions for the Numerov method in the scattering
 * case.
 * y: gsl_vector where to store the initial conditions [output]
 * r_low: initial value of the spatial coordinate
 * h: step for the Numerov algorithm
 */
void _set_initial_conditions_lj(gsl_vector *y, const double r_low, const double h)
{
    gsl_vector_set(y, 0, exp(-b_sc / pow(r_low, 5)));
    gsl_vector_set(y, 1, exp(-b_sc / pow(r_low + h, 5)));
}

/*
 * Find the phase shifts delta_l and the total cross section sigma_tot for a fixed energy value.
 * INPUTS:
 * E: energy
 * l_max: maximum value of the orbital angular momentum quantum number for which delta_l is
 *        calculated
 * r_low: initial value of the spatial coordinate for the wave function calculation
 * r_1, r_2: points at which the wave function is evaluated for the calculation of delta_l
 * h: step for the Numerov algorithm
 * M: number of points used in the Numerov algorithm
 * OUTPUTS:
 * x: pointer to a double M-array, where the spatial coordinate will be stored
 * y: pointer to a double [M * (l_max + 1)]-array, where the wave functions computed for different
 *    values of l will be stored
 * delta_l: pointer to a double (l_max + 1)-array, where the phase shifts will be stored
 * sigma_tot: pointer to a double variable, where the total cross section will be stored
 */
int main_scattering(const double E, const size_t l_max, const double r_low, const double r_1,
                    const double r_2, const double h, const size_t M, double *x, double *y,
                    double *delta_l, double *sigma_tot)
{
    size_t l;
    size_t r_1_index = ceil((r_1 - r_low) / h) - 1;
    size_t r_2_index = ceil((r_2 - r_low) / h) - 1;

    double K;
    double k = sqrt(2. * E / B_SC);
    double k_r_1 = k * r_1;
    double k_r_2 = k * r_2;

    gsl_vector_view _x = gsl_vector_view_array(x, M);
    gsl_matrix_view _y_matrix = gsl_matrix_view_array(y, l_max + 1, M);
    gsl_vector_view _y;
    gsl_vector *V = gsl_vector_alloc(M);
    gsl_vector *jl_1 = gsl_vector_alloc(l_max + 1);
    gsl_vector *jl_2 = gsl_vector_alloc(l_max + 1);
    gsl_vector *nl_1 = gsl_vector_alloc(l_max + 1);
    gsl_vector *nl_2 = gsl_vector_alloc(l_max + 1);

    bessel_j_l(jl_1, k_r_1, l_max);
    bessel_j_l(jl_2, k_r_2, l_max);
    bessel_n_l(nl_1, k_r_1, l_max);
    bessel_n_l(nl_2, k_r_2, l_max);

    set_coordinate(&_x.vector, r_low, h);

    for (l = 0; l <= l_max; l++) {
        _y = gsl_matrix_row(&_y_matrix.matrix, l);
        _set_initial_conditions_lj(&_y.vector, r_low, h);
        _set_potential_lj(V, &_x.vector, l);
        numerov(&_y.vector, V, E, B_SC, h);

        K = gsl_vector_get(&_y.vector, r_1_index) * r_2 /
            (gsl_vector_get(&_y.vector, r_2_index) * r_1);
        delta_l[l] = atan((K * gsl_vector_get(jl_2, l) - gsl_vector_get(jl_1, l)) /
                          (K * gsl_vector_get(nl_2, l) - gsl_vector_get(nl_1, l)));
    }

    *sigma_tot = 0.;
    for (l = 0; l <= l_max; l++) {
        *sigma_tot += (2. * l + 1.) * pow(sin(delta_l[l]), 2);
    }
    *sigma_tot *= 4. * M_PI / (k * k);

    gsl_vector_free(V);
    gsl_vector_free(jl_1);
    gsl_vector_free(jl_2);
    gsl_vector_free(nl_1);
    gsl_vector_free(nl_2);

    return 0;
}
