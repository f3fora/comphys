#include "hartree_fock.h"

/*
 * Compute the density matrix.
 * density: MxM gls_matrix in which the density matrix will be stored [output]
 * C: Mx(N/2) gsl_matrix whose columns contain the coefficients which define the orbitals
 */
int compute_density(gsl_matrix *density, const gsl_matrix *C)
{
    size_t i, j, k;
    double d;

    for (i = 0; i < C->size1; i++) {
        for (j = i; j < C->size1; j++) {
            d = 0.;
            for (k = 0; k < C->size2; k++) {
                d += gsl_matrix_get(C, i, k) * gsl_matrix_get(C, j, k);
            }
            gsl_matrix_set(density, i, j, d);
            if (i != j) {
                gsl_matrix_set(density, j, i, d);
            }
        }
    }

    return 0;
}

/*
 * Compute the combinations of electron repulsion integrals (ERIs) which enter into the calculation
 * of the Fock matrix. The computation exploits the symmetries of the ERIs and only the terms
 * required in the Fock matrix calculation are calculated.
 * eris: pointer to a double (M*M*M*M)-array, where the results will be stored [output]
 * alpha: pointer to a double M-array containing the coefficients which define the Gaussians in the
 *        ERIs
 * M: size of the array pointed to by alpha
 */
int compute_eris(double *eris, const double *alpha, const size_t M)
{
    size_t i, j, n, m;

    for (i = 0; i < M; i++) {
        for (j = 0; j < i; j++) {
            for (n = 0; n < M; n++) {
                for (m = 0; m < M; m++) {
                    eris[IDX(i, j, n, m, M)] =
                            2. * pow(M_PI, 2.5) / sqrt(alpha[i] + alpha[j] + alpha[n] + alpha[m]) *
                            (2. / ((alpha[i] + alpha[j]) * (alpha[n] + alpha[m])) -
                             1. / ((alpha[i] + alpha[m]) * (alpha[j] + alpha[n])));
                }
            }
        }
        for (n = 0; n < M; n++) {
            for (m = 0; m <= n; m++) {
                eris[IDX(i, i, n, m, M)] = 2. * pow(M_PI, 2.5) /
                                           sqrt(2. * alpha[i] + alpha[n] + alpha[m]) *
                                           (1. / (alpha[i] * (alpha[n] + alpha[m])) -
                                            1. / ((alpha[i] + alpha[m]) * (alpha[i] + alpha[n])));
            }
        }
    }

    return 0;
}

/*
 * Compute the Fock matrix (including the direct and exchange terms, but not the one-body term).
 * F: MxM gsl_matrix in which the Fock matrix will be stored [output]
 * eris: pointer to a double (M*M*M*M)-array containing the result of the function compute_eris
 * density: MxM gsl-matrix containing the density matrix
 */
int fock(gsl_matrix *F, const double *eris, const gsl_matrix *density)
{
    size_t i, j, n, m;
    double f;

    for (i = 0; i < F->size1; i++) {
        for (j = 0; j < i; j++) {
            f = 0.;
            for (n = 0; n < F->size1; n++) {
                for (m = 0; m < F->size1; m++) {
                    f += gsl_matrix_get(density, n, m) * eris[IDX(i, j, n, m, F->size1)];
                }
            }
            gsl_matrix_set(F, i, j, f);
            gsl_matrix_set(F, j, i, f);
        }
        f = 0.;
        for (n = 0; n < F->size1; n++) {
            for (m = 0; m < n; m++) {
                f += 2. * gsl_matrix_get(density, n, m) * eris[IDX(i, i, n, m, F->size1)];
            }
            f += gsl_matrix_get(density, n, n) * eris[IDX(i, i, n, n, F->size1)];
        }
        gsl_matrix_set(F, i, i, f);
    }

    return 0;
}

/*
 * Compute the common term which appears in both energy calculation methods implemented by the
 * functions compute_energy_eig and compute_energy_exp.
 * eris: pointer to a double (M*M*M*M)-array containing the result of the function compute_eris
 * density: MxM gsl-matrix containing the density matrix
 * return: the parameter "fock_energy" required by the functions compute_energy_eig and
 *         compute_energy_exp
 */
double compute_fock_energy(const double *eris, const gsl_matrix *density)
{
    size_t i, j, n, m;
    double fock_energy = 0.;

    for (i = 0; i < density->size1; i++) {
        for (j = 0; j < i; j++) {
            for (n = 0; n < density->size1; n++) {
                for (m = 0; m < density->size1; m++) {
                    fock_energy += 2. * gsl_matrix_get(density, i, j) *
                                   gsl_matrix_get(density, n, m) *
                                   eris[IDX(i, j, n, m, density->size1)];
                }
            }
        }
        for (n = 0; n < density->size1; n++) {
            for (m = 0; m < n; m++) {
                fock_energy += 2. * gsl_matrix_get(density, i, i) * gsl_matrix_get(density, n, m) *
                               eris[IDX(i, i, n, m, density->size1)];
            }
            fock_energy += gsl_matrix_get(density, i, i) * gsl_matrix_get(density, n, n) *
                           eris[IDX(i, i, n, n, density->size1)];
        }
    }

    return fock_energy;
}

/*
 * Compute the energy exploiting the eigenvalues obtained by the solution of the
 * Hartree-Fock-Roothaan equations.
 * E: pointer to a double (N/2)-array containing the eigenvalues
 * fock_energy: output of the function compute_fock_energy
 * N: number of electrons
 * return: energy
 */
double compute_energy_eig(const double *E, const double fock_energy, const size_t N)
{
    size_t i;
    double e = 0.;

    for (i = 0; i < N / 2; i++) {
        e += E[i];
    }

    return 2. * e - fock_energy;
}

/*
 * Compute the energy as expectation value of the Hamiltonian calculated on the solutions of the
 * Hartree-Fock-Roothaan equations.
 * h: MxM gsl_matrix containing the one-body part of the Hamiltonian
 * fock_energy: output of the function compute_fock_energy
 * density: density matrix
 * return: energy
 */
double compute_energy_exp(const gsl_matrix *h, const double fock_energy, const gsl_matrix *density)
{
    size_t i, j;
    double e = 0.;

    for (i = 0; i < density->size1; i++) {
        for (j = 0; j < i; j++) {
            e += 2. * gsl_matrix_get(density, i, j) * gsl_matrix_get(h, i, j);
        }
        e += gsl_matrix_get(density, i, i) * gsl_matrix_get(h, i, i);
    }

    return 2. * e + fock_energy;
}

/*
 * Mix two matrices, i.e. compute a weighted sum of the two.
 * mat_1: MxM gsl_matrix, where (1 - mix) * mat_1 + mix * mat_2 will be stored [output]
 * mat_2: MxM gsl_matrix (at the end, it will be multiplied by mix)
 * mix: mixing parameter
 */
int mixing(gsl_matrix *mat_1, gsl_matrix *mat_2, const double mix)
{
    gsl_matrix_scale(mat_1, 1. - mix);
    gsl_matrix_scale(mat_2, mix);
    gsl_matrix_add(mat_1, mat_2);

    return 0;
}

/*
 * Sum two symmetric matrices.
 * sum: MxM gsl_matrix, where the sum will be stored [output]
 * mat_1: MxM symmetric gsl_matrix, first addend
 * mat_2: MxM symmetric gsl_matrix, second addend
 */
int sum_sym_matrices(gsl_matrix *sum, const gsl_matrix *mat_1, const gsl_matrix *mat_2)
{
    size_t i, j;
    double s;

    for (i = 0; i < sum->size1; i++) {
        for (j = i; j < sum->size1; j++) {
            s = gsl_matrix_get(mat_1, i, j) + gsl_matrix_get(mat_2, i, j);
            gsl_matrix_set(sum, i, j, s);
            if (i != j) {
                gsl_matrix_set(sum, j, i, s);
            }
        }
    }

    return 0;
}

/*
 * Find the num_states smallest eigenvalues and the corresponding eigenvectors.
 * E: pointer to a double (num_states)-array, where the smallest eigenvalues will be stored [output]
 * C: Mx(num_states) gsl_matrix in whose columns the eigenvetors corresponding to the smallest
 *    eigenvalues will be stored [output]
 * e: M-gsl_vector containing the eigenvalues
 * c: MxM gsl_matrix whose columns contain the eigenvectors
 * num_states: number of eigenvalues to find
 */
int find_lower_states(double *E, gsl_matrix *C, const gsl_vector *e, const gsl_matrix *c,
                      const size_t num_states)
{
    size_t i, min_idx;
    double max = gsl_vector_max(e) + 1.;
    gsl_vector_view vec_view;
    gsl_vector *e_lamb = gsl_vector_alloc(e->size);

    gsl_vector_memcpy(e_lamb, e);

    for (i = 0; i < num_states; i++) {
        min_idx = gsl_vector_min_index(e_lamb);
        E[i] = gsl_vector_get(e_lamb, min_idx);
        gsl_vector_set(e_lamb, min_idx, max);
        vec_view = gsl_matrix_column(C, i);
        gsl_matrix_get_col(&vec_view.vector, c, min_idx);
    }

    gsl_vector_free(e_lamb);

    return 0;
}

/*
 * Compute the ground state energy and the coefficients defining the N/2 occupied orbitals of a
 * neutral atom with an even number N > 1 of electrons, solving the Hartree-Fock-Roothaan equations,
 * written on a basis of M Gaussians, through a self-consistent field calculation.
 * energy: pointer to a double variable, where the estimated ground state energy will be stored
 *         [output]
 * C: Mx(N/2) gsl_matrix in whose columns the coefficients defining the orbitals will be stored
 *    [output]
 * alpha: pointer to a double M-array containing the coefficients which define the Gaussians that
 *        form the considered basis
 * M: number of basis functions (size of the array pointed to by alpha)
 * N: number of electrons (it must be N > 1 even)
 * density_0: MxM gsl_matrix containing the initial guess on the density matrix
 * mix: mixing parameter
 * thresh: threshold for evaluating the convergence of the self-consistent field calculation;
 *         the algorithm ends when the results of the functions compute_energy_eig and
 *         compute_energy_exp differ by less than thresh
 * return: if the algorithm converges within NUM_ITER_MAX iterations:
 *             number of iterations required for the convergence
 *         else:
 *             0
 */
size_t self_consistent_method(double *energy, gsl_matrix *C, const double *alpha, const size_t M,
                              const size_t N, const gsl_matrix *density_0, const double mix,
                              const double thresh)
{
    size_t i, N2 = N / 2;
    size_t num_iter = 0;
    double E[N2];
    double eris[(size_t)pow(M, 4)];
    double fock_energy, energy_exp;

    gsl_matrix *S = gsl_matrix_alloc(M, M);
    gsl_matrix *h = gsl_matrix_alloc(M, M);
    gsl_matrix *H = gsl_matrix_alloc(M, M);
    gsl_matrix *F = gsl_matrix_alloc(M, M);
    gsl_matrix *density_1 = gsl_matrix_alloc(M, M);
    gsl_matrix *density_2 = gsl_matrix_alloc(M, M);
    gsl_matrix *evec = gsl_matrix_alloc(M, M);
    gsl_vector *eval = gsl_vector_alloc(M);
    gsl_vector_view vec_view;

    gsl_matrix_memcpy(density_1, density_0);

    overlap_matrix(S, alpha);
    hamilton(h, alpha, N);
    compute_eris(eris, alpha, M);

    do {
        if (num_iter > 0) {
            mixing(density_1, density_2, mix);
        }

        fock(F, eris, density_1);
        sum_sym_matrices(H, h, F);
        find_eigenstates(eval, evec, H, S);
        find_lower_states(E, C, eval, evec, N2);

        for (i = 0; i < N2; i++) {
            vec_view = gsl_matrix_column(C, i);
            normalize(&vec_view.vector, alpha);
        }

        compute_density(density_2, C);

        fock_energy = compute_fock_energy(eris, density_2);
        *energy = compute_energy_eig(E, fock_energy, N);
        energy_exp = compute_energy_exp(h, fock_energy, density_2);

        num_iter++;
    } while (fabs(*energy - energy_exp) > thresh && num_iter <= NUM_ITER_MAX);

    if (fabs(*energy - energy_exp) > thresh) {
        num_iter = 0;
    }

    gsl_matrix_free(S);
    gsl_matrix_free(h);
    gsl_matrix_free(H);
    gsl_matrix_free(F);
    gsl_matrix_free(density_1);
    gsl_matrix_free(density_2);
    gsl_matrix_free(evec);
    gsl_vector_free(eval);

    return num_iter;
}
