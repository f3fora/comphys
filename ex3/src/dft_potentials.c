#include "dft_potentials.h"

/*
 * Fill V_ext with the external potential (given by the jellium model) evaluated at the points in r.
 * V_ext: gsl_vector where to store the external potential [output]
 * r: points where to evaluate the external potential
 * r_s: Wigner-Seitz radius of the considered atomic species
 * N: number of valence electrons
 * It must be V_ext->size = r->size.
 */
int set_external_potential(gsl_vector *V_ext, const gsl_vector *r, const double r_s, const size_t N)
{
    size_t i;
    double _r;
    double c = 1.5 / pow(r_s, 3);
    double R_c = pow(N, 1. / 3.) * r_s;

    for (i = 0; i < V_ext->size; i++) {
        _r = gsl_vector_get(r, i);

        if (_r <= R_c) {
            gsl_vector_set(V_ext, i, c * (_r * _r / 3. - R_c * R_c));
        } else {
            gsl_vector_set(V_ext, i, -c * pow(R_c, 3) / (1.5 * _r));
        }
    }

    return 0;
}

/*
 * Fill V_centr with the centrifugal potential evaluated at the points in r.
 * V_centr: gsl_vector where to store the centrifugal potential [output]
 * r: points where to evaluate the centrifugal potential
 * l: orbital angular momentum quantum number
 * It must be V_centr->size = r->size.
 */
int set_centrifugal_potential(gsl_vector *V_centr, const gsl_vector *r, const size_t l)
{
    size_t i;
    double c = 0.5 * (double)l * ((double)l + 1.);

    for (i = 0; i < V_centr->size; i++) {
        gsl_vector_set(V_centr, i, c / pow(gsl_vector_get(r, i), 2));
    }

    return 0;
}

/*
 * Fill V_d with the direct potential evaluated at the points in r.
 * V_d: gsl_vector where to store the direct potential [output]
 * r: points where to evaluate the direct potential
 * rho: electronic density
 * h: discretization step
 * It must be V_d->size = r->size >= rho->size; if V_d->size > rho->size, then, in the computation,
 * the electronic density is considered null for r > (rho->size - 1) * h.
 */
int set_direct_potential(gsl_vector *V_d, const gsl_vector *r, const gsl_vector *rho,
                         const double h)
{
    size_t i;
    double integ;

    gsl_vector_const_view _r = gsl_vector_const_subvector(r, 0, rho->size);
    gsl_vector_view sub_rho_r2, sub_rho_r;

    gsl_vector *rho_r2 = gsl_vector_alloc(rho->size);
    gsl_vector *rho_r = gsl_vector_alloc(rho->size);

    pow_vector(rho_r2, &_r.vector, 2);
    gsl_vector_mul(rho_r2, rho);
    multiply_vectors(rho_r, rho, &_r.vector);

    for (i = 0; i < rho->size; i++) {
        sub_rho_r2 = gsl_vector_subvector(rho_r2, 0, i + 1);
        sub_rho_r = gsl_vector_subvector(rho_r, i, rho_r->size - i);

        gsl_vector_set(V_d, i,
                       FOUR_PI * (integral(&sub_rho_r2.vector, h) / gsl_vector_get(r, i) +
                                  integral(&sub_rho_r.vector, h)));
    }

    integ = FOUR_PI * integral(rho_r2, h);

    for (i = rho->size; i < V_d->size; i++) {
        gsl_vector_set(V_d, i, integ / gsl_vector_get(r, i));
    }

    gsl_vector_free(rho_r2);
    gsl_vector_free(rho_r);

    return 0;
}

/*
 * Compute the exchange energy density.
 * eps_x: gsl_vector where to store the exchange energy density [output]
 * rho: electronic density
 * It must be eps_x->size = rho->size.
 */
int compute_exchange_energy_density(gsl_vector *eps_x, const gsl_vector *rho)
{
    pow_vector(eps_x, rho, 1. / 3.);
    gsl_vector_scale(eps_x, -0.75 * pow(3. / M_PI, 1. / 3.));

    return 0;
}

/*
 * Compute the Perdew-Wang correlation energy density.
 * eps_c: gsl_vector where to store the correlation energy density [output]
 * rho: electronic density
 * It must be eps_c->size = rho->size.
 */
int compute_correlation_energy_density(gsl_vector *eps_c, const gsl_vector *rho)
{
    size_t i;
    gsl_vector *R_S = gsl_vector_alloc(rho->size);

    pow_vector(R_S, rho, -1. / 3.);
    gsl_vector_scale(R_S, pow(3. / FOUR_PI, 1. / 3.));

    for (i = 0; i < eps_c->size; i++) {
        // clang-format off
        gsl_vector_set(eps_c, i,
                       -2. * A * (1. + ALPHA_1 * gsl_vector_get(R_S, i)) *
                       log(1. + 1. / (2. * A * gsl_vector_get(R_S, i) *
                           (BETA_1 / sqrt(gsl_vector_get(R_S, i)) + BETA_2 +
                            BETA_3 * sqrt(gsl_vector_get(R_S, i)) +
                            BETA_4 * pow(gsl_vector_get(R_S, i), P)))));
        // clang-format on
    }

    gsl_vector_free(R_S);

    return 0;
}

/*
 * Compute the derivative of the Perdew-Wang correlation energy density.
 * der_eps_c: gsl_vector where to store the derivative of the correlation energy density [output]
 * rho: electronic density
 * It must be der_eps_c->size = rho->size.
 */
void _compute_der_corr_energy_density(gsl_vector *der_eps_c, const gsl_vector *rho)
{
    size_t i;
    double den, c1 = pow(3. / FOUR_PI, 1. / 3.), c2 = 2. * A * ALPHA_1 * c1, c3, rho_four_third;

    gsl_vector *rho_one_third = gsl_vector_alloc(rho->size);
    gsl_vector *R_S = gsl_vector_alloc(rho->size);

    pow_vector(rho_one_third, rho, 1. / 3.);
    gsl_vector_set_all(R_S, 1.);
    gsl_vector_div(R_S, rho_one_third);
    gsl_vector_scale(R_S, c1);

    for (i = 0; i < der_eps_c->size; i++) {
        den = 2. * A * gsl_vector_get(R_S, i) *
              (BETA_1 / sqrt(gsl_vector_get(R_S, i)) + BETA_2 +
               BETA_3 * sqrt(gsl_vector_get(R_S, i)) + BETA_4 * pow(gsl_vector_get(R_S, i), P));
        c3 = 1. + 1. / den;
        rho_four_third = pow(gsl_vector_get(rho_one_third, i), 4);

        // clang-format off
        gsl_vector_set(der_eps_c, i,
                       c2 / (3. * rho_four_third) * log(c3) -
                       (2. * A + c2 / gsl_vector_get(rho_one_third, i)) * 1. / (c3 * den * den) *
                       2. * A * (BETA_1 * sqrt(c1) /
                       (6. * pow(gsl_vector_get(rho_one_third, i), 7. / 2.)) + BETA_2 * c1 /
                       (3. * rho_four_third) + 0.5 * BETA_3 * pow(c1, 3. / 2.) /
                       pow(gsl_vector_get(rho, i), 3. / 2.) + 2. * BETA_4 * c1 * c1 /
                       (3. * pow(gsl_vector_get(rho_one_third, i), 5))));
        // clang-format on
    }

    gsl_vector_free(rho_one_third);
    gsl_vector_free(R_S);
}

/*
 * Fill V_xc with the exchange-correlation potential.
 * V_xc: gsl_vector where to store the exchange-correlation potential [output]
 * rho: electronic density
 * It must be V_xc->size >= rho->size; if V_xc->size > rho->size, then, in the computation, the
 * electronic density is considered null for r > (rho->size - 1) * h (where h is the discretization
 * step).
 */
int set_exchange_correlation_potential(gsl_vector *V_xc, const gsl_vector *rho)
{
    size_t i;

    gsl_vector *eps_x = gsl_vector_alloc(rho->size);
    gsl_vector *eps_c = gsl_vector_alloc(rho->size);
    gsl_vector *der_eps_c = gsl_vector_alloc(rho->size);

    compute_exchange_energy_density(eps_x, rho);
    compute_correlation_energy_density(eps_c, rho);
    _compute_der_corr_energy_density(der_eps_c, rho);

    for (i = 0; i < rho->size; i++) {
        // clang-format off
        gsl_vector_set(V_xc, i,
                       gsl_vector_get(eps_x, i) / 0.75 + gsl_vector_get(eps_c, i) +
                       gsl_vector_get(der_eps_c, i) * gsl_vector_get(rho, i));
        // clang-format on
    }

    set_value(V_xc, rho->size, V_xc->size - 1, 0.);

    gsl_vector_free(eps_x);
    gsl_vector_free(eps_c);
    gsl_vector_free(der_eps_c);

    return 0;
}

/*
 * Fill V_ks with the total mean-field potential (V_ext + V_d + V_xc).
 * V_ks: gsl_vector where to store the mean-field potential [output]
 * V_ext: external potential
 * r: points of the mesh
 * rho: electronic density
 * h: discretization step
 * It must be V_ks->size = V_ext->size = r->size >= rho->size; if V_ks->size > rho->size,
 * then, in the computation, the electronic density is considered null for r > (rho->size - 1) * h.
 */
int set_ks_potential(gsl_vector *V_ks, const gsl_vector *V_ext, const gsl_vector *r,
                     const gsl_vector *rho, const double h)
{
    gsl_vector *V_xc = gsl_vector_alloc(V_ks->size);

    set_direct_potential(V_ks, r, rho, h);
    set_exchange_correlation_potential(V_xc, rho);
    gsl_vector_add(V_ks, V_xc);
    gsl_vector_add(V_ks, V_ext);

    gsl_vector_free(V_xc);

    return 0;
}

/*
 * Add the centrifugal potential to an arbitrary potential, obtaining an effective potential.
 * V_eff: gsl_vector where to store the effective potential [output]
 * V: potential
 * r: points of the mesh
 * l: orbital angular momentum quantum number
 * It must be V_eff->size = V->size = r->size.
 */
int set_effective_potential(gsl_vector *V_eff, const gsl_vector *V, const gsl_vector *r,
                            const size_t l)
{
    set_centrifugal_potential(V_eff, r, l);
    gsl_vector_add(V_eff, V);

    return 0;
}

/*
 * Find the minimum value of an arbitrary potential.
 * V: potential
 * return: minimum value of the potential
 */
double find_potential_minimum(const gsl_vector *V)
{
    size_t i;
    double V_min = gsl_vector_get(V, 0);

    for (i = 1; i < V->size; i++) {
        if (gsl_vector_get(V, i) < V_min) {
            V_min = gsl_vector_get(V, i);
        }
    }

    return V_min;
}
