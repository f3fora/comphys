#ifndef PARTICLES_H
#define PARTICLES_H

#include <stdlib.h>
#include <math.h>
#include <stddef.h>
#include <mpi.h>

/*
 * Position of a particle.
 * r: double 3-array containing the coordinates of the particle:
 *    r[0] -> x, r[1] -> y, r[2] -> z
 */
typedef struct Particle {
    double r[3];
} Particle;

int place_particles(Particle *, const double, const size_t);
double pbc(const double, const double);
double compute_squared_distance(const Particle *, const Particle *, const double);
MPI_Datatype create_Particle_mpiDatatype();

#endif
