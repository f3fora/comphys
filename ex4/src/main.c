#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "autocorrelation.h"
#include "vmc.h"

/*
 * Names of the text files where the results will be saved:
 * - FILE_CE -> autocorrelation coefficient of the local energy
 * - FILE_CE_JF -> autocorrelation coefficient of the Jackson-Fenberg local energy
 * - FILE_RESULTS -> mean value of local energy (per particle), statistical error on the mean local
 *                   energy (per particle), mean value of JF local energy (per particle),
 *                   statistical error on the mean JF local energy (per particle), autocorrelation
 *                   length of local energy, error on the autocorrelation length of local energy,
 *                   autocorrelation length of JF local energy, error on the autocorrelation length
 *                   of JF local energy, final value of the parameter Delta divided by the side of
 *                   the simulation box, average acceptance rate during the statistical phase
 */
#define FILE_CE "ce"
#define FILE_CE_JF "ce_jf"
#define FILE_RESULTS "res"

char *concat(const char *, const char *);

/*
 * Execute the function vmc of vmc.h and save the results to text files.
 * It must be run with the following parameters:
 * path to save the files, seed, b, n_cells, rho, size_CE, size_CE_JF, M_eq, M_eq_procs, M_stat,
 * acc_rate_min, acc_rate_max, delta_ini_over_L, dDelta_over_L, steps_control.
 * For more details, see the documentation of the function vmc of vmc.h.
 * For the parallel execution: compile using "mpicc", run using "mpiexec -n <number_of_processes>"
 * or "mpirun -np <number_of_processes>".
 */
int main(int argc, char *argv[])
{
    size_t seed, n_cells, size_CE, size_CE_JF, M_eq, M_eq_procs, M_stat, steps_control;
    double b, rho, acc_rate_min, acc_rate_max, delta_ini_over_L, dDelta_over_L;

    double E_mean, E_std, E_JF_mean, E_JF_std;
    double tau, tau_std, tau_JF, tau_JF_std, delta_over_L, acc_rate;
    double *CE, *CE_JF;

    char *path, *file_name;
    int rank;

    FILE *file;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    if (argc != 16) {
        return 1;
    }

    path = argv[1];
    seed = atol(argv[2]);
    b = atof(argv[3]);
    n_cells = atol(argv[4]);
    rho = atof(argv[5]);
    size_CE = atol(argv[6]);
    size_CE_JF = atol(argv[7]);
    M_eq = atol(argv[8]);
    M_eq_procs = atol(argv[9]);
    M_stat = atol(argv[10]);
    acc_rate_min = atof(argv[11]);
    acc_rate_max = atof(argv[12]);
    delta_ini_over_L = atof(argv[13]);
    dDelta_over_L = atof(argv[14]);
    steps_control = atol(argv[15]);

    CE = malloc(size_CE * sizeof(double));
    CE_JF = malloc(size_CE_JF * sizeof(double));

    vmc(seed, b, n_cells, rho, size_CE, size_CE_JF, M_eq, M_eq_procs, M_stat, acc_rate_min,
        acc_rate_max, delta_ini_over_L, dDelta_over_L, steps_control, &E_mean, &E_std, &E_JF_mean,
        &E_JF_std, CE, &tau, &tau_std, CE_JF, &tau_JF, &tau_JF_std, &delta_over_L, &acc_rate);

    if (rank == 0) {
        file_name = concat(path, FILE_CE);
        save_autocorr(CE, size_CE, file_name);
        free(file_name);

        file_name = concat(path, FILE_CE_JF);
        save_autocorr(CE_JF, size_CE_JF, file_name);
        free(file_name);

        file_name = concat(path, FILE_RESULTS);
        file = fopen(file_name, "w");

        fprintf(file, "%f\n", E_mean);
        fprintf(file, "%f\n", E_std);
        fprintf(file, "%f\n", E_JF_mean);
        fprintf(file, "%f\n", E_JF_std);
        fprintf(file, "%f\n", tau);
        fprintf(file, "%f\n", tau_std);
        fprintf(file, "%f\n", tau_JF);
        fprintf(file, "%f\n", tau_JF_std);
        fprintf(file, "%f\n", delta_over_L);
        fprintf(file, "%f\n", acc_rate);

        fclose(file);
        free(file_name);
    }

    free(CE);
    free(CE_JF);

    MPI_Finalize();

    return 0;
}

/*
 * Concatenate two strings.
 * s1: 1st string
 * s2: 2nd string
 * return: concatenation of the two strings
 */
char *concat(const char *s1, const char *s2)
{
    char *res = malloc(strlen(s1) + strlen(s2) + 1);

    strcpy(res, s1);
    strcat(res, s2);

    return res;
}
