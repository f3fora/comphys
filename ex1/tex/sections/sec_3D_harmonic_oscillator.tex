\section{Three-Dimensional Harmonic Oscillator} \label{sec: 3D_ho}

The application of the Numerov method is not limited to 1D problems. The algorithm can also be exploited to solve the radial Schrödinger equation of a 3D spherically symmetric problem. In this section, we consider the dimensionless Schrödinger equation of a 3D harmonic oscillator:
\begin{equation}
    \left(-\frac{1}{2}\nabla^2 + \frac{1}{2}r^2\right)\!\psi(r,\theta,\phi) = E\psi(r,\theta,\phi)\,,
\end{equation}
which has solutions 
\begin{equation}
    \psi_{n\ell m}(r,\theta,\phi) = \frac{R_{n\ell}(r)}{r}\,Y_{\ell m}(\theta,\phi)\,,
\end{equation} 
where $Y_{\ell m}(\theta,\phi)$ are the spherical harmonics and the functions $R_{n\ell}(r)$ satisfy the radial equation
\begin{equation}\label{eq: 3D_ho_se}
    \left(-\dv[2]{r}+\frac{\ell(\ell+1)}{r^2}+r^2\right)\!R_{n\ell}(r) = 2E_{n\ell} R_{n\ell}(r)\,.
\end{equation}
This has solutions
\begin{equation}
    R_{n\ell}(r) = c_{n\ell} r^{\ell+1}L_{n}^{\ell + \sfrac{1}{2}}(r^2)e^{-r^2 / 2}\,,
\end{equation}
where
\begin{equation}\label{eq: theo_wf_3dho}
    L^{(\alpha)}_n(x) = \frac{1}{n!} x^{-\alpha} e^x \dv[n]{x}\!\left(x^{n+\alpha}e^{-x}\right)
\end{equation}
are the generalized Laguerre polynomials and $c_{n\ell}$ are normalization constants. The corresponding eigenenergies are given by
\begin{equation}\label{eq: theo_el_3dho}
    E_{n\ell} = 2n + \ell + \frac{3}{2} \quad \text{with }n = 0,\,1,\,2\dots,\,\ell = 0,\,1,\,2\dots
\end{equation}

In this case, with reference to the \autoref{sec: numerov}, we have
\begin{equation}
    K^2(r_i) = 2E - \frac{\ell(\ell+1)}{r_i^2} - r_i^2\,.
\end{equation}
To set the initial conditions, as done in the previous section, we consider the $r \to 0$ limit:
\begin{equation}
    R_{n\ell}(r) \sim r^{\ell + 1}\,,
\end{equation}
where the normalization is arbitrary. Hence, for the research of each energy level, we start the Numerov algorithm with the conditions
\begin{equation}
    R_{n\ell}(r_{low}) = r_{low}^{\ell + 1}\,, \quad R_{n\ell}(r_{low} + h) = (r_{low} + h)^{\ell + 1}\,,
\end{equation}
with $r_{low} \neq 0$ sufficiently small, since the centrifugal potential diverges for $r \to 0$. The spectrum of the 3D harmonic oscillator is found for $n \le 2$ and $\ell \le 2$, following the same procedure adopted in the 1D case of the previous section.

The wave functions and the corresponding eigenenergies numerically found are represented in \autoref{fig:3D_ho_wf_l0}, \autoref{fig:3D_ho_E}, \autoref{fig:3D_ho_wf_l1} and \autoref{fig:3D_ho_wf_l2} (the latter two reported in \autoref{app: figures}). To get each plot, we set $r_{low} = h = 10^{-5}$, $r_{M} = 6$, $\delta E = \num{1.1e-2}$, $\varepsilon = 10^{-4}$.

\inputfigure{3D_ho_wf_l0}{3D Harmonic Oscillator: Wave Functions for $\boldsymbol{\ell = 0}$}{The first plot shows the normalized wave functions $R_{n\ell}(r)$ describing the first 3 eigenstates of the 3D harmonic oscillator (described by equation \eqref{eq: 3D_ho_se}), computed numerically using the Numerov algorithm \eqref{eq: numerov}, by setting $\ell = 0$. The successive plots, for each index $n$, show the point by point squared difference $\Delta^2R_{n\ell}(r)$ between the numerically computed wave function and the theoretical one (given by equation \eqref{eq: theo_wf_3dho}).}

\autoref{fig:3D_ho_wf_l0}, \autoref{fig:3D_ho_wf_l1} and \autoref{fig:3D_ho_wf_l2} show a good agreement between the wave functions computed numerically and the analytical solutions \eqref{eq: theo_wf_3dho}. However, as in the 1D case, we observe a progressive divergence of the numerical wave functions from the analytical ones for $r$ approaching $r_M$. This is probably due to the same reasons reported in the previous section.

\autoref{fig:3D_ho_E} shows how the eigenenergies calculated numerically are compatible, within their uncertainty, with those predicted by the theoretical formula \eqref{eq: theo_el_3dho}.

As in the previous section, we investigate the dependence of our numerical solutions on the step $h \propto 1 / M$. The results are represented in \autoref{fig:3D_ho_DE} and \autoref{fig:3D_ho_MSE}.

\autoref{fig:3D_ho_DE} shows how the numerical approximation of the energy levels $E_{n\ell}$ improves with the reduction of the step $h$, as in the 1D case. In particular, for $h \le 10^{-2}$, the discrepancy between numerically calculated and theoretical eigenenergies is $\abs{\Delta E} < 10^{-4}$ for each $n$ and $\ell$ considered.

\inputfigure{3D_ho_E}{3D Harmonic Oscillator: Energy Spectrum}{The figure illustrates the energy spectrum of the 3D harmonic oscillator (described by equation \eqref{eq: 3D_ho_se}) computed numerically. The plot shows the eigenenergies $E_{n\ell}$ and the corresponding wave functions $R_{n\ell}(r)$ for $n \le 2$ and $\ell \le 2$. The energy levels are compatible with those predicted by formula \eqref{eq: theo_el_3dho}.}

\autoref{fig:3D_ho_MSE} does not show a significant dependence of the MSE, which affects the numerical wave functions, on the step $h$. For the considered values of $h$, the plotted curves are practically constant or seem to fluctuate around a mean value. Moreover, we do not detect substantial differences for $n$ even and $n$ odd, unlike the 1D case. This is probably due to the fact that, for the 3D harmonic oscillator, the behaviour of the wave functions around the origin is independent on the parity of $n$, since $R_{n\ell}(r) \sim r^{\ell+1}$ for all $n$.

\inputfigure[.95]{3D_ho_DE}{3D Harmonic Oscillator: Eigenenergies vs Step}{The figure shows the dependence of the difference $\Delta E$ between the numerically computed energy levels and the analytical ones on the step $h$ in the Numerov algorithm \eqref{eq: numerov}. The plot is obtained by setting $r_{low} = 10^{-5}$, $r_{M} = 6$, $\delta E = \num{1.1e-2}$, $\varepsilon = 10^{-4}$. For each $n$ and $\ell$, the discrepancy between numerically calculated and theoretical eigenenergies grows with the increase of the step.} 

\inputfigure[.95]{3D_ho_MSE}{3D Harmonic Oscillator: Wave Functions vs Step}{The figure shows the dependence of the mean squared error (MSE), which affects the numerically computed wave functions with respect to the analytical ones, on the step $h$ in the Numerov algorithm \eqref{eq: numerov}. The plot is obtained by setting $r_{low} = 10^{-5}$, $r_{M} = 6$, $\delta E = \num{1.1e-2}$, $\varepsilon = 10^{-4}$. For the considered values of $h$, a significant dependence of MSE on the step is not observed.}