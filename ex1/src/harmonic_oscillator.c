#include "harmonic_oscillator.h"

/*
 * Calculate the 1D harmonic potential at point x.
 * x: point where to evaluate the potential
 * return: harmonic potential evaluated in x
 */
double _potential_1Dho(const double x)
{
    return 0.5 * x * x;
}

/*
 * Fill V with the 1D harmonic potential evaluated at the points in x.
 * V: gsl_vector where to store the potential [output]
 * x: points where to evaluate the potential
 */
void _set_potential_1Dho(gsl_vector *V, const gsl_vector *x)
{
    size_t i;

    for (i = 0; i < V->size; i++) {
        gsl_vector_set(V, i, _potential_1Dho(gsl_vector_get(x, i)));
    }
}

/*
 * Put, in the first two items of y, the initial conditions for the Numerov method in the 1D
 * harmonic oscillator case.
 * y: gsl_vector where to store the initial conditions [output]
 * n: principal quantum number
 * r_low: initial value of the spatial coordinate
 * h: step for the Numerov algorithm
 */
void _set_initial_conditions_1Dho(gsl_vector *y, const size_t n, const double r_low, const double h)
{
    if (n % 2 != 0) {
        gsl_vector_set(y, 0, r_low);
        gsl_vector_set(y, 1, r_low + h);
    } else {
        gsl_vector_set(y, 0, 1);
        gsl_vector_set(y, 1, 1);
    }
}

/*
 * Calculate harmonic potential and centrifugal potential at point x.
 * x: point where to evaluate the potential
 * l: orbital angular momentum quantum number
 * return: harmonic potential + centrifugal potential at point x
 */
double _potential_3Dho(const double x, const size_t l)
{
    return 0.5 * (x * x + (double)l * ((double)l + 1.) / (x * x));
}

/*
 * Fill V with harmonic potential + centrifugal potential evaluated at the points in x.
 * V: gsl_vector where to store the potential [output]
 * x: points where to evaluate the potential
 * l: orbital angular momentum quantum number
 */
void _set_potential_3Dho(gsl_vector *V, const gsl_vector *x, const size_t l)
{
    size_t i;

    for (i = 0; i < V->size; i++) {
        gsl_vector_set(V, i, _potential_3Dho(gsl_vector_get(x, i), l));
    }
}

/*
 * Put, in the first two items of y, the initial conditions for the Numerov method in the 3D
 * harmonic oscillator case.
 * y: gsl_vector where to store the initial conditions [output]
 * l: orbital angular momentum quantum number
 * r_low: initial value of the spatial coordinate
 * h: step for the Numerov algorithm
 */
void _set_initial_conditions_3Dho(gsl_vector *y, const size_t l, const double r_low, const double h)
{
    gsl_vector_set(y, 0, pow(r_low, l + 1));
    gsl_vector_set(y, 1, pow(r_low + h, l + 1));
}

/*
 * Find the energy levels and the corresponding wave functions for n <= n_max in the 1D harmonic
 * oscillator case.
 * INPUTS:
 * n_max: maximum value of the principal quantum number
 * DE: energy increase for find_next_eigen
 * thresh: twice the error on each eigenenergy found
 * r_low: initial value of the spatial coordinate for the wave function calculation
 * h: step for the Numerov algorithm
 * M: number of points at which each wave function is evaluated
 * OUTPUTS:
 * x: pointer to a double M-array, where the spatial coordinate will be stored
 * y: pointer to a double [M * (n_max + 1)]-array, where the wave functions will be stored
 * E: pointer to a double (n_max + 1)-array, where the energy levels will be stored
 * dE: pointer to a double (n_max + 1)-array, where the errors on the eigenenergies will be stored
 * return: if the secant algorithm has terminated successfully for each value of the principal
 *         quantum number: 0
 *         else: number of values of the principal quantum number for which the maximum number of
 *               iterations MAX_SEC has been reached (see numerov.h)
 */
int main_harmonic_oscillator_1D(const size_t n_max, const double DE, const double thresh,
                                const double r_low, const double h, const size_t M, double *x,
                                double *y, double *E, double *dE)
{
    size_t n;
    int out = 0;
    double _E = 0.;

    gsl_vector_view _x = gsl_vector_view_array(x, M);
    gsl_matrix_view _y_matrix = gsl_matrix_view_array(y, n_max + 1, M);
    gsl_vector_view _y;
    gsl_vector *V = gsl_vector_alloc(M);

    set_coordinate(&_x.vector, r_low, h);
    _set_potential_1Dho(V, &_x.vector);

    for (n = 0; n <= n_max; n++) {
        _y = gsl_matrix_row(&_y_matrix.matrix, n);
        _set_initial_conditions_1Dho(&_y.vector, n, r_low, h);
        out += find_next_eigen(&E[n], &_y.vector, V, _E, DE, thresh, B_HO, h);
        dE[n] = thresh * 0.5;
        _E = E[n] + DE;
    }

    gsl_vector_free(V);

    return out;
}

/*
 * Find the energy levels and the corresponding wave functions for n <= n_max and l fixed in the 3D
 * harmonic oscillator case.
 * INPUTS:
 * n_max: maximum value of the principal quantum number
 * l: orbital angular momentum quantum number
 * DE: energy increase for find_next_eigen
 * thresh: twice the error on each eigenenergy found
 * r_low: initial value of the spatial coordinate for the wave function calculation
 * h: step for the Numerov algorithm
 * M: number of points at which each wave function is evaluated
 * OUTPUTS:
 * x: pointer to a double M-array, where the spatial coordinate will be stored
 * y: pointer to a double [M * (n_max + 1)]-array, where the wave functions will be stored
 * E: pointer to a double (n_max + 1)-array, where the energy levels will be stored
 * dE: pointer to a double (n_max + 1)-array, where the errors on the eigenenergies will be stored
 * return: if the secant algorithm has terminated successfully for each value of the principal
 *         quantum number: 0
 *         else: number of values of the principal quantum number for which the maximum number of
 *               iterations MAX_SEC has been reached (see numerov.h)
 */
int main_harmonic_oscillator_3D(const size_t n_max, const size_t l, const double DE,
                                const double thresh, const double r_low, const double h,
                                const size_t M, double *x, double *y, double *E, double *dE)
{
    size_t n;
    int out = 0;
    double _E = 0.;

    gsl_vector_view _x = gsl_vector_view_array(x, M);
    gsl_matrix_view _y_matrix = gsl_matrix_view_array(y, n_max + 1, M);
    gsl_vector_view _y;
    gsl_vector *V = gsl_vector_alloc(M);

    set_coordinate(&_x.vector, r_low, h);
    _set_potential_3Dho(V, &_x.vector, l);

    for (n = 0; n <= n_max; n++) {
        _y = gsl_matrix_row(&_y_matrix.matrix, n);
        _set_initial_conditions_3Dho(&_y.vector, l, r_low, h);
        out += find_next_eigen(&E[n], &_y.vector, V, _E, DE, thresh, B_HO, h);
        dE[n] = thresh * 0.5;
        _E = E[n] + DE;
    }

    gsl_vector_free(V);

    return out;
}
