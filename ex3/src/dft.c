#include "dft.h"

/*
 * Put, in the first two items of a gsl_vector, the initial conditions for the Numerov method.
 * orbital: gsl_vector where to store the initial conditions [output]
 * r_low: 1st point of the mesh
 * h: discretization step
 * l: orbital angular momentum quantum number
 */
void _set_initial_conditions(gsl_vector *orbital, const double r_low, const double h,
                             const size_t l)
{
    gsl_vector_set(orbital, 0, pow(r_low, l + 1));
    gsl_vector_set(orbital, 1, pow(r_low + h, l + 1));
}

/*
 * Normalize an orbital.
 * orbital: orbital to normalize [output]
 * r: radial coordinate
 * h: discretization step
 * It must be orbital->size <= r->size.
 */
void _normalize(gsl_vector *orbital, const gsl_vector *r, const double h)
{
    gsl_vector_const_view _r = gsl_vector_const_subvector(r, 0, orbital->size);
    gsl_vector *integ = gsl_vector_alloc(orbital->size);

    pow_vector(integ, orbital, 2);
    gsl_vector_scale(orbital, 1. / sqrt(integral(integ, h)));

    gsl_vector_div(orbital, &_r.vector);

    gsl_vector_free(integ);
}

/*
 * Compute the electronic density.
 * rho: m-gsl_vector where to store the electronic density [output]
 * orbitals: nxm gsl_matrix whose rows contain the normalized orbitals
 * degeneracy: pointer to a size_t n-array containing the degeneracy of the energy levels
 */
void _compute_density(gsl_vector *rho, gsl_matrix *orbitals, const size_t *degeneracy)
{
    size_t i;
    gsl_vector_view orbital;
    gsl_vector *temp = gsl_vector_alloc(orbitals->size2);

    gsl_vector_set_zero(rho);

    for (i = 0; i < orbitals->size1; i++) {
        orbital = gsl_matrix_row(orbitals, i);
        pow_vector(temp, &orbital.vector, 2);
        gsl_vector_scale(temp, degeneracy[i]);
        gsl_vector_add(rho, temp);
    }

    gsl_vector_scale(rho, 1. / FOUR_PI);

    gsl_vector_free(temp);
}

/*
 * Compute the degeneracy of an energy level with a certain value of the orbital angular momentum
 * quantum number.
 * l: orbital angular momentum quantum number
 * return: degeneracy of the level
 */
size_t _degeneracy(const size_t l)
{
    return 2 * (2 * l + 1);
}

/*
 * Compute, through the function find_next_eigen of numerov.h, the energy levels with principal
 * quantum number n <= n_max and orbital angular momentum quantum number l fixed.
 * orbitals: gsl_matrix whose rows will contain the normalized orbitals [output]
 * E: pointer to a double array, where the eigenenergies will be stored [output]
 * first_idx: index of the 1st row of the gsl_matrix orbitals and of the 1st item of the array
 *            pointed by E to fill
 * n_max: maximum value of the principal quantum number
 * r: radial coordinate
 * V: potential (without centrifugal component)
 * l: orbital angular momentum quantum number
 * np: Numerov parameters
 * return: if the secant algorithm used by find_next_eigen has terminated successfully for each
 *         value of the principal quantum number: 0
 *         else: number of values of the principal quantum number for which the maximum number of
 *               iterations MAX_SEC has been reached (see numerov.h)
 * The size of the array pointed by E must be equal to orbitals->size1. Moreover, it must be
 * orbitals->size2 <= r->size = V->size.
 */
int _compute_levels(gsl_matrix *orbitals, double *E, const size_t first_idx, const size_t n_max,
                    const gsl_vector *r, const gsl_vector *V, const size_t l,
                    const NumerovParams np)
{
    size_t i, last_idx = first_idx + n_max;
    int out = 0;
    double E_min;

    gsl_vector_view orbital, _orbital_cut;
    gsl_vector *_orbital = gsl_vector_alloc(r->size);
    gsl_vector *V_eff = gsl_vector_alloc(r->size);

    set_effective_potential(V_eff, V, r, l);
    E_min = find_potential_minimum(V_eff);

    for (i = first_idx; i <= last_idx; i++) {
        _set_initial_conditions(_orbital, np.r_low, np.h, l);
        out += find_next_eigen(&E[i], _orbital, V_eff, E_min, np.dE, np.thresh, 1., np.h);
        E_min = E[i] + np.dE;

        orbital = gsl_matrix_row(orbitals, i);
        _orbital_cut = gsl_vector_subvector(_orbital, 0, orbitals->size2);
        gsl_vector_memcpy(&orbital.vector, &_orbital_cut.vector);
        _normalize(&orbital.vector, r, np.h);
    }

    gsl_vector_free(_orbital);
    gsl_vector_free(V_eff);

    return out;
}

/*
 * Given a set of energy levels, find those occupied and determine if the system is closed-shell.
 * levels: pointer to an initialized free Levels variable, where the occupied energy levels and the
 *         corresponding orbitals will be stored [output]
 * orbitals: gsl_matrix whose rows contain the normalized orbitals
 * E: pointer to a double array containing the eigenenergies
 * num_n: number of values of the principal quantum number
 * num_l: number of values of the orbital angular momentum quantum number
 * N: number of valence electrons
 * return: if the system is closed-shell: true
 *         else: false
 * The energy of the level (n, l) must be stored in the n + l * num_n position of the array pointed
 * by E, while the corresponding orbital must be stored in the n + l * num_n row of the gsl_matrix
 * orbitals. The size of the array pointed by E and orbitals->size1 must be equal to num_n * num_l.
 * Warning: in determining which systems are closed-shell, the function does not check if levels
 *          identified by different pairs of quantum numbers are degenerate in energy.
 */
bool _find_occupied_levels(Levels *levels, gsl_matrix *orbitals, const double *E,
                           const size_t num_n, const size_t num_l, const size_t N)
{
    size_t N_placed, n, l, n_min, l_min, idx;
    double E_min;
    size_t last_n[num_l];
    gsl_vector_view orbital;

    for (l = 0; l < num_l; l++) {
        last_n[l] = -1;
    }

    for (N_placed = 0; N_placed < N; N_placed += _degeneracy(l_min), last_n[l_min] = n_min) {
        for (l = 0, E_min = 1.; l < num_l; l++) {
            for (n = last_n[l] + 1; n < num_n; n++) {
                idx = n + l * num_n;
                if (E[idx] < E_min) {
                    n_min = n;
                    l_min = l;
                    E_min = E[idx];
                    break;
                }
            }
        }

        orbital = gsl_matrix_row(orbitals, n_min + l_min * num_n);

        Levels_add(levels, &orbital.vector, E_min, n_min, l_min);
    }

    return (N == N_placed);
}

/*
 * Compute, using the functions _compute_levels and _find_occupied_levels, the occupied energy
 * levels and determine if the system is closed-shell.
 * out: integer returned by the function _compute_levels [output]
 * levels: pointer to an initialized free Levels variable, where the occupied energy levels and the
 *         corresponding normalized orbitals will be stored [output]
 * N: number of valence electrons
 * r: radial coordinate
 * V: potential (without centrifugal component)
 * np: Numerov parameters
 * return: if the system is closed-shell: true
 *         else: false
 * Warning: in determining which systems are closed-shell, the function does not check if levels
 *          identified by different pairs of quantum numbers are degenerate in energy.
 */
bool _compute_occupied_levels(int *out, Levels *levels, const size_t N, const gsl_vector *r,
                              const gsl_vector *V, const NumerovParams np)
{
    size_t i, num_l, num_n = N / 2, n_max, size, deg_old, deg_tot;
    bool closed_shell_system;
    double *E = NULL;
    gsl_matrix *orbitals = NULL;

    for (i = 0, num_l = 0; i < N; num_l++) {
        i += _degeneracy(num_l);
    }
    size = num_n * num_l;

    E = malloc(size * sizeof(double));
    for (i = 0; i < size; i++) {
        E[i] = DBL_MAX;
    }

    orbitals = gsl_matrix_alloc(size, levels->M);

    for (i = 0, deg_old = 0, deg_tot = 0; i < num_l; deg_old = deg_tot, i++) {
        deg_tot += _degeneracy(i);
        n_max = (N - deg_old) / deg_tot + ((N - deg_old) % deg_tot > 0) - 1;
        *out += _compute_levels(orbitals, E, i * num_n, n_max, r, V, i, np);
    }

    closed_shell_system = _find_occupied_levels(levels, orbitals, E, num_n, num_l, N);

    free(E);
    gsl_matrix_free(orbitals);

    return closed_shell_system;
}

/*
 * Compute the difference between the values of two different expressions for the ground state
 * energy of the system with interacting electrons, in order to check if the self-consistent field
 * algorithm has reached convergence.
 * levels: pointer to a Levels variable containing the occupied energy levels and the corresponding
 *         normalized orbitals
 * degeneracy: pointer to a double (levels->size)-array containing the degeneracy of the levels
 * r: radial coordinate
 * V_ext: external potential
 * rho: electronic density
 * h: discretization step
 * return: difference between the values of the two different expressions for the ground state
 *         energy; if the convergence is reached, this quantity tends to zero
 * It must be levels->M = rho->size <= r->size = V_ext->size; if rho->size < r->size, then, in the
 * computation, orbitals and electronic density are considered null for r > (rho->size - 1) * h.
 */
double _check_convergence(const Levels *levels, const size_t *degeneracy, const gsl_vector *r,
                          const gsl_vector *V_ext, const gsl_vector *rho, const double h)
{
    size_t i;
    double conv, e_tot = 0.;

    gsl_vector_view orbital;
    gsl_vector_const_view _r = gsl_vector_const_subvector(r, 0, rho->size);
    gsl_vector_const_view _V_ext = gsl_vector_const_subvector(V_ext, 0, rho->size);

    gsl_vector *integ = gsl_vector_alloc(rho->size);
    gsl_vector *aus = gsl_vector_alloc(rho->size);
    gsl_vector *laplacian = gsl_vector_alloc(rho->size);

    gsl_vector_set_zero(integ);

    for (i = 0; i < levels->size; i++) {
        e_tot += levels->E[i] * degeneracy[i];

        orbital = Levels_get_orbital(levels, i);

        multiply_vectors(aus, &orbital.vector, &_r.vector);
        second_derivative(laplacian, aus, h);
        gsl_vector_div(laplacian, &_r.vector);
        gsl_vector_scale(laplacian, 0.5);

        set_centrifugal_potential(aus, &_r.vector, levels->l[i]);
        gsl_vector_mul(aus, &orbital.vector);

        gsl_vector_sub(aus, laplacian);
        gsl_vector_mul(aus, &orbital.vector);
        gsl_vector_scale(aus, _degeneracy(levels->l[i]));

        gsl_vector_add(integ, aus);
    }

    gsl_vector_scale(integ, 1. / FOUR_PI);

    set_ks_potential(aus, &_V_ext.vector, &_r.vector, rho, h);
    gsl_vector_mul(aus, rho);
    gsl_vector_add(integ, aus);

    gsl_vector_mul(integ, &_r.vector);
    gsl_vector_mul(integ, &_r.vector);

    conv = FOUR_PI * integral(integ, h) - e_tot;

    gsl_vector_free(integ);
    gsl_vector_free(aus);
    gsl_vector_free(laplacian);

    return conv;
}

/*
 * Compute the ground state energy of the system with interacting electrons.
 * levels: pointer to a Levels variable containing the occupied energy levels
 * r: radial coordinate
 * rho: electronic density
 * h: discretization step
 * return: ground state energy of the system
 * It must be rho->size <= r->size.
 */
double _compute_energy(const Levels *levels, const gsl_vector *r, const gsl_vector *rho,
                       const double h)
{
    size_t i;
    double e_tot = 0., energy;

    gsl_vector_const_view _r = gsl_vector_const_subvector(r, 0, rho->size);

    gsl_vector *integ = gsl_vector_alloc(rho->size);
    gsl_vector *eps_x = gsl_vector_alloc(rho->size);
    gsl_vector *eps_c = gsl_vector_alloc(rho->size);
    gsl_vector *V_xc = gsl_vector_alloc(rho->size);

    for (i = 0; i < levels->size; i++) {
        e_tot += levels->E[i] * _degeneracy(levels->l[i]);
    }

    set_direct_potential(integ, r, rho, h);
    gsl_vector_scale(integ, -0.5);
    set_exchange_correlation_potential(V_xc, rho);
    gsl_vector_sub(integ, V_xc);

    compute_exchange_energy_density(eps_x, rho);
    compute_correlation_energy_density(eps_c, rho);

    gsl_vector_add(integ, eps_x);
    gsl_vector_add(integ, eps_c);

    gsl_vector_mul(integ, rho);
    gsl_vector_mul(integ, &_r.vector);
    gsl_vector_mul(integ, &_r.vector);

    energy = FOUR_PI * integral(integ, h) + e_tot;

    gsl_vector_free(integ);
    gsl_vector_free(eps_x);
    gsl_vector_free(eps_c);
    gsl_vector_free(V_xc);

    return energy;
}

/*
 * Mix two vectors, i.e. compute a weighted sum of the two.
 * vec_1: m-gsl_vector, where (1 - mix) * vec_1 + mix * vec_2 will be stored [output]
 * vec_2: m-gsl_vector (at the end, it will be multiplied by mix)
 * mix: mixing parameter
 */
void _mixing(gsl_vector *vec_1, gsl_vector *vec_2, const double mix)
{
    gsl_vector_scale(vec_1, 1. - mix);
    gsl_vector_scale(vec_2, mix);
    gsl_vector_add(vec_1, vec_2);
}

/*
 * Compute the numbers of valence electrons in an alkali metal cluster for which closed-shell
 * systems are obtained, treating the electrons as independent. For each closed-shell system,
 * compute the occupied energy levels, the corresponding normalized orbitals, the electronic density
 * and the total ground state energy.
 * INPUTS:
 * num_cs_systems: number of closed-shell systems to find, starting from N = 2 valence electrons
 * r_s: Wigner-Seitz radius of the considered atomic species
 * np: Numerov parameters
 * M: number of points of the mesh on which the external potential is discretized
 * M_cut: number of points of the mesh on which the orbitals and the electronic density are
 *        discretized
 * OUTPUTS:
 * N: pointer to a size_t (num_cs_systems)-array, where the numbers of valence electrons that
 *    characterize the first num_cs_systems closed-shell systems will be stored
 * E_tot: pointer to a double (num_cs_systems)-array, where the total ground state energies of the
 *        computed closed-shell systems will be stored
 * r: pointer to a double M-array, where the spatial mesh will be stored
 * V_ext: pointer to a double (num_cs_systems * M)-array, where the external potentials of the
 *        computed closed-shell systems will be stored; the array will contain the external
 *        potential of the m-th closed-shell system in the range of positions
 *        [m * M, (m + 1) * M - 1]
 * rho: pointer to a double (num_cs_systems * M_cut)-array, where the electronic densities of the
 *      computed closed-shell systems will be stored; the array will contain the electronic density
 *      of the m-th closed-shell system in the range of positions [m * M_cut, (m + 1) * M_cut - 1]
 * levels: pointer that will point to a Levels variable containing the occupied energy levels, with
 *         the corresponding normalized orbitals, of each closed-shell system
 * levels_sizes: pointer to a size_t (num_cs_systems)-array, where the number of occupied levels for
 *               each closed-shell system will be stored; the first levels_sizes[0] levels in the
 *               Levels variable pointed by levels will be related to the 1st closed-shell system,
 *               the next levels_sizes[1] levels will be related to the 2nd closed-shell system, and
 *               so on
 * return: if the secant algorithm used in the search for the energy levels has terminated
 *         successfully for each computed level: 0
 *         else: number of times the maximum number of iterations MAX_SEC has been reached (see
 *               numerov.h)
 * It must be M >= M_cut.
 * Warning: in determining which systems are closed-shell, the function does not check if levels
 *          identified by different pairs of quantum numbers are degenerate in energy.
 */
int compute_closed_shell_systems(const size_t num_cs_systems, const double r_s,
                                 const NumerovParams np, const size_t M, const size_t M_cut,
                                 size_t *N, double *E_tot, double *r, double *V_ext, double *rho,
                                 Levels *levels, size_t *levels_sizes)
{
    size_t i = 0, _N = 2, j;
    int out = 0;
    size_t *degeneracy = NULL;
    Levels aus_levels;

    gsl_vector_view _r = gsl_vector_view_array(r, M);
    gsl_matrix_view _V_ext = gsl_matrix_view_array(V_ext, num_cs_systems, M);
    gsl_matrix_view _rho = gsl_matrix_view_array(rho, num_cs_systems, M_cut);
    gsl_vector_view _V_ext_row;
    gsl_vector_view _rho_row;
    gsl_matrix_view orbitals;

    Levels_initialize(levels, M_cut);
    Levels_initialize(&aus_levels, M_cut);

    set_coordinate(&_r.vector, np.r_low, np.h);

    do {
        _V_ext_row = gsl_matrix_row(&_V_ext.matrix, i);
        set_external_potential(&_V_ext_row.vector, &_r.vector, r_s, _N);

        if (_compute_occupied_levels(&out, &aus_levels, _N, &_r.vector, &_V_ext_row.vector, np)) {
            _rho_row = gsl_matrix_row(&_rho.matrix, i);
            orbitals = gsl_matrix_view_array(aus_levels.orbitals, aus_levels.size, aus_levels.M);

            degeneracy = realloc(degeneracy, aus_levels.size * sizeof(size_t));
            for (j = 0, E_tot[i] = 0.; j < aus_levels.size; j++) {
                degeneracy[j] = _degeneracy(aus_levels.l[j]);
                E_tot[i] += aus_levels.E[j] * degeneracy[j];
            }

            _compute_density(&_rho_row.vector, &orbitals.matrix, degeneracy);

            levels_sizes[i] = aus_levels.size;
            Levels_append(levels, &aus_levels);

            N[i] = _N;

            i++;
        }

        Levels_free(&aus_levels);

        _N += 2;
    } while (i < num_cs_systems);

    free(degeneracy);

    return out;
}

/*
 * Solve, through a self-consistent field procedure, the Kohn-Sham equations for an alkali metal
 * cluster described by a jellium model, obtaining the effective mean-field potential, the ground
 * state energy and the electronic density.
 * INPUTS:
 * N: number of valence electrons
 * r_s: Wigner-Seitz radius of the considered atomic species
 * np: Numerov parameters
 * M: number of points of the mesh on which the potential is discretized
 * M_cut: number of points of the mesh on which the orbitals and the electronic density are
 *        discretized
 * mix: mixing parameter
 * thresh_scf: threshold for evaluating the convergence of the self-consistent field calculation;
 *             the algorithm ends when the result of the function _check_convergence is in modulus
 *             less than thresh_scf for two consecutive iterations
 * OUTPUTS:
 * E_tot: pointer to a double variable, where the ground state energy will be stored
 * r: pointer to a double M-array, where the spatial mesh will be stored
 * V_ks: pointer to a double M-array, where the effective mean-field potential (V_ext + V_d + V_xc)
 *       will be stored
 * rho: pointer to a double (M_cut)-array, where the electronic density will be stored
 * levels: pointer that will point to a Levels variable containing the occupied energy levels
 *         (without physical meaning), with the corresponding normalized orbitals
 * num_iter: pointer to a size_t variable, where the number of iterations required for the
 *           convergence will be stored; if the algorithm does not reach convergence within
 *           NUM_ITER_SCF_MAX iterations, the variable pointed by num_iter will contain 0
 * return: if the system is closed-shell and the final levels correspond to the initial ansatz:
 *             if the secant algorithm used in the search for the energy levels has terminated
 *             successfully for each computed level: 0
 *             else: number of times the maximum number of iterations MAX_SEC has been reached (see
 *                   numerov.h)
 *         else:
 *             if the system is not closed-shell: -1
 *             if the system seems closed-shell, but the final levels do not correspond to the
 *             initial ansatz: -2
 * It must be M >= M_cut.
 * Warning: in determining if a system is closed-shell, the function does not check if levels
 *          identified by different pairs of quantum numbers are degenerate in energy.
 */
int self_consistent_method(const size_t N, const double r_s, const NumerovParams np, const size_t M,
                           const size_t M_cut, const double mix, const double thresh_scf,
                           double *E_tot, double *r, double *V_ks, double *rho, Levels *levels,
                           size_t *num_iter)
{
    size_t i, j, k, num_l, num_n, num_levels = 0;
    int out = 0;
    double conv[2] = { DBL_MAX, DBL_MAX };
    bool closed_shell_system, closed_shell_system_end;
    size_t *n_max = NULL, *degeneracy = NULL;
    double *E = NULL;

    gsl_vector_view _r = gsl_vector_view_array(r, M);
    gsl_vector_view _V_ks = gsl_vector_view_array(V_ks, M);
    gsl_vector_view _rho = gsl_vector_view_array(rho, M_cut);
    gsl_vector_view orbital;
    gsl_matrix_view _orbitals;

    gsl_vector *V_ext = gsl_vector_alloc(M);
    gsl_vector *rho_1 = gsl_vector_alloc(M_cut);
    gsl_matrix *orbitals = NULL;

    *num_iter = 0;

    Levels_initialize(levels, M_cut);

    set_coordinate(&_r.vector, np.r_low, np.h);
    set_external_potential(V_ext, &_r.vector, r_s, N);

    closed_shell_system = _compute_occupied_levels(&out, levels, N, &_r.vector, V_ext, np);

    if (closed_shell_system) {
        n_max = malloc(sizeof(size_t));
        n_max[0] = -1;
        for (i = 0, num_l = 1; i < levels->size; i++) {
            if (levels->l[i] >= num_l) {
                num_l++;
                n_max = realloc(n_max, num_l * sizeof(size_t));
                n_max[num_l - 1] = -1;
            }
            n_max[levels->l[i]]++;
        }

        _orbitals = gsl_matrix_view_array(levels->orbitals, levels->size, levels->M);

        degeneracy = malloc(levels->size * sizeof(size_t));
        for (i = 0; i < levels->size; i++) {
            degeneracy[i] = _degeneracy(levels->l[i]);
        }

        _compute_density(rho_1, &_orbitals.matrix, degeneracy);

        for (i = 0, k = 0; i < num_l; i++) {
            for (j = 0; j <= n_max[i]; j++, k++) {
                degeneracy[k] = _degeneracy(i);
            }
        }

        do {
            if (*num_iter > 0) {
                _mixing(rho_1, &_rho.vector, mix);
            }

            set_ks_potential(&_V_ks.vector, V_ext, &_r.vector, rho_1, np.h);

            for (i = 0, k = 0; i < num_l; i++) {
                num_n = n_max[i] + 1;
                E = realloc(E, num_n * sizeof(double));
                orbitals = gsl_matrix_alloc(num_n, M_cut);

                out += _compute_levels(orbitals, E, 0, n_max[i], &_r.vector, &_V_ks.vector, i, np);

                for (j = 0; j <= n_max[i]; j++, k++) {
                    orbital = gsl_matrix_row(orbitals, j);
                    Levels_set(levels, k, &orbital.vector, E[j], j, i);
                }

                gsl_matrix_free(orbitals);
            }

            _orbitals = gsl_matrix_view_array(levels->orbitals, levels->size, levels->M);
            _compute_density(&_rho.vector, &_orbitals.matrix, degeneracy);

            i = *num_iter % 2;
            conv[i] = _check_convergence(levels, degeneracy, &_r.vector, V_ext, &_rho.vector, np.h);

            (*num_iter)++;

            printf("Iteration: %3ld", *num_iter);
            printf("\t\tConvergence: %10f -> %f\n", conv[i], thresh_scf);

        } while ((fabs(conv[0]) > thresh_scf || fabs(conv[1]) > thresh_scf) &&
                 *num_iter < NUM_ITER_SCF_MAX);

        Levels_free(levels);
        out = 0;

        closed_shell_system_end =
                _compute_occupied_levels(&out, levels, N, &_r.vector, &_V_ks.vector, np);

        free(degeneracy);
        free(E);
    }

    gsl_vector_free(V_ext);
    gsl_vector_free(rho_1);

    if (closed_shell_system && closed_shell_system_end) {
        for (i = 0; i < num_l; i++) {
            for (j = 0; j <= n_max[i]; j++) {
                for (k = 0; k < levels->size; k++) {
                    if (levels->n[k] == j && levels->l[k] == i) {
                        num_levels++;
                        break;
                    }
                }
            }
        }

        free(n_max);

        if (num_levels == levels->size) {
            *E_tot = _compute_energy(levels, &_r.vector, &_rho.vector, np.h);

            if (fabs(conv[0]) > thresh_scf || fabs(conv[1]) > thresh_scf) {
                *num_iter = 0;

                printf("\nConvergence not reached\n");
            }
        } else {
            Levels_free(levels);
            *E_tot = 0;
            *num_iter = 0;
            out = -2;

            printf("\nThe final levels do not correspond to the initial ansatz\n");
        }
    } else {
        if (closed_shell_system) {
            free(n_max);
        }

        Levels_free(levels);
        *E_tot = 0;
        *num_iter = 0;
        out = -1;

        printf("\nNot a closed-shell system\n");
    }

    return out;
}
