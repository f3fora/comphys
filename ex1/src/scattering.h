#ifndef SCATTERING_H
#define SCATTERING_H

#define _USE_MATH_DEFINES

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_const_mksa.h>
#include <gsl/gsl_const_num.h>
#include "../../src/numerov.h"
#include "bessel.h"

// Conversion factors
#define hbar GSL_CONST_MKSA_PLANCKS_CONSTANT_HBAR
#define meV (GSL_CONST_NUM_MILLI * GSL_CONST_MKSA_ELECTRON_VOLT)
#define Angstrom GSL_CONST_MKSA_ANGSTROM
#define amu GSL_CONST_MKSA_UNIFIED_ATOMIC_MASS

// Parameters
#define Kr_mass (83.798 * amu) // kg
#define H_mass (1.008 * amu) // kg
#define sigma (3.18 * Angstrom) // m
#define epsilon (5.9 * meV) // J
#define reduced_mass (Kr_mass * H_mass / (Kr_mass + H_mass)) // kg

// hbar^2 / reduced_mass in dimensionless units
#define B_SC (hbar * hbar / (reduced_mass * sigma * sigma * epsilon))

// Constant used in the calculation of the initial conditions for the Numerov algorithm
#define b_sc (2. / 5. * sqrt(2. / B_SC))

int main_scattering(const double, const size_t, const double, const double, const double,
                    const double, const size_t, double *, double *, double *, double *);

#endif
