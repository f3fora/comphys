\section{One-Dimensional Harmonic Oscillator} \label{sec: 1D_ho}

As a first example of application of the Numerov algorithm, we consider the 1D quantum harmonic oscillator, described by the following Schrödinger equation:
\begin{equation}\label{eq: 1D_ho_se}
    \left(-\frac{1}{2}\dv[2]{x} + \frac{1}{2}x^2\right)\!\psi(x) = E\psi(x)\,,
\end{equation}
where we used dimensionless units. It is known that the equation is satisfied by the wave functions
\begin{equation}\label{eq: theo_wf_1dho}
    \psi_n(x) = c_n H_n(x) e^{-x^2 / 2},\,
\end{equation}
where
\begin{equation}
    H_n(x) = (-1)^n e^{x^2} \dv[n]{x} e^{-x^2}
\end{equation}
are the Hermite polynomials and $c_n$ are normalization constants. The corresponding eigenenergies are
\begin{equation}\label{eq: theo_el_1dho}
    E_n = n + \frac{1}{2} \quad \text{with } n = 0,\,1,\,2\dots
\end{equation}

With reference to the previous section, in this particular case, we have $K^2(x_i) = 2E - x_i^2$. In order to fix the initial conditions for the Numerov algorithm, we can exploit the (even or odd) symmetry of the wave functions with respect to $x = 0$ and their behaviour in the $x \to 0$ limit:
\begin{equation}
    \psi_n(x) \sim 
    \begin{cases}
        1 \quad \text{if $n$ even}\\
        x \quad \text{if $n$ odd}
    \end{cases}
\end{equation}
where sign and normalization are arbitrary. Hence, for each index $n$, as initial conditions we set
\begin{equation}\label{eq: 1d_ho_init_cond}
    \psi_n(0) = 
    \begin{cases}
        1 \quad \text{if $n$ even}\\
        0 \quad \text{if $n$ odd}
    \end{cases}
    ,\quad
    \psi_n(h) = 
    \begin{cases}
        1 \quad \text{if $n$ even}\\
        h \quad \text{if $n$ odd}
    \end{cases}
\end{equation}
and propagate the solution through the Numerov formula \eqref{eq: numerov} for $x \ge 0$, up to $x_M \equiv Mh$.

The wave functions describing the stationary states of the harmonic oscillator are normalizable, thus they tend to zero for $x \to \pm\infty$. Hence, to find numerically the energy levels $E_n$, choosing a sufficiently large $x_M$, we can search for the zeroes of $\psi(E, x_M)$, calculated using the Numerov algorithm. Starting from $E = 0$, we progressively increase $E$ by a small fixed quantity $\delta E$, until we observe the first change of sign in $\psi(E, x_M)$. At this point, we applied the secant method in order to identify with more precision the eigenenergy $E_0$. The secant algorithm is terminated when the new energy found in the iterative process differs from the previous one by less than a chosen threshold $\varepsilon$. Hence, choosing a sufficiently small step $h$, the error on $E_0$ is approximately equal to $\pm \varepsilon / 2$. Proceeding in the same way, we find the entire spectrum up to a chosen value $n = n_{max}$, reaching each level $E_n$ starting the research from the energy $E_{n-1} + \delta E$.

The wave functions and the corresponding eigenenergies numerically found for $n \le 4$ are represented in \autoref{fig:1D_ho_wf} and \autoref{fig:1D_ho_E}. To get both plots, we set $h = 10^{-5}$, $x_{M} = 6$, $\delta E = \num{1.1e-2}$, $\varepsilon = 10^{-4}$.

\inputfigure[.93]{1D_ho_wf}{1D Harmonic Oscillator: Wave Functions}{The first plot shows the normalized wave functions $\psi_n(x)$ describing the first 5 eigenstates of the 1D harmonic oscillator (described by equation \eqref{eq: 1D_ho_se}), computed numerically using the Numerov algorithm \eqref{eq: numerov}. The successive plots, for each index $n$, show the point by point squared difference $\Delta^2\psi_n(x)$ between the numerically computed wave function and the theoretical one. To make the comparison, to some analytical wave functions, given by equation \eqref{eq: theo_wf_1dho}, a global change of sign has been applied, having arbitrarily chosen a positive sign for $x \to 0$ in the numerical calculations through the Numerov method (see initial conditions \eqref{eq: 1d_ho_init_cond}).}

\autoref{fig:1D_ho_wf} shows a good agreement between the wave functions computed numerically and the analytical solutions \eqref{eq: theo_wf_1dho}. However, we observe a progressive divergence of the numerical wave functions from the analytical ones for $x$ approaching $x_M$. This can be due to the imposition of $\psi_n(x_M) = 0$ in the search for the energy levels. However, this does not represent a problem, since the wave functions for the considered values of $n$ are all approximately equal to zero for $x \gtrsim 5$. Moreover, taking care to choose a sufficiently small step $h$, $x_M$ can be taken arbitrarily large if necessary.

\inputfigure[.91]{1D_ho_E}{1D Harmonic Oscillator: Energy Spectrum}{The figure illustrates the energy spectrum of the 1D harmonic oscillator (described by equation \eqref{eq: 1D_ho_se}) computed numerically. The plot shows the eigenenergies $E_n$ and the corresponding wave functions $\psi_n(x)$ for $n \le 4$, and the harmonic potential $V(x) = x^2 / 2$. The energy levels are compatible with those predicted by formula \eqref{eq: theo_el_1dho}.}

\autoref{fig:1D_ho_E} shows how the eigenenergies calculated numerically are compatible, within their uncertainty, with those expected, described by formula \eqref{eq: theo_el_1dho}.

Using the developed algorithms, we investigate in particular the dependence of our numerical solutions on the number $M$ of points in the mesh, i.e. on the step $h \propto 1 / M$. The results are represented in \autoref{fig:1D_ho_DE} and \autoref{fig:1D_ho_MSE}.

\inputfigure[.89]{1D_ho_DE}{1D Harmonic Oscillator: Eigenenergies vs Step}{The figure shows the dependence of the difference $\Delta E$ between the numerically computed energy levels and the analytical ones on the step $h$ (and the number $M$ of points in the mesh) in the Numerov algorithm \eqref{eq: numerov}. The plot is obtained by setting $x_{M} = 6$, $\delta E = \num{1.1e-2}$, $\varepsilon = 10^{-4}$. For each index $n$, the discrepancy between numerically calculated and theoretical eigenenergies grows with the increase of the step.}

\inputfigure[.89]{1D_ho_MSE}{1D Harmonic Oscillator: Wave Functions vs Step}{The figure shows the dependence of the mean squared error (MSE), which affects the numerically computed wave functions with respect to the analytical ones, on the step $h$ (and the number $M$ of points in the mesh) in the Numerov algorithm \eqref{eq: numerov}. The plot is obtained by setting $x_{M} = 6$, $\delta E = \num{1.1e-2}$, $\varepsilon = 10^{-4}$. The behaviour is significantly different for $n$ even and $n$ odd.}

\autoref{fig:1D_ho_DE} shows how the numerical approximation of the energy levels $E_n$ improves with the reduction of the step $h$. In particular, for $h \le 10^{-3}$, the discrepancy between numerically calculated and theoretical eigenenergies is $\abs{\Delta E} < 10^{-3}$ for each $n$ considered. Moreover, the plot shows a significantly smaller discrepancy for $n$ odd than for $n$ even. 

\autoref{fig:1D_ho_MSE} further underlines the difference in the dependence on the step $h$ of even and odd numerical solutions. For $n$ even, the MSE on the numerical wave functions grows with the increase of $h$, while, for $n$ odd, the MSE exhibits a more irregular behaviour, always remaining less than that for $n$ even. The different behaviour for even and odd solutions can be due to the different initial conditions considered in the two cases (see equation \eqref{eq: 1d_ho_init_cond}).