# ComputationalPhysics

University of Trento - Department of Physics  
Computational Physics [145649] - 2021  
_Professor:_ Prof. FRANCESCO PEDERIVA  
_Authors:_ MATTEO CESCATO, FILIPPO FERRARI, ALESSANDRO FORADORI  

## About the Project

This project includes the solutions to the exercises assigned during the MSc _Computational Physics_ course, held in 2021 by Prof. Francesco Pederiva at the University of Trento.


Note that:
- the C, Python, Bash, Jupyter code is distributed under the license [GNU GPLv3](LICENSE);
- the images, the LaTeX code and the related PDFs are subject to the license [CC BY-NC-SA](https://creativecommons.org/licenses/by-nc-sa/4.0/);
- the README.pdf files introducing the exercises belong to Prof. Francesco Pederiva;
- the logo of the University of Trento belongs to the University of Trento.


_It is emphasized that any permitted use of this work must be done taking care to properly cite this same project and the authors._

## Exercises

1. [Scattering of H Atoms on a Kr Atom](ex1/README.md)
1. [Hartree-Fock Method](ex2/README.md)
1. [Alkali Metal Clusters in Density Functional Theory](ex3/README.md)
1. [Variational Monte Carlo for Superfluid <sup>4</sup>He](ex4/README.md)

## Dependencies

- **Python**: create a [conda](https://www.anaconda.com/) environment with all requirements with [`conda env create -f conda_env.yml`](conda_env.yml), including but not limited to:
	- [Jupyter](https://jupyter.org/)
	- [Matplotlib](https://matplotlib.org/)
	- [NumPy](https://numpy.org/)
	- [SciPy](https://www.scipy.org/)

+ **C**:
	+ [GSL](https://www.gnu.org/software/gsl/doc/html/index.html#)
	+ [Open MPI](https://www.open-mpi.org/) or equivalent implementation of MPI

## Code Style

- **Python**: [PEP8 Style](https://www.python.org/dev/peps/pep-0008/)

- **C**: Custom style described in [`.clang-format`](.clang-format).
It is essentially the [Kernel Style](https://www.kernel.org/doc/html/v4.10/process/coding-style.html), but with a reduced tab width and an increased line length. 

## Project Layout

```
comphys
+-- README.md
+-- LICENSE
+-- conda_env.yml
+-- .clang-format
+-- .gitignore
+-- .gitlab-ci.yml
+-- .utils
|   +-- script_for_ci.whatever
+-- src
|   +-- useful_functions.c
|   +-- useful_functions.h
+-- lib
|   +-- useful_functions.py
+-- ex?
|   +-- README.md
|   +-- README.pdf // assignment
|   +-- main.ipynb // notebook for data analysis
|   +-- src
|   |   +-- functions.c
|   |   +-- functions.h
|   +-- lib
|   |   +-- mainCfunction.out // not traced
|   |   +-- libCfunctions.so // not traced
|   |   +-- binding_of_C_functions.py
|   |   +-- python_functions.py
|   +-- data
|   |   +-- some_data.whatever
|   +-- img
|   |   +-- some_img.svg/pdf/png
|   +-- tex // report 
|   |   +-- files.tex
```

