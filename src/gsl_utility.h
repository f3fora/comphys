#ifndef GSL_UTILITY_H
#define GSL_UTILITY_H

#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <gsl/gsl_vector.h>

double last_value(const gsl_vector *);
int sum_vectors(gsl_vector *, const gsl_vector *, const gsl_vector *);
int multiply_vectors(gsl_vector *, const gsl_vector *, const gsl_vector *);
int pow_vector(gsl_vector *, const gsl_vector *, const double);
int set_value(gsl_vector *, const size_t, const size_t, const double);
double integral(const gsl_vector *, const double);
int second_derivative(gsl_vector *, const gsl_vector *, const double);
bool is_there_a_nan(gsl_vector *);

#endif
