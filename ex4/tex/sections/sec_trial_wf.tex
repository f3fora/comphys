\subsection{Trial Wave Function} \label{sec:trial_wf}

The choice of the trial wave function is extremely important in a VMC calculation. In order to effectively describe the correlations among particles, a common choice is a wave function of the form
\begin{equation}\label{eq:general_wf}
    \psi_T(R) = \phi_{MF}(R) \,\exp\!\Bigg(-\frac{1}{2} \sum_{i<j} U^{(2)}(r_{ij})\Bigg)\,.
\end{equation}
In our case, the mean-field factor $\phi_{MF}(R)$ is a constant, since the \ce{^4He} atoms are bosons and we assume a constant and uniform density. The second factor, called Jastrow factor, depends on the Jastrow correlation pseudopotential $U^{(2)}(r)$, which has to satisfy the so-called cusp condition:
\begin{equation}\label{eq:general_cusp_condition}
    \frac{\mathcal{H}\psi_{T}(R)}{\psi_{T}(R)} < \infty \quad \forall\,R,
\end{equation}
which, in our case, becomes
\begin{equation}\label{eq:cusp_condition}
    \lim_{r_{ij} \to 0}\frac{\mathcal{H}_{ij}\exp\!\left(-\frac{1}{2}\sum_{i<j}U^{(2)}(r_{ij})\right)}{\exp\!\left(-\frac{1}{2}\sum_{i<j}U^{(2)}(r_{ij})\right)} < +\infty\,,
\end{equation}
where $\mathcal{H}_{ij}$ is the Hamiltonian of two particles only. This condition is satisfied if
\begin{equation}\label{eq:Jastrow_pseudopotential}
    U^{(2)}(r) = \left(\frac{b}{r}\right)^5 ,
\end{equation}
which implies the so-called Jastrow-McMillan trial wave function:
\begin{equation}\label{eq:trial_wf}
    \psi_T(R) = \exp\!\Bigg(-\frac{1}{2}\sum_{i<j}\left(\frac{b}{r_{ij}}\right)^5\Bigg)\,,
\end{equation}
where $b$ is a variational parameter and the normalization is arbitrary.

With this trial wave function, the local energy \eqref{eq:local_energy} becomes
\begin{align}\label{eq:local_energy_Jastrow}
    \nonumber
    E_L(R) &= \frac{\hbar^2}{4m} \sum_{i=1}^N \left[\sum_{j \,(\neq i)} \Bigg(\derivative[2]{U^{(2)}(r_{ij})}{r_{ij}} + \frac{2}{r_{ij}} \derivative{U^{(2)}(r_{ij})}{r_{ij}}\Bigg) - \frac{1}{2} \Bigg(\sum_{j \,(\neq i)} \derivative{U^{(2)}(r_{ij})}{r_{ij}} \frac{\mathbf{r}_{ij}}{r_{ij}}\Bigg)^2\right] + \\
    \nonumber
    &\quad\, + \sum_{i<j} V(r_{ij}) \\
    &= \frac{\hbar^2}{2m} b^5 \left[\sum_{i<j} \frac{20}{r_{ij}^7} - \frac{25}{2} b^5 \Bigg(\sum_{i<j} \frac{1}{r_{ij}^{12}} + \sum_{i}\sum_{(j<k) \neq i} \frac{\mathbf{r}_{ij} \cdot \mathbf{r}_{ik}}{r_{ij}^7 r_{ik}^7}\Bigg)\right] + \sum_{i<j} V(r_{ij})\,,
\end{align}
while the JF local energy \eqref{eq:JF_local_energy} reads
\begin{equation}\label{eq:JF_local_energy_Jastrow}
    \begin{aligned}
        E^{JF}_L(R) &= \frac{\hbar^2}{8m} \sum_{i \neq j}\left(\derivative[2]{U^{(2)}(r_{ij})}{r_{ij}} + \frac{2}{r_{ij}} \derivative{U^{(2)}(r_{ij})}{r_{ij}}\right) + \sum_{i<j} V(r_{ij}) \\
        &= \frac{\hbar^2}{2m} 10 b^5 \sum_{i<j} \frac{1}{r_{ij}^7} + \sum_{i<j} V(r_{ij})\,,
    \end{aligned}
\end{equation}
where $\mathbf{r}_{ij} \equiv \mathbf{r}_i - \mathbf{r}_j$; the sums over $j \,(\neq i)$ are performed over the index $j$, with $j \neq i$; the sum over $i \neq j$ is over both indices $i$ and $j$, with $i \neq j$; the sum over $(j<k) \neq i$ is over the indices $j$ and $k$ such that $j<k$, $j \neq i$ and $k \neq i$.