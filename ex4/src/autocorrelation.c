#include "autocorrelation.h"

/*
 * Initialize an array to zero.
 * C: pointer to the array [output]
 * size: size of the array
 */
int initialize_autocorr(double *C, const size_t size)
{
    size_t i;

    for (i = 0; i < size; i++) {
        C[i] = 0.;
    }

    return 0;
}

/*
 * Update an array used for the computation of an autocorrelation coefficient.
 * C: pointer to the double M-array to update [output]
 * E: pointer to a circular array of size M containing the last M samples of the quantity to which
 *    the autocorrelation coefficient refers
 */
int update_autocorr(double *C, const Circ_array *E)
{
    size_t i, j;

    for (i = 0, j = E->size - 1; i < E->size; i++, j--) {
        C[i] += Circ_array_last_item(E) * Circ_array_get(E, j);
    }

    return 0;
}

/*
 * Normalize an autocorrelation coefficient.
 * C: pointer to the array containing the non-normalized autocorrelation coefficient [output]
 * size: size of the array pointed by C
 * E_mean: mean value of the observable to which the autocorrelation coefficient refers
 * M: number of Metropolis steps considered in the computation of the autocorrelation coefficient
 */
int normalize_autocorr(double *C, const size_t size, const double E_mean, const size_t M)
{
    size_t i;
    double E_mean2 = E_mean * E_mean;

    C[0] = C[0] / (double)M - E_mean2;

    for (i = 1; i < size; i++) {
        C[i] = (C[i] / (double)M - E_mean2) / C[0];
    }

    C[0] = 1.;

    return 0;
}

/*
 * Compute the autocorrelation length from the autocorrelation coefficient.
 * tau: pointer to a double variable, where the autocorrelation length will be stored [output]
 * tau_std: pointer to a double variable, where the error on the autocorrelation length will be
 *          stored [output]
 * C: pointer to the array containing the normalized autocorrelation coefficient
 * size: size of the array pointed by C
 */
int compute_tau(double *tau, double *tau_std, const double *C, const size_t size)
{
    size_t i;
    double x[size], w[size], y[size], result, var, sumsq;

    for (i = 0; i < size; i++) {
        x[i] = i;
        w[i] = C[i] * C[i];
        y[i] = log(C[i]);
    }

    gsl_fit_wmul(x, 1, w, 1, y, 1, size, &result, &var, &sumsq);

    *tau = -1. / result;
    *tau_std = *tau * *tau * sqrt(var);

    return 0;
}

/*
 * Save an autocorrelation coefficient to a text file.
 * C: pointer to the array containing the autocorrelation coefficient to save
 * size: size of the array pointed by C
 * file_name: string containing the path of the file to write to
 */
int save_autocorr(const double *C, const size_t size, const char *file_name)
{
    size_t i;

    FILE *file = fopen(file_name, "w");

    for (i = 0; i < size; i++) {
        fprintf(file, "%f\n", C[i]);
    }

    return fclose(file);
}
