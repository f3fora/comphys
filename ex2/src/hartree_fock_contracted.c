#include "hartree_fock_contracted.h"

/*
 * Function used by _hamilton_contr and _overlap_matrix_contr to compute the one-body part of the
 * Hamiltonian and the overlap matrix in the case of contracted orbitals.
 * fin: M1xM1 gsl_matrix in which the resulting matrix will be stored [output]
 * ini: MxM gsl_matrix to contract
 * beta: pointer to a double M-array containing the coefficients which define the M1 contractions of
 *       Gaussians that form the considered basis; each contraction is constructed with M / M1
 *       Gaussians
 */
void _contract(gsl_matrix *fin, const gsl_matrix *ini, const double *beta)
{
    size_t i, j, k, l, idx1, idx2;
    size_t M2 = ini->size1 / fin->size1;
    double acc;

    for (i = 0; i < fin->size1; i++) {
        for (j = 0; j < i; j++) {
            acc = 0.;
            for (k = 0; k < M2; k++) {
                for (l = 0; l < M2; l++) {
                    idx1 = k + i * M2;
                    idx2 = l + j * M2;
                    acc += beta[idx1] * beta[idx2] * gsl_matrix_get(ini, idx1, idx2);
                }
            }
            gsl_matrix_set(fin, i, j, acc);
            gsl_matrix_set(fin, j, i, acc);
        }
        acc = 0.;
        for (k = 0; k < M2; k++) {
            for (l = 0; l < k; l++) {
                idx1 = k + i * M2;
                idx2 = l + i * M2;
                acc += 2. * beta[idx1] * beta[idx2] * gsl_matrix_get(ini, idx1, idx2);
            }
            idx1 = k + i * M2;
            acc += beta[idx1] * beta[idx1] * gsl_matrix_get(ini, idx1, idx1);
        }
        gsl_matrix_set(fin, i, i, acc);
    }
}

/*
 * Compute the one-body part of the Hamiltonian matrix in the case of contracted orbitals.
 * H: M1xM1 gsl_matrix in which the one-body part of the Hamiltonian will be stored [output]
 * alpha: pointer to a double M-array containing the coefficients which define the M Gaussians whose
 *        contractions form the basis with respect to which the Hamiltonian is written
 * beta: pointer to a double M-array containing the coefficients which define M1 contractions of
 *       M2 = M / M1 Gaussians
 * M2: number of Gaussians with which each contraction is constructed
 * Z: nuclear charge
 */
void _hamilton_contr(gsl_matrix *H, const double *alpha, const double *beta, const size_t M2,
                     const size_t Z)
{
    gsl_matrix *h = gsl_matrix_alloc(M2 * H->size1, M2 * H->size1);

    hamilton(h, alpha, Z);
    _contract(H, h, beta);

    gsl_matrix_free(h);
}

/*
 * Compute the overlap matrix in the case of contracted orbitals.
 * S: M1xM1 gsl_matrix in which the overlap matrix will be stored [output]
 * alpha: pointer to a double M-array containing the coefficients which define the M Gaussians whose
 *        contractions form the basis with respect to which the Hamiltonian is written
 * beta: pointer to a double M-array containing the coefficients which define M1 contractions of
 *       M2 = M / M1 Gaussians
 * M2: number of Gaussians with which each contraction is constructed
 */
void _overlap_matrix_contr(gsl_matrix *S, const double *alpha, const double *beta, const size_t M2)
{
    gsl_matrix *s = gsl_matrix_alloc(M2 * S->size1, M2 * S->size1);

    overlap_matrix(s, alpha);
    _contract(S, s, beta);

    gsl_matrix_free(s);
}

/*
 * Compute the combinations of electron repulsion integrals (ERIs) which enter into the calculation
 * of the Fock matrix in the case of contracted orbitals. The computation exploits the symmetries of
 * the ERIs and only the terms required in the Fock matrix calculation are calculated.
 * eris_contr: pointer to a double (M1*M1*M1*M1)-array, where the results will be stored [output]
 * alpha: pointer to a double M-array containing the coefficients which define the M Gaussians whose
 *        contractions form the considered basis
 * beta: pointer to a double M-array containing the coefficients which define M1 contractions of
 *       M2 = M / M1 Gaussians
 * M1: number of contractions (i.e. number of basis functions)
 * M2: number of Gaussians with which each contraction is constructed
 */
void _compute_eris_contr(double *eris_contr, const double *alpha, const double *beta,
                         const size_t M1, const size_t M2)
{
    size_t i1, j1, n1, m1, i2, j2, n2, m2, i, j, n, m;
    size_t M = M1 * M2;
    double acc, temp;
    double eris[(size_t)pow(M, 4)];

    compute_eris(eris, alpha, M);

    for (i1 = 0; i1 < M1; i1++) {
        for (j1 = 0; j1 < i1; j1++) {
            for (n1 = 0; n1 < M1; n1++) {
                for (m1 = 0; m1 < M1; m1++) {
                    acc = 0.;
                    for (i2 = 0; i2 < M2; i2++) {
                        for (j2 = 0; j2 < M2; j2++) {
                            for (n2 = 0; n2 < M2; n2++) {
                                for (m2 = 0; m2 < M2; m2++) {
                                    i = i2 + i1 * M2;
                                    j = j2 + j1 * M2;
                                    n = n2 + n1 * M2;
                                    m = m2 + m1 * M2;
                                    acc += eris[IDX(i, j, n, m, M)] * beta[i] * beta[j] * beta[n] *
                                           beta[m];
                                }
                            }
                        }
                    }
                    eris_contr[IDX(i1, j1, n1, m1, M1)] = acc;
                }
            }
        }
        for (n1 = 0; n1 < M1; n1++) {
            for (m1 = 0; m1 <= n1; m1++) {
                acc = 0.;
                for (i2 = 0; i2 < M2; i2++) {
                    for (j2 = 0; j2 < i2; j2++) {
                        for (n2 = 0; n2 < M2; n2++) {
                            for (m2 = 0; m2 < M2; m2++) {
                                i = i2 + i1 * M2;
                                j = j2 + i1 * M2;
                                n = n2 + n1 * M2;
                                m = m2 + m1 * M2;
                                acc += 2. * eris[IDX(i, j, n, m, M)] * beta[i] * beta[j] * beta[n] *
                                       beta[m];
                            }
                        }
                    }
                    for (n2 = 0; n2 < M2; n2++) {
                        for (m2 = 0; m2 <= n2; m2++) {
                            i = i2 + i1 * M2;
                            n = n2 + n1 * M2;
                            m = m2 + m1 * M2;
                            temp = eris[IDX(i, i, n, m, M)] * pow(beta[i], 2) * beta[n] * beta[m];
                            if (n2 != m2) {
                                temp *= 2.;
                            }
                            acc += temp;
                        }
                    }
                }
                eris_contr[IDX(i1, i1, n1, m1, M1)] = acc;
            }
        }
    }
}

/*
 * Scale the coefficients of a linear combination of contractions of Gaussians, so that the
 * single-particle wave function it defines is normalized.
 * C: M1-gsl_vector containing the coefficients (of the linear combination) to scale [output]
 * alpha: pointer to a double M-array containing the coefficients which define the Gaussians
 * beta: pointer to a double M-array containing the coefficients which define M1 contractions of
 *       M2 = M / M1 Gaussians
 * M2: number of Gaussians with which each contraction is constructed
 */
void _normalize_contr(gsl_vector *C, const double *alpha, const double *beta, const size_t M2)
{
    size_t i, j, idx;
    double norm;
    gsl_vector *c = gsl_vector_alloc(M2 * C->size);

    for (i = 0; i < C->size; i++) {
        for (j = 0; j < M2; j++) {
            idx = j + i * M2;
            gsl_vector_set(c, idx, beta[idx] * gsl_vector_get(C, i));
        }
    }

    norm = normalize(c, alpha);
    gsl_vector_scale(C, 1. / norm);

    gsl_vector_free(c);
}

/*
 * Compute the ground state energy and the coefficients defining the N/2 occupied orbitals of a
 * neutral atom with an even number N > 1 of electrons, solving the Hartree-Fock-Roothaan equations,
 * written on a basis of M1 contractions of M2 Gaussians, through a self-consistent field
 * calculation.
 * energy: pointer to a double variable, where the estimated ground state energy will be stored
 *         [output]
 * C: M1x(N/2) gsl_matrix in whose columns the coefficients defining the orbitals will be stored
 *    [output]
 * alpha: pointer to a double M-array containing the coefficients which define the M = M1 * M2
 *        Gaussians whose contractions form the considered basis
 * beta: pointer to a double M-array containing the coefficients which define the M1 contractions of
 *       Gaussians that form the considered basis; each contraction is constructed with M2 = M / M1
 *       Gaussians
 * M1: number of contractions (i.e. number of basis functions)
 * M2: number of Gaussians with which each contraction is constructed
 * N: number of electrons (it must be N > 1 even)
 * density_0: M1xM1 gsl_matrix containing the initial guess on the density matrix
 * mix: mixing parameter
 * thresh: threshold for evaluating the convergence of the self-consistent field calculation;
 *         the algorithm ends when the results of the functions compute_energy_eig and
 *         compute_energy_exp (of hartree_fock.h) differ by less than thresh
 * return: if the algorithm converges within NUM_ITER_MAX (defined in hartree_fock.h) iterations:
 *             number of iterations required for the convergence
 *         else:
 *             0
 */
size_t self_consistent_method_contr(double *energy, gsl_matrix *C, const double *alpha,
                                    const double *beta, const size_t M1, const size_t M2,
                                    const size_t N, const gsl_matrix *density_0, const double mix,
                                    const double thresh)
{
    size_t i, N2 = N / 2;
    size_t num_iter = 0;
    double E[N2];
    double eris_contr[(size_t)pow(M1, 4)];
    double fock_energy, energy_exp;

    gsl_matrix *S = gsl_matrix_alloc(M1, M1);
    gsl_matrix *h = gsl_matrix_alloc(M1, M1);
    gsl_matrix *H = gsl_matrix_alloc(M1, M1);
    gsl_matrix *F = gsl_matrix_alloc(M1, M1);
    gsl_matrix *density_1 = gsl_matrix_alloc(M1, M1);
    gsl_matrix *density_2 = gsl_matrix_alloc(M1, M1);
    gsl_matrix *evec = gsl_matrix_alloc(M1, M1);
    gsl_vector *eval = gsl_vector_alloc(M1);
    gsl_vector_view vec_view;

    gsl_matrix_memcpy(density_1, density_0);

    _overlap_matrix_contr(S, alpha, beta, M2);
    _hamilton_contr(h, alpha, beta, M2, N);
    _compute_eris_contr(eris_contr, alpha, beta, M1, M2);

    do {
        if (num_iter > 0) {
            mixing(density_1, density_2, mix);
        }

        fock(F, eris_contr, density_1);
        sum_sym_matrices(H, h, F);
        find_eigenstates(eval, evec, H, S);
        find_lower_states(E, C, eval, evec, N2);

        for (i = 0; i < N2; i++) {
            vec_view = gsl_matrix_column(C, i);
            _normalize_contr(&vec_view.vector, alpha, beta, M2);
        }

        compute_density(density_2, C);

        fock_energy = compute_fock_energy(eris_contr, density_2);
        *energy = compute_energy_eig(E, fock_energy, N);
        energy_exp = compute_energy_exp(h, fock_energy, density_2);

        num_iter++;
    } while (fabs(*energy - energy_exp) > thresh && num_iter <= NUM_ITER_MAX);

    if (fabs(*energy - energy_exp) > thresh) {
        num_iter = 0;
    }

    gsl_matrix_free(S);
    gsl_matrix_free(h);
    gsl_matrix_free(H);
    gsl_matrix_free(F);
    gsl_matrix_free(density_1);
    gsl_matrix_free(density_2);
    gsl_matrix_free(evec);
    gsl_vector_free(eval);

    return num_iter;
}
