\section{Hartree-Fock Method} \label{sec:hartree_fock}

The Hartree-Fock method is a mean-field method, largely used in Atomic Physics and Quantum Chemistry for the calculation of the properties of atoms and molecules.

In this exercise, we apply this procedure to the computation of the ground state properties of some simple neutral atoms with an even number $N = Z$ of electrons. Considering the nucleus fixed in the origin of the coordinates and considering only the Coulomb interactions, an atom is described by the following Hamiltonian:
\begin{equation}\label{eq:hamiltonian}
    \mathcal{H} = \sum_{i=1}^N\left(-\frac{1}{2}\nabla^2_{i} - \frac{Z}{r_i}\right) + \sum_{i < j} \frac{1}{|\mathbf{r}_i - \mathbf{r}_j|}\,,
\end{equation}
where $\mathbf{r}_1,\,\mathbf{r}_2,\,\dots,\,\mathbf{r}_N$ denote the coordinates of the electrons, and we used the so-called Hartree atomic units, defined by setting $\hbar = e = a_0 = m_e = 1$. We will use these units in the rest of this paper.

We consider half electrons with spin up and the other half with spin down. In this case, the mean-field approximation prescribes an overall wave function of the form
\begin{equation}\label{eq:many_body_wf}
    \Psi\!\left(\textbf{r}_1,\,\textbf{r}_2,\,\dots,\,\textbf{r}_N\right) = \psi_{\uparrow}\!\left(\textbf{r}_1,\,\dots,\,\mathbf{r}_{N/2}\right)\psi_{\downarrow}\!\left(\textbf{r}_{N/2+1},\,\dots,\,\mathbf{r}_{N}\right),
\end{equation}
where $\psi_{\uparrow}$ and $\psi_{\downarrow}$ are Slater determinants of orthonormal single-particle wave functions $\phi_k(\textbf{r})$ (called orbitals):
\begin{equation}
    \psi_{\uparrow}\!\left(\mathbf{r}_1,\,\mathbf{r}_2,\,\dots,\,\mathbf{r}_{N/2}\right) = \frac{1}{\sqrt{(N/2)!}}\det\!
    \begin{pmatrix}
        \phi_1\!\left(\mathbf{r}_1\right) & \dots & \phi_{N/2}\!\left(\mathbf{r}_1\right) \\
        \vdots & \ddots & \vdots \\
        \phi_1\!\left(\mathbf{r}_{N/2}\right) & \dots & \phi_{N/2}\!\left(\mathbf{r}_{N/2}\right) \\
    \end{pmatrix}
\end{equation}
and similarly for $\psi_{\downarrow}$, with the same orbitals, but different electrons. In this way, the many-body wave functions $\psi_{\sigma}$ are antisymmetric by particle exchange, as required by the fermionic nature of electrons.

By minimizing, with respect to the orbitals $\phi_k$, the expectation value of the Hamiltonian \eqref{eq:hamiltonian} on the wave function \eqref{eq:many_body_wf}, we obtain the Hartree-Fock (HF) equations\footnote{For a complete derivation, see for example \cite{Kim}.}:
\begin{equation}\label{eq:HF}
    \begin{aligned}
        &\left(-\frac{1}{2}\nabla^2 - \frac{Z}{r}\right)\phi_k(\mathbf{r}) + \sum_{\ell=1}^{N/2}\bigg[2\left(\int\phi^{*}_{\ell}(\mathbf{r}')\frac{1}{|\mathbf{r}-\mathbf{r}'|}\phi_{\ell}(\mathbf{r}')\,\text{d}\mathbf{r}'\right)\phi_k(\mathbf{r})\,+ \\
        &-\left(\int\phi^{*}_{\ell}(\mathbf{r}')\frac{1}{|\mathbf{r}-\mathbf{r}'|}\phi_k(\mathbf{r}')\,\text{d}\mathbf{r}'\right)\phi_{\ell}(\mathbf{r})\bigg] = \varepsilon_k\phi_k(\mathbf{r})\,,
    \end{aligned}
\end{equation}
where the two terms inside the square brackets, respectively called direct term and exchange term, constitute the mean-field approximation of the electron-electron interactions.

It is convenient to transform the HF equations into a generalized eigenvalue problem. Expanding the orbitals on a finite set of functions $\left\{\chi_p(\mathbf{r})\right\}$ in such a way that
\begin{equation}
    \phi_k(\mathbf{r}) = \sum_{p}c_{kp}\chi_p(\mathbf{r})\,,
\end{equation}
we obtain the Hartree-Fock-Roothaan (HFR) equations:
\begin{equation}\label{eq:HFR}
    \begin{aligned}
        &\sum_{p}\bigg\{h_{qp} + \sum_{n,m} \rho_{nm}\bigg[2\left(qn\left|\frac{1}{|\mathbf{r}-\mathbf{r}'|}\right|pm\right) +\\
        &-\left(qn\left|\frac{1}{|\mathbf{r}-\mathbf{r}'|}\right|mp\right) \bigg]\bigg\} c_{kp} = \varepsilon_k\sum_{p}S_{qp}c_{kp}\,,
    \end{aligned}
\end{equation}
where 
\begin{equation}\label{eq:overlap_matrix}
    S_{qp} \equiv \int \chi^{*}_q(\mathbf{r}) \chi_p(\mathbf{r})\,\text{d}\textbf{r}
\end{equation}
is the overlap matrix,
\begin{equation}\label{eq:one_body_matrix}
    h_{qp} \equiv \int \chi^{*}_{q}(\mathbf{r})\left(-\frac{1}{2}\nabla^2 - \frac{Z}{r}\right)\chi_{p}(\mathbf{r})\,\text{d}\mathbf{r}
\end{equation}
is the one-body Hamiltonian matrix, 
\begin{equation}\label{eq:density_matrix}
    \rho_{nm} \equiv \sum_{\ell = 1}^{N/2} c^{*}_{\ell n}c_{\ell m}
\end{equation}
is the density matrix, while
\begin{equation}\label{eq:ERI}
    \left(qn\left|\frac{1}{|\mathbf{r}-\mathbf{r}'|}\right|pm\right) \equiv \iint\chi^{*}_q(\mathbf{r})\chi^{*}_n(\mathbf{r}')\frac{1}{|\mathbf{r}-\mathbf{r}'|}\chi_p(\mathbf{r})\chi_m(\mathbf{r}')\,\text{d}\mathbf{r}\text{d}\mathbf{r}'
\end{equation}
and similarly for the remaining term.

The unknowns of the generalized eigenvalue problem are the $N / 2$ smallest eigenvalues $\varepsilon_k$, with $k = 1,\,2,\,\dots,\,N/2$, and the corresponding coefficients $c_{kp}$. Since the solutions appear in the density matrix, the resolution requires a self-consistent field (SCF) procedure, as described in \autoref{fig:flowchart}.

As an initial guess on the density matrix, we set $\rho_{nm} = 0\;\forall\,n,\,m$. At each iteration, the mixing between the new density matrix $\rho_{nm}^{new}$ and the old one $\rho_{nm}^{old}$ is defined by the following weighted average:
\begin{equation}\label{eq:mixing}
    \rho_{nm} = \mu \rho_{nm}^{new} + (1 - \mu) \rho_{nm}^{old}\quad\forall \,n,\,m,
\end{equation}
where $0 < \mu < 1$ is called mixing parameter. This procedure is typically necessary to avoid the non-convergence of the algorithm.

For the convergence criterion, we exploit the existence of two different expressions for the ground state energy of the system. The first one is the expectation value of the mean-field Hamiltonian on the wave function that solves the HFR equations:
\begin{equation}\label{eq:energy_exp}
    \begin{aligned}
        E_0^{exp} =\,& 2\sum_{n,m}\rho_{nm} h_{nm} + \sum_{q,p,n,m} \rho_{qp}\rho_{nm}\bigg[2 \left(qn\left|\frac{1}{|\mathbf{r}-\mathbf{r}'|}\right|pm\right) + \\
        &-\left(qn\left|\frac{1}{|\mathbf{r}-\mathbf{r}'|}\right|mp\right) \bigg]\,.
    \end{aligned}
\end{equation}
The second one considers the eigenvalues $\varepsilon_k$ resulting from the HFR equations:
\begin{equation}\label{eq:energy_eig}
    \begin{aligned}
        E_0^{eig} =\,& 2\sum_{k=1}^{N/2}\varepsilon_k - \sum_{q, p, n, m} \rho_{qp}\rho_{nm}\bigg[2\left(qn\left|\frac{1}{|\mathbf{r}-\mathbf{r}'|}\right|pm\right) + \\
        &-\left(qn\left|\frac{1}{|\mathbf{r}-\mathbf{r}'|}\right|mp\right)\bigg]\,.
    \end{aligned}
\end{equation}
If the SCF procedure has reached convergence, then the two expressions must be equal. Hence, the SCF loop is terminated when the difference between $E_0^{exp}$ and $E_0^{eig}$ is smaller in modulus than a fixed threshold $\epsilon$.

\inputflowchart{flowchart}{Self-Consistent Field Method}{The algorithm starts from an initial guess on the density matrix \eqref{eq:density_matrix}. Iteratively, the HFR equations \eqref{eq:HFR} are solved, obtaining the eigenvalues $\varepsilon_k$ and the coefficients $c_{kp}$. The latter are used to compute a new density matrix. In the next iteration, the density matrix is a weighted average between the last calculated density matrix and that of the previous iteration. The loop ends when an established convergence criterion is satisfied.}

% Subsections
\vspace{1mm}
\input{sections/sec_hydrogen.tex}
\vspace{1mm}
\input{sections/sec_helium.tex}
\vspace{1mm}
\input{sections/sec_beryllium.tex}