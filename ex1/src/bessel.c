#include "bessel.h"

// Function used by bessel_j, bessel_j_all, bessel_n, bessel_n_all.
double _bessel(const double x, const size_t l, double s_iminus1, double s_i)
{
    double s_iplus1;
    size_t i;

    for (i = 0; i < l; i++) {
        s_iplus1 = (2. * i + 1.) / x * s_i - s_iminus1;
        s_iminus1 = s_i;
        s_i = s_iplus1;
    }

    return s_i;
}

// Function used by bessel_j_l, bessel_n_l.
void _bessel_l(gsl_vector *y, const double x, const size_t l_max, double s_iminus1, double s_i)
{
    size_t i;

    gsl_vector_set(y, 0, s_i);

    for (i = 1; i <= l_max; i++) {
        gsl_vector_set(y, i, (2. * (i - 1) + 1.) / x * s_i - s_iminus1);
        s_iminus1 = s_i;
        s_i = gsl_vector_get(y, i);
    }
}

/*
 * Compute the spherical Bessel function j_l at point x.
 * x: point where to evaluate j_l
 * l: index of the spherical Bessel function (orbital angular momentum quantum number)
 * return: j_l evaluated in x
 */
double bessel_j(const double x, const size_t l)
{
    if (x != 0.) {
        return _bessel(x, l, cos(x) / x, sin(x) / x);
    } else if (l == 0) {
        return 1.;
    } else {
        return 0.;
    }
}

/*
 * Compute the spherical Bessel functions j_l, with 0 <= l <= l_max, at point x.
 * y: gsl_vector where to store the evaluation of the functions [output]
 * x: point where to evaluate the functions
 * l_max: maximum index of the spherical Bessel functions to evaluate
 */
int bessel_j_l(gsl_vector *y, const double x, const size_t l_max)
{
    size_t i;

    if (x != 0.) {
        _bessel_l(y, x, l_max, cos(x) / x, sin(x) / x);
    } else {
        gsl_vector_set(y, 0, 1.);

        for (i = 1; i <= l_max; i++) {
            gsl_vector_set(y, i, 0.);
        }
    }

    return 0;
}

/*
 * Compute the spherical Bessel function j_l at the points in x.
 * n: size of the double arrays y and x
 * y: spherical Bessel function j_l [output]
 * x: points where to evaluate j_l
 * l: index of the spherical Bessel function (orbital angular momentum quantum number)
 */
int bessel_j_all(const size_t n, double *y, const double *x, const size_t l)
{
    size_t i;

    for (i = 0; i < n; i++) {
        y[i] = bessel_j(x[i], l);
    }

    return 0;
}

/*
 * Compute the spherical Neumann function n_l at point x.
 * x: point where to evaluate n_l
 * l: index of the spherical Neumann function (orbital angular momentum quantum number)
 * return: n_l evaluated in x
 */
double bessel_n(const double x, const size_t l)
{
    if (x != 0.) {
        return _bessel(x, l, sin(x) / x, -cos(x) / x);
    } else {
        return -INFINITY;
    }
}

/*
 * Compute the spherical Neumann functions n_l, with 0 <= l <= l_max, at point x.
 * y: gsl_vector where to store the evaluation of the functions [output]
 * x: point where to evaluate the functions
 * l_max: maximum index of the spherical Neumann functions to evaluate
 */
int bessel_n_l(gsl_vector *y, const double x, const size_t l_max)
{
    size_t i;

    if (x != 0.) {
        _bessel_l(y, x, l_max, sin(x) / x, -cos(x) / x);
    } else {
        for (i = 0; i <= l_max; i++) {
            gsl_vector_set(y, i, -INFINITY);
        }
    }

    return 0;
}

/*
 * Compute the spherical Neumann function n_l at the points in x.
 * n: size of the double arrays y and x
 * y: spherical Neumann function n_l [output]
 * x: points where to evaluate n_l
 * l: index of the spherical Neumann function (orbital angular momentum quantum number)
 */
int bessel_n_all(const size_t n, double *y, const double *x, const size_t l)
{
    size_t i;

    for (i = 0; i < n; i++) {
        y[i] = bessel_n(x[i], l);
    }

    return 0;
}
