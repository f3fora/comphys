#ifndef BESSEL_H
#define BESSEL_H

#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_vector.h>

double bessel_j(const double, const size_t);
int bessel_j_l(gsl_vector *, const double, const size_t);
int bessel_j_all(const size_t, double *, const double *, const size_t);
double bessel_n(const double, const size_t);
int bessel_n_l(gsl_vector *, const double, const size_t);
int bessel_n_all(const size_t, double *, const double *, const size_t);

#endif
