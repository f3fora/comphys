#include "variational.h"

/*
 * Compute the one-body Hamiltonian of an electron immersed in the Coulomb potential produced by the
 * nucleus.
 * H: MxM gls_matrix in which the Hamiltonian matrix will be stored [output]
 * alpha: pointer to a double M-array containing the coefficients which define the Gaussians that
 *        form the basis with respect to which the Hamiltonian is written
 * Z: nuclear charge
 */
int hamilton(gsl_matrix *H, const double *alpha, const size_t Z)
{
    size_t i, j;
    double h;

    for (i = 0; i < H->size1; i++) {
        for (j = i; j < H->size1; j++) {
            // clang-format off
            h = (3. * alpha[i] * alpha[j] / pow(M_1_PI * (alpha[i] + alpha[j]), 1.5) -
                 2. * M_PI * (double)Z) / (alpha[i] + alpha[j]);
            // clang-format on
            gsl_matrix_set(H, i, j, h);
            if (i != j) {
                gsl_matrix_set(H, j, i, h);
            }
        }
    }

    return 0;
}

/*
 * Compute the overlap matrix resulting from the use of a basis of non-orthogonal Gaussians.
 * S: MxM gls_matrix in which the overlap matrix will be stored [output]
 * alpha: pointer to a double M-array containing the coefficients which define the Gaussians that
 *        form the considered basis
 */
int overlap_matrix(gsl_matrix *S, const double *alpha)
{
    size_t i, j;
    double e;

    for (i = 0; i < S->size1; i++) {
        for (j = i; j < S->size1; j++) {
            e = pow(M_PI / (alpha[i] + alpha[j]), 1.5);
            gsl_matrix_set(S, i, j, e);
            if (i != j) {
                gsl_matrix_set(S, j, i, e);
            }
        }
    }

    return 0;
}

/*
 * Solve the generalized eigenvalue problem HC = ESC, computing eigenvalues and eigenvectors.
 * E: pointer to a double M-array, where the eigenvalues will be stored [output]
 * C: MxM gsl_matrix whose columns will host the eigenvectors [output]
 * H: Hamiltonian (MxM gsl_matrix)
 * S: overlap matrix (MxM gsl_matrix)
 */
int find_eigenstates(gsl_vector *E, gsl_matrix *C, const gsl_matrix *H, const gsl_matrix *S)
{
    gsl_matrix *H_lamb = gsl_matrix_alloc(H->size1, H->size1);
    gsl_matrix *S_lamb = gsl_matrix_alloc(S->size1, S->size1);

    gsl_matrix_memcpy(S_lamb, S);
    gsl_matrix_memcpy(H_lamb, H);

    gsl_eigen_gensymmv_workspace *w = gsl_eigen_gensymmv_alloc(H->size1);
    gsl_eigen_gensymmv(H_lamb, S_lamb, E, C, w);
    gsl_eigen_gensymmv_free(w);

    gsl_matrix_free(H_lamb);
    gsl_matrix_free(S_lamb);

    return 0;
}

/*
 * Find the smallest eigenvalue and the corresponding eigenvector which solve the generalized
 * eigenvalue problem HC = ESC.
 * C: M-gsl_vector in which the eigenvector will be stored [output]
 * H: Hamiltonian (MxM gsl_matrix)
 * S: overlap matrix (MxM gsl_matrix)
 * return: smallest eigenvalue
 */
double find_ground_state(gsl_vector *C, const gsl_matrix *H, const gsl_matrix *S)
{
    gsl_vector *eval = gsl_vector_alloc(H->size1);
    gsl_matrix *evec = gsl_matrix_alloc(H->size1, H->size1);

    find_eigenstates(eval, evec, H, S);

    size_t i = gsl_vector_min_index(eval);
    double E = gsl_vector_get(eval, i);
    gsl_matrix_get_col(C, evec, i);

    gsl_vector_free(eval);
    gsl_matrix_free(evec);

    return E;
}

/*
 * Scale the coefficients of a linear combination of Gaussians, so that the single-particle wave
 * function it defines is normalized.
 * C: M-gsl_vector containing the coefficients (of the linear combination) to scale
 * alpha: pointer to a double M-array containing the coefficients which define the Gaussians that
 *        form the wave function
 * return: norm of the wave function
 */
double normalize(gsl_vector *C, const double *alpha)
{
    size_t i, j;
    double norm;

    norm = 0.;
    for (i = 0; i < C->size; i++) {
        for (j = 0; j < i; j++) {
            norm += 2. * gsl_vector_get(C, i) * gsl_vector_get(C, j) /
                    pow(alpha[i] + alpha[j], 1.5);
        }
        norm += pow(gsl_vector_get(C, i), 2) / pow(2. * alpha[i], 1.5);
    }
    norm = sqrt(pow(M_PI, 1.5) * norm);

    gsl_vector_scale(C, 1. / norm);

    return norm;
}
