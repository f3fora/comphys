import ctypes
from numpy.ctypeslib import ndpointer
import numpy as np

from pathlib import Path
import sys

THIS_DIRECTORY = Path(__file__).parent.absolute()
COMMON_DIRECTORY = Path(__file__).parents[2] / 'lib'

sys.path.append(str(COMMON_DIRECTORY))

from C_functions_utils import Cfunction


class NumerovParams(ctypes.Structure):
    """
    Parameters for the Numerov algorithm and the search for the energy levels.
    thresh: convergence threshold -> twice the error on the eigenenergies
    dE: energy increase for the search for the eigenenergies
    r_low: 1st point of the mesh
    h: discretization step

    typedef struct NumerovParams {
        double thresh;
	    double dE;
	    double r_low;
	    double h;
    } NumerovParams;
    """
    _fields_ = [('thresh', ctypes.c_double), ('dE', ctypes.c_double),
                ('r_low', ctypes.c_double), ('h', ctypes.c_double)]


class Levels(ctypes.Structure):
    """
    Energy levels.
    orbitals: pointer to a double (size * M)-array containing the orbitals
    E: pointer to a double (size)-array containing the eigenenergies
    n: pointer to a size_t (size)-array containing the values of the principal quantum number
    l: pointer to a szie_t (size)-array containing the values of the orbital angular momentum quantum number
    size: number of energy levels
    M: number of points of the mesh on which the orbitals are discretized
    
    typedef struct Levels {
        double *orbitals;
        double *E;
        size_t *n;
        size_t *l;
        size_t size;
        size_t M;
    } Levels;
    """
    _fields_ = [('orbitals', ctypes.POINTER(ctypes.c_double)),
                ('E', ctypes.POINTER(ctypes.c_double)),
                ('n', ctypes.POINTER(ctypes.c_size_t)),
                ('l', ctypes.POINTER(ctypes.c_size_t)),
                ('size', ctypes.c_size_t), ('M', ctypes.c_size_t)]


class ShellSystems(Cfunction):
    def __init__(self):
        _argtypes = (
            ctypes.c_size_t,  # const size_t num_cs_systems
            ctypes.c_double,  # const double r_s
            NumerovParams,  # const NumerovParams np
            ctypes.c_size_t,  # const size_t M
            ctypes.c_size_t,  # const size_t M_cut
            ndpointer(dtype=np.uint, ndim=1,
                      flags='C_CONTIGUOUS'),  # size_t *N
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # double *E_tot
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # double *r
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # double *V_ext
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # double *rho
            ctypes.POINTER(Levels),  # Levels *levels
            ndpointer(dtype=np.uint, ndim=1,
                      flags='C_CONTIGUOUS')  # size_t *levels_sizes
        )
        _restype = ctypes.c_int
        _filename = THIS_DIRECTORY / 'libdft.so'
        super().__init__(_filename, _argtypes, _restype)
        self.init_function(self.CDLL.compute_closed_shell_systems)

    def _compute_closed_shell_systems(self, num_cs_systems: int, r_s: float,
                                      M: int, M_cut: int, thresh: float,
                                      dE: float, r_low: float, h: float):
        NP = NumerovParams(thresh, dE, r_low, h)
        N = np.empty(shape=num_cs_systems, dtype=np.uint)
        E_tot = np.empty(shape=num_cs_systems)
        r = np.empty(shape=M)
        V_ext = np.empty(shape=num_cs_systems * M)
        rho = np.empty(shape=num_cs_systems * M_cut)
        levels_sizes = np.empty(shape=num_cs_systems, dtype=np.uint)
        levels = Levels()

        not_conv = self.function(ctypes.c_size_t(num_cs_systems),
                                 ctypes.c_double(r_s), NP, ctypes.c_size_t(M),
                                 ctypes.c_size_t(M_cut), N, E_tot, r, V_ext,
                                 rho, ctypes.byref(levels), levels_sizes)

        n = np.asarray(levels.n[:levels.size], dtype=np.uint)
        l = np.asarray(levels.l[:levels.size], dtype=np.uint)
        E = np.asarray(levels.E[:levels.size])
        orbitals = np.asarray(levels.orbitals[:levels.size *
                                              levels.M]).reshape(
                                                  levels.size, levels.M)
        return N, E_tot, r, V_ext.reshape(-1, M), rho.reshape(
            -1, M_cut), levels_sizes, n, l, E, orbitals, not_conv


def compute_closed_shell_systems(*args, **kwargs):
    """ compute_closed_shell_systems(num_cs_systems, r_s, M, M_cut, thresh, dE, r_low, h)
        Compute the numbers of valence electrons in an alkali metal cluster for which closed-shell systems are obtained, treating the electrons as independent. For each closed-shell system, compute the occupied energy levels, the corresponding normalized orbitals, the electronic density and the total ground state energy.

        Parameters:
            num_cs_systems: int
                Number of closed-shell systems to find, starting from N = 2 valence electrons
            r_s: float
                Wigner-Seitz radius of the considered atomic species
            M: int
                Number of points of the mesh on which the external potential is discretized
            M_cut: int
                Number of points of the mesh on which the orbitals and the electronic density are discretized

            -- NumerovParams
            thresh: float
            dE: float
            r_low: float
            h: float     

        Returns:
            N: 1D array(shape num_cs_system)
                Numbers of valence electrons 
            E_tot: 1D array(shape num_cs_system)
                Total ground state energies
            r: 1D array(shape M)
                Positions
            V_ext: 2D array(shape num_cs_system x M)
                External potentials 
            rho: 2D array(shape num_cs_system x M_cut)
                Electronic densities 
            levels_sizes: 1D array(1D, shape num_cs_system)
                Number of occupied levels for each closed-shell system

            -- Levels
            n: 1D array(shape size)
            l: 1D array(shape size)
            E: 1D array(shape size)
            orbitals: 2D array(shape size x M_cut)

            not_conv: int
                if the secant algorithm used in the search for the energy levels has terminated successfully for each computed level: 0
                else: number of times the maximum number of iterations MAX_SEC has been reached

        Binding:
            int compute_closed_shell_systems(const size_t num_cs_systems, const double r_s, const NumerovParams np, const size_t M, const size_t M_cut, size_t *N, double *E_tot, double *r, double *V_ext, double *rho, Levels *levels, size_t *levels_sizes)

        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libdft.so ./src/*.c ./src/*.h ../src/*.c ../src/*.h
    """
    return ShellSystems()._compute_closed_shell_systems(*args, **kwargs)


class SelfConsistent(Cfunction):
    def __init__(self):
        _argtypes = (
            ctypes.c_size_t,  # const size_t N
            ctypes.c_double,  # const double r_s
            NumerovParams,  # const NumerovParams np
            ctypes.c_size_t,  # const size_t M
            ctypes.c_size_t,  # const size_t M_cut
            ctypes.c_double,  # const double mix
            ctypes.c_double,  # const double thresh_scf
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # double *E_tot
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # double *r
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # double *V_ks
            ndpointer(dtype=np.double, ndim=1,
                      flags='C_CONTIGUOUS'),  # double *rho
            ctypes.POINTER(Levels),  # Levels *levels
            ndpointer(dtype=np.uint, ndim=1,
                      flags='C_CONTIGUOUS')  # size_t *num_iter
        )
        _restype = ctypes.c_int
        _filename = THIS_DIRECTORY / 'libdft.so'
        super().__init__(_filename, _argtypes, _restype)
        self.init_function(self.CDLL.self_consistent_method)

    def _self_consistent_method(self, N: int, r_s: float, M: int, M_cut: int,
                                mix: float, thresh_scf: float, thresh: float,
                                dE: float, r_low: float, h: float):
        NP = NumerovParams(thresh, dE, r_low, h)
        E_tot = np.empty(shape=1)
        r = np.empty(shape=M)
        V_ks = np.empty(shape=M)
        rho = np.empty(shape=M_cut)
        num_iter = np.empty(shape=1, dtype=np.uint)
        levels = Levels()

        not_conv = self.function(ctypes.c_size_t(N), ctypes.c_double(r_s), NP,
                                 ctypes.c_size_t(M), ctypes.c_size_t(M_cut),
                                 ctypes.c_double(mix),
                                 ctypes.c_double(thresh_scf), E_tot, r, V_ks,
                                 rho, ctypes.byref(levels), num_iter)

        n = np.array(levels.n[:levels.size], dtype=np.uint)
        l = np.array(levels.l[:levels.size], dtype=np.uint)
        E = np.array(levels.E[:levels.size])
        orbitals = np.array(levels.orbitals[:levels.size * levels.M]).reshape(
            levels.size, levels.M)
        return E_tot, r, V_ks, rho, n, l, E, orbitals, num_iter, not_conv


def self_consistent_method(*args, **kwargs):
    """ self_consistent_method(N, r_s, M, M_cut, mix, thresh_scf, thresh, dE, r_low, h)
        Solve, through a self-consistent field procedure, the Kohn-Sham equations for an alkali metal cluster described by a jellium model, obtaining the effective mean-field potential, the ground state energy and the electronic density.

        Parameters:
            N: int
                Number of valence electrons
            r_s: float
                Wigner-Seitz radius of the considered atomic species
            M: int
                Number of points of the mesh on which the potential is discretized
            M_cut: int 
                Number of points of the mesh on which the orbitals and the electronic density are discretized
            mix: float
                Mixing parameter
            thresh_scf: float
                Threshold for evaluating the convergence of the self-consistent field calculation;

            -- NumerovParams
            thresh: float
            dE: float
            r_low: float
            h: float

        Returns:
            E_tot: 0D array
                Ground state energy
            r: 1D array(shape M) 
                Positions
            V_ks: 1D array(shape M)
                Effective mean-field potential (V_ext + V_d + V_xc)
            rho: 1D array(shape M_cut)
                Electronic density
            
            -- Levels
            n: 1D array(shape shape size)
            l: 1D array(shape shape size)
            E: 1D array(shape shape size)  
            orbitals: 1D array(shape shape size)
        
            num_iter: 0D array
                Number of iterations required for the convergence or 0 if not converges
            not_conv: int
                if the system is closed-shell and the final levels correspond to the initial ansatz:
                    if the secant algorithm used in the search for the energy levels has terminated successfully for each computed level: 0
                    else: number of times the maximum number of iterations MAX_SEC has been reached
                else:
				    if the system is not closed-shell: -1
			        if the system seems closed-shell, but the final levels do not correspond to the initial ansatz: -2

        Binding:
            int self_consistent_method(const size_t N, const double r_s, const NumerovParams np, const size_t M, const size_t M_cut, const double mix, const double thresh_scf, double *E_tot, double *r, double *V_ks, double *rho, Levels *levels, size_t *num_iter)
                                       
        Compiling:
            gcc -fPIC -Wall -shared -lm -lgsl -lgslcblas -o ./lib/libdft.so ./src/*.c ./src/*.h ../src/*.c ../src/*.h
    """
    return SelfConsistent()._self_consistent_method(*args, **kwargs)
