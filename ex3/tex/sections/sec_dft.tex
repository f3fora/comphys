\section{Density Functional Theory} \label{sec:dft}

The Density Functional Theory (DFT) is a mean-field theory, largely used in Condensed Matter Physics and Quantum Chemistry for the study of many-body quantum systems.

In this exercise, we apply this theory to closed-shell alkali metal clusters of Na and K atoms, modeled as a sphere of uniform positive charge (\textit{jellium}) containing $N$ electrons. We assume that the \textit{jellium} sphere has a charge density $\rho_b$ and a radius $R_c$, such that $(4/3)\pi\rho_b R_c^3 = N$. $\rho_b$ is parametrized by the Wigner-Seitz radius $R_s$, dependent on the considered atomic species, in such a way that $(4/3)\pi\rho_b R_s^3 = 1$. In particular, for Na atoms $R_s = 3.93\,a_0$, while for K atoms $R_s = 4.86\,a_0$. With these assumptions, the potential generated by the positive charge takes the form\footnote{Here and for the rest of this paper, we use the so-called Hartree atomic units, defined by setting $\hbar = e = a_0 = m_e = 1$.}
\begin{equation}\label{eq:V_ext}
    V_{ext}(r) = 2 \pi \rho_b \times
    \begin{dcases}
        \frac{1}{3} r^2 - R_c^2 &\mbox{if } r \le R_c \\
        -\frac{2}{3} \frac{R_c^3}{r} &\mbox{if } r > R_c
    \end{dcases}
\end{equation}
where $r = |\mathbf{r}|$ is the radial distance from the center of the cluster.

In order to calculate the ground state energy and the ground state electronic density $\rho(r)\,$\footnote{In our case, the electronic density depends only on the radial coordinate, since we consider spherically symmetrical closed-shell systems.} of such modeled clusters, we exploit the DFT, which prescribes to minimize the following energy functional \cite{Pederiva}:
\begin{equation}\label{eq:EF}
    \begin{aligned}
        E[\rho] = &\sum_{k} \int \dd\mathbf{r}\,\phi_{k}^{*}(\mathbf{r})\left(-\frac{1}{2}\nabla^2\right)\phi_{k}(\mathbf{r}) + \int \dd\mathbf{r}\,\rho(r)V_{ext}(r) \,+\\ &+\frac{1}{2}\iint\dd\mathbf{r}\dd\mathbf{r}'\,\frac{\rho(r)\rho(r')}{\abs{\mathbf{r}-\mathbf{r}'}} + E_{xc}[\rho]\,,
    \end{aligned}   
\end{equation}
where the sum is extended on all the occupied orbitals $\phi_k(\mathbf{r})$. $E_{xc}[\rho]$ is the exchange-correlation energy, which, in the local density approximation (LDA), reads
\begin{equation}\label{eq:LDA}
    E_{xc}[\rho] = \int\dd\mathbf{r}\,\rho(r) \varepsilon_{xc}\!\left[\rho(r)\right] = \int\dd\mathbf{r}\,\rho(r)\left(\varepsilon_x\!\left[\rho(r)\right] + \varepsilon_c\!\left[\rho(r)\right]\right),
\end{equation}
where $\varepsilon_x\!\left[\rho(r)\right]$ and $\varepsilon_c\!\left[\rho(r)\right]$ are respectively the exchange and correlation energy density. As exchange energy density, we consider
\begin{equation}\label{eq:eps_x}
    \varepsilon_x\!\left[\rho(r)\right] = -\frac{3}{4}\left(\frac{3}{\pi}\right)^{1/3}\!\rho(r)^{1/3}\,,
\end{equation}
while as correlation energy density we use the so-called Perdew-Wang parametrization \cite{Perdew1992}:
\begin{equation}\label{eq:eps_c}
    \varepsilon_c\!\left[\rho(r)\right] = -2A\left(1 + \alpha_1 r_s\right) \log\!\left(1 + \frac{1}{2A \big[\beta_1 r_s^{1/2} + \beta_2 r_s + \beta_3 r_s^{3/2}\! + \beta_4 r_s^{p+1}\big]}\right),
\end{equation}
where
\begin{equation}\label{eq:r_s}
     r_s\!\left[\rho(r)\right] \equiv \left[\frac{3}{4\pi\rho(r)}\right]^{1/3}
\end{equation}
and the parameters are set equal to the following values:
\begin{equation}\label{eq:parameters}
    \begin{gathered}
        p = 1, \quad A = 0.031091, \quad \alpha_1 = 0.21370, \\
        \beta_1 = 7.5957, \quad \beta_2 = 3.5876, \quad \beta_3 = 1.6382, \quad \beta_4 = 0.49294.
    \end{gathered}
\end{equation}

By minimizing the energy density functional \eqref{eq:EF} with respect to the orbitals $\phi_k(\mathbf{r})$, we obtain the Kohn-Sham (KS) equations \cite{Pederiva}:
\begin{equation}\label{eq:KS_equations}
\begin{aligned}
    &\left[-\frac{1}{2}\nabla^2 + V_{KS}(r)\right]\!\phi_k(\mathbf{r}) = \mu_k \phi_k(\mathbf{r})\,,
\end{aligned}
\end{equation}
where
\begin{equation}\label{eq:V_KS}
    V_{KS}(r) \equiv V_{ext}(r) + V_{d}(r) + V_{xc}\!\left[\rho(r)\right],
\end{equation}
with
\begin{equation}\label{eq:V_d}
    V_{d}(r) \equiv \int \dd\mathbf{r}'\,\frac{\rho(r')}{\abs{\mathbf{r}-\mathbf{r}'}} = 4\pi \left[\frac{1}{r}\int_0^r \dd r'\,r'^2\rho(r') + \int_r^{+\infty}\dd r'\,r'\rho(r')\right]
\end{equation}
and
\begin{equation}\label{eq:V_xc}
    V_{xc}\!\left[\rho(r)\right] \equiv \frac{\delta E_{xc}[\rho]}{\delta\rho} = \varepsilon_{xc}\!\left[\rho(r)\right] + \rho(r)\frac{\partial\varepsilon_{xc}\!\left[\rho(r)\right]}{\partial \rho}\,,
\end{equation}
where $\delta E_{xc}[\rho] / \delta\rho$ is a functional derivative\footnote{The functional derivative of a functional $F\!\left[\phi(x)\right]$ is evaluated as \[\frac{\delta F\!\left[\phi(x)\right]}{\delta \phi(y)} = \lim_{\varepsilon\to 0} \frac{F\!\left[\phi(x) + \varepsilon\delta(x-y)\right] - F\!\left[\phi(x)\right]}{\varepsilon}\,.\]}. The KS equations \eqref{eq:KS_equations} have solutions of the form
\begin{equation}\label{eq:orbitals}
    \phi_{n\ell m}(\mathbf{r}) = \mathcal{R}_{n\ell}(r) Y_{\ell m}(\theta, \varphi) = \frac{u_{n\ell}(r)}{r} Y_{\ell m}(\theta, \varphi)\,,
\end{equation}
where $Y_{\ell m}(\theta, \varphi)$ are the spherical harmonics and the radial functions $u_{n\ell}(r) \equiv \mathcal{R}_{n\ell}(r) r$ satisfy the following radial Schrödinger equation:
\begin{equation}\label{eq:radial_equation}
    \left[-\frac{1}{2}\derivative[2]{r} + \frac{\ell(\ell + 1)}{2r^2} + V_{KS}(r)\right]\!u_{n\ell}(r) = \mu_{n\ell}u_{n\ell}(r)\,.
\end{equation}

The unknowns of \eqref{eq:radial_equation} are the radial functions $u_{n\ell}(r)$ which define the occupied orbitals and the corresponding eigenvalues $\mu_{n\ell}$. The solutions appear in the mean-field potential \eqref{eq:V_KS} through the electronic density
\begin{equation}\label{eq:density}
    \rho(r) = 2\sum_{n,\ell,m} \abs{\phi_{n\ell m}(\mathbf{r})}^2 = \sum_{n,\ell} \frac{2(2\ell + 1)}{4\pi} \abs{\frac{u_{n\ell}(r)}{r}}^2,
\end{equation}
where the sum is extended on the occupied orbitals. Consequently, the resolution of \eqref{eq:radial_equation} requires a self-consistent field (SCF) procedure, as illustrated in \autoref{fig:flowchart}.

In order to find the quantum numbers which identify the occupied orbitals, we preliminarily solve the independent electron problem described by the following radial Schrödinger equation:
\begin{equation}\label{eq:indep_radial_equation}
    \left[-\frac{1}{2}\derivative[2]{r} + \frac{\ell(\ell + 1)}{2r^2} + V_{ext}(r)\right]\!u_{n\ell}(r) = \mu_{n\ell}u_{n\ell}(r)\,,
\end{equation}
from which we obtain an initial guess on the electronic density $\rho(r)$. Subsequently, at each iteration of the SCF loop, we calculate only the energy levels found by solving the independent electron problem. At the end of the algorithm, we verify a posteriori that the calculated levels are the lowest in energy, i.e. those occupied.

\inputflowchart{flowchart}{Self-Consistent Field Method}{The algorithm starts with the resolution of the independent electron problem, which provides an initial guess on the electronic density $\rho(r)$. Subsequently, iteratively, the KS equations \eqref{eq:KS_equations} are solved, obtaining the eigenvalues $\mu_k$ and the occupied orbitals $\phi_k(\mathbf{r})$. The latter are used to compute a new electronic density. In the next iteration, the density is a weighted average between the last calculated density and that of the previous iteration. The loop ends when an established convergence criterion is satisfied.}

At each iteration, the mixing between the new electronic density $\rho_{new}(r)$ and the old one $\rho_{old}(r)$ is defined by the following weighted average:
\begin{equation}
    \rho(r) = \xi \rho_{new}(r) + (1 - \xi)\rho_{old}(r)\,, 
\end{equation}
where $0 < \xi < 1$ is called mixing parameter. This procedure is typically necessary to avoid the non-convergence of the algorithm.

For the convergence criterion, we exploit the existence of two different expressions for the ground state energy of the system. The first one is given by the energy functional \eqref{eq:EF} calculated on the solutions of the KS equations \eqref{eq:KS_equations}:
\begin{equation}\label{eq:E_0_fun}
    \begin{aligned}
        E_0^{fun} =& \sum_{n,\ell}2(2\ell + 1) \int\dd\mathbf{r}\,\frac{u_{n\ell}^{*}(r)}{r^2}\left[-\frac{1}{2}\derivative[2]{u_{n\ell}(r)}{r} + \frac{\ell(\ell + 1)}{2 r^2} u_{n\ell}(r)\right] +\\ &+ \int\dd\mathbf{r}\,\rho(r)V_{ext}(r) + \frac{1}{2}\iint\dd\mathbf{r}\dd\mathbf{r}'\,\frac{\rho(r)\rho(r')}{\abs{\mathbf{r}-\mathbf{r}'}} + E_{xc}[\rho]\,.
    \end{aligned}    
\end{equation}
The second one considers the eigenvalues $\mu_{n\ell}$ resulting from the resolution of \eqref{eq:radial_equation}:
\begin{equation}\label{eq:E_0_eig}
    E_0^{eig} = \sum_{n,\ell}2(2\ell + 1)\mu_{n\ell} - \frac{1}{2}\iint\dd\mathbf{r}\dd\mathbf{r}'\,\frac{\rho(r)\rho(r')}{\abs{\mathbf{r}-\mathbf{r}'}} + E_{xc}[\rho] - \int\dd\mathbf{r}\,\rho(r)\frac{\delta E_{xc}[\rho]}{\delta\rho}\,.
\end{equation}
If the SCF procedure has reached convergence, then the two expressions must be equal. Hence, the SCF loop is terminated when the difference between $E_0^{fun}$ and $E_0^{eig}$ is smaller in modulus than a fixed threshold $\epsilon_{scf}$.

For the resolution of the Schrödinger equations \eqref{eq:radial_equation} and \eqref{eq:indep_radial_equation} we apply the Numerov method\footnote{In the following, with reference to Exercise 1 \cite{OurWork}, $h$ is the discretization step, $M$ is the number of points in the mesh and $\epsilon_{N}$ is the convergence threshold.}, following the same strategy adopted in Exercise 1 \cite{OurWork}. In order to obtain appropriate initial conditions for the algorithm, we study the equations in the $r \to 0$ limit, for which
\begin{equation}\label{eq:r_0_limit}
    u_{n\ell}(r) \sim r^{\ell+1}\,,
\end{equation}
where the normalization is arbitrary. Hence, we set
\begin{equation}\label{eq:initial_conditions}
    u_{n\ell}(r_{low}) = r_{low}^{\ell + 1}\,, \quad u_{n\ell}(r_{low} + h) = (r_{low} + h)^{\ell + 1}\,,
\end{equation}
with $r_{low} \neq 0$ sufficiently small, since the centrifugal potential diverges for $r \to 0$.

All the integrals dependent on the orbitals are numerically evaluated using the composite Simpson's $1/3$ rule\footnote{The composite Simpson's $1/3$ rule is \[\int_a^b f(x)\,\dd x \approx \frac{h}{3} \bigg[f(x_0) + 2\sum_{i=1}^{m/2-1} f(x_{2i}) + 4\sum_{i=1}^{m/2} f(x_{2i-1}) + f(x_m)\bigg]\,,\] with $x_i \equiv a + ih$, $m \equiv (b - a) / h \ge 2$ even.}. The second derivative appearing in \eqref{eq:E_0_fun} is instead numerically computed through seven-point formulas\footnote{As an example, the symmetrical seven-point approximation of the second derivative (obtained through an online \href{https://web.media.mit.edu/~crtaylor/calculator.html}{\underline{calculator}}) is \[\derivative[2]{f(x)}{x} \approx \frac{2f(x-3h) - 27f(x-2h) + 270f(x-h) - 490f(x) + 270f(x+h) - 27f(x+2h) + 2f(x+3h)}{180 h^2}\,.\]}.