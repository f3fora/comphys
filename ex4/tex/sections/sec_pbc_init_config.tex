\subsection{Periodic Boundary Conditions and Initial Configuration} \label{sec:pbc_init_config}

The superfluid \ce{^4He} is treated as an infinite, homogeneous and isotropic system. In particular, we describe the system as an ensemble of infinite cubic boxes, arranged side by side, each containing an equal number of particles, placed in the same configuration in each box. Our attention therefore focuses on the $N$ particles in the main box (hereinafter referred to as the simulation box), which is imagined to be periodically repeated in space. The volume $V$, and therefore the side $L$, of the simulation box are fixed by the density $\rho$ of the system, according to the relation
\begin{equation}
    \rho = \frac{N}{V} = \frac{N}{L^3}\,,
\end{equation}
while the positions of the particles inside it are described by a Cartesian coordinate system with origin placed in the center of the cube. Consequently, the particles in the box have coordinates $x$, $y$ and $z$ that satisfy the relations $-L/2 \le x \le L/2$, $-L/2 \le y \le L/2$ and $-L/2 \le z \le L/2$.

To account for the periodicity of the system, we impose the Born-von Karman periodic boundary conditions (pbc). These require that, if a particle leaves the simulation box, then another one enters from the opposite edge of the box, at the point identified by the direction of the movement of the first. The pbc are easily implemented by transforming each coordinate $r^k$ (with $k = x,\,y,\,z$) external to the simulation box according to the following relation\footnote{We define $\lfloor x \rfloor \equiv \max\!\left(\{q \in \Z \,|\, q \le x\}\right)$.}: 
\begin{equation}
    r^k \mapsto r^k - L \left\lfloor \frac{r^k}{L} + \frac{1}{2} \right\rfloor .
\end{equation}

To avoid problems due to the divergence of the LJ potential \eqref{eq:lj_potential} for $r \to 0$, as an initial configuration, we place the $N$ particles of the simulation box in a face-centered cubic (fcc) lattice. In particular, the simulation box is divided into $n = N / 4$ elementary cubic cells of side $\ell = L / n^{\sfrac{1}{3}}$, each containing $4$ particles\footnote{Notice that $N$ must be a natural number writable as $N = 4 q^{3}$, with $q \in \N$.}. The side $\ell$ can also be written in terms of the density $\rho$, as $\ell = (4 / \rho)^{\sfrac{1}{3}}$. Each elementary cell is filled by placing the first particle in the vertex $(x_0,\,y_0,\,z_0)$ (the vertex that would be placed at $(0,\,0,\,0)$ if we moved rigidly the cell in space until a vertex is situated at $(0,\,0,\,0)$, with all the other points of the cell placed in the octant of the coordinate system characterized by the three coordinates always positive), and the remaining $3$ particles in the positions
\begin{equation}
    \left(x_0 + \frac{\ell}{2},\, y_0 + \frac{\ell}{2},\, z_0\right), \quad \left(x_0,\, y_0 + \frac{\ell}{2},\, z_0 + \frac{\ell}{2}\right), \quad \left(x_0 + \frac{\ell}{2},\, y_0,\, z_0 + \frac{\ell}{2}\right).
\end{equation}